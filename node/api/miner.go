package api

import (
	"fmt"
	"io"
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/NebulousLabs/encoding"

	"gitlab.com/scpcorp/ScPrime/types"
)

type (
	// MinerGET contains the information that is returned after a GET request
	// to /miner.
	MinerGET struct {
		BlocksMined      int  `json:"blocksmined"`
		CPUHashrate      int  `json:"cpuhashrate"`
		CPUMining        bool `json:"cpumining"`
		StaleBlocksMined int  `json:"staleblocksmined"`
	}
)

// minerHandler handles the API call that queries the miner's status.
func (api *API) minerHandler(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	blocksMined, staleMined := api.miner.BlocksMined()
	mg := MinerGET{
		BlocksMined:      blocksMined,
		CPUHashrate:      api.miner.CPUHashrate(),
		CPUMining:        api.miner.CPUMining(),
		StaleBlocksMined: staleMined,
	}
	WriteJSON(w, mg)
}

// minerStartHandler handles the API call that starts the miner.
func (api *API) minerStartHandler(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	api.miner.StartCPUMining()
	WriteSuccess(w)
}

// minerStopHandler handles the API call to stop the miner.
func (api *API) minerStopHandler(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	api.miner.StopCPUMining()
	WriteSuccess(w)
}

// minerHeaderHandlerGET handles the API call that retrieves a block header
// for work for BLAKE2.
func (api *API) minerHeaderHandlerGET(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	bhfw, target, height, err := api.miner.HeaderForWork()
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	allowBlake2b := height < types.Blake256r14SoftforkHeight
	if !allowBlake2b {
		WriteError(w, Error{"BLAKE2 is disabled"}, http.StatusBadRequest)
		return
	}
	w.Write(encoding.MarshalAll(target, bhfw))
}

// minerBlake256r14InputHandlerGET handles the API call that retrieves an
// input for BLAKE256r14 mining.
func (api *API) minerBlake256r14InputHandlerGET(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	bhfw, target, height, blockValue, err := api.miner.HeaderForWorkWithBlockValue()
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	allowBlake256r14 := height >= types.Blake256r14HardforkHeight
	if !allowBlake256r14 {
		WriteError(w, Error{"BLAKE256r14 is disabled"}, http.StatusBadRequest)
		return
	}
	var input [types.Blake256r14InputSize]byte
	bhfw.Blake256r14Input(&input)

	// Set block height HTTP response header.
	w.Header().Set("ScPrime-BlockHeight", fmt.Sprintf("%d", height+1))
	w.Header().Set("ScPrime-BlockValue", blockValue.String())

	w.Write(encoding.MarshalAll(target, input))
}

// minerHeaderHandlerPOST handles the API call to submit a block header to the
// miner.
func (api *API) minerHeaderHandlerPOST(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var bh types.BlockHeader
	err := encoding.NewDecoder(req.Body, encoding.DefaultAllocLimit).Decode(&bh)
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	err = api.miner.SubmitHeader(bh)
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	WriteSuccess(w)
}

// minerBlockHandlerPOST handles the API call to submit a solved block to the
// miner.
func (api *API) minerBlockHandlerPOST(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var b types.Block
	err := encoding.NewDecoder(req.Body, encoding.DefaultAllocLimit).Decode(&b)
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	err = api.miner.SubmitBlock(b)
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	WriteSuccess(w)
}

// minerBlake256r14InputHandlerPOST handles the API call to submit a (solved) BLAKE256r14 input.
func (api *API) minerBlake256r14InputHandlerPOST(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	// Read all body, limiting to Blake256r14InputSize bytes.
	body, err := io.ReadAll(&io.LimitedReader{R: req.Body, N: types.Blake256r14InputSize})
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	if len(body) != types.Blake256r14InputSize {
		WriteError(w, Error{fmt.Errorf("Wrong input size: %d, want %d", len(body), types.Blake256r14InputSize).Error()}, http.StatusBadRequest)
		return
	}

	var input [types.Blake256r14InputSize]byte
	copy(input[:], body)

	var bh types.BlockHeader
	if err := bh.FromBlake256r14Input(&input); err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}

	err = api.miner.SubmitHeader(bh)
	if err != nil {
		WriteError(w, Error{err.Error()}, http.StatusBadRequest)
		return
	}
	WriteSuccess(w)
}
