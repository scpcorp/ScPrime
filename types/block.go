package types

// block.go defines the Block type for ScPrime, and provides some helper functions
// for working with blocks.

import (
	"bytes"
	"fmt"
	"math"

	"gitlab.com/NebulousLabs/encoding"
	"gitlab.com/scpcorp/ScPrime/crypto"
)

const (
	// BlockHeaderSize is the size, in bytes, of a block header.
	// 32 (ParentID) + 8 (Nonce) + 8 (Timestamp) + 32 (MerkleRoot)
	BlockHeaderSize = 80
)

type (
	// A Block is a summary of changes to the state that have occurred since the
	// previous block. Blocks reference the ID of the previous block (their
	// "parent"), creating the linked-list commonly known as the blockchain. Their
	// primary function is to bundle together transactions on the network. Blocks
	// are created by "miners," who collect transactions from other nodes, and
	// then try to pick a Nonce that results in a block whose BlockID is below a
	// given Target.
	Block struct {
		ParentID     BlockID         `json:"parentid"`
		Nonce        BlockNonce      `json:"nonce"`
		Timestamp    Timestamp       `json:"timestamp"`
		MinerPayouts []SiacoinOutput `json:"minerpayouts"`
		Transactions []Transaction   `json:"transactions"`
	}

	// A BlockHeader contains the data that, when hashed, produces the Block's ID.
	BlockHeader struct {
		ParentID   BlockID     `json:"parentid"`
		Nonce      BlockNonce  `json:"nonce"`
		Timestamp  Timestamp   `json:"timestamp"`
		MerkleRoot crypto.Hash `json:"merkleroot"`
	}

	// BlockHeight is the number of blocks that exist after the genesis block.
	BlockHeight uint64
	// A BlockID is the hash of a BlockHeader. A BlockID uniquely
	// identifies a Block, and indicates the amount of work performed
	// to mine that Block. The more leading zeros in the BlockID, the
	// more work was performed.
	BlockID crypto.Hash
	// The BlockNonce is a "scratch space" that miners can freely alter to produce
	// a BlockID that satisfies a given Target.
	BlockNonce [8]byte
)

// preForkCoinbase is the former version of CalculateCoinbase function.
// It was used before the Blake256r14 hardfork was implemented.
// Use for tests only!
func preForkCoinbase(height BlockHeight) Currency {
	base := InitialCoinbase - uint64(height)
	if uint64(height) > InitialCoinbase || base < MinimumCoinbase {
		base = MinimumCoinbase
	}
	return NewCurrency64(base).Mul(SiacoinPrecision)
}

// CalculateCoinbase calculates the coinbase for a given height. The coinbase
// equation is:
//
//	coinbase := max(InitialCoinbase - height, MinimumCoinbase) * SiacoinPrecision in [0, Blake256r14HardforkHeight).
//	coinbase := FixedCoinbase * SiacoinPrecision in [Blake256r14HardforkHeight, FixedCoinbaseEndHeight)
//	coinbase := `1 hasting` in [FixedCoinbaseEndHeight, infinity)
func CalculateCoinbase(height BlockHeight) Currency {
	if height >= FixedCoinbaseEndHeight {
		return NewCurrency64(1)
	}
	base := uint64(0)
	if height >= Blake256r14HardforkHeight && height < FixedCoinbaseEndHeight {
		base = FixedCoinbase
	} else {
		base = InitialCoinbase - uint64(height)
		if uint64(height) > InitialCoinbase || base < MinimumCoinbase {
			base = MinimumCoinbase
		}
	}
	return NewCurrency64(base).Mul(SiacoinPrecision)
}

type coinbaseRange struct {
	start BlockHeight
	end   BlockHeight
	value Currency
}

// preForkCalculateNumSiacoins is the former version of CalculateNumSiacoins
// used before Blake256r14 hardfork was implemented. Only for tests!
func preForkCalculateNumSiacoins(height BlockHeight) Currency {
	airdropCoins := AirdropCommunityValue.Add(AirdropPoolValue).Add(AirdropNebulousLabsValue).Add(AirdropSiaPrimeValue)

	deflationBlocks := BlockHeight(InitialCoinbase - MinimumCoinbase)
	avgDeflationSiacoins := CalculateCoinbase(0).Add(CalculateCoinbase(height)).Div(NewCurrency64(2))
	if height <= deflationBlocks {
		deflationSiacoins := avgDeflationSiacoins.Mul(NewCurrency64(uint64(height + 1)))
		return airdropCoins.Add(deflationSiacoins)
	}
	deflationSiacoins := avgDeflationSiacoins.Mul(NewCurrency64(uint64(deflationBlocks + 1)))
	trailingSiacoins := NewCurrency64(uint64(height - deflationBlocks)).Mul(CalculateCoinbase(height))
	return airdropCoins.Add(deflationSiacoins).Add(trailingSiacoins)
}

// CalculateNumSiacoins calculates the number of siacoins in circulation at a
// given height.
func CalculateNumSiacoins(height BlockHeight) (totalCoins Currency) {
	airdropCoins := AirdropCommunityValue.Add(AirdropPoolValue).Add(AirdropNebulousLabsValue).Add(AirdropSiaPrimeValue)
	deflationEnd := BlockHeight(InitialCoinbase - MinimumCoinbase + 1)
	var avgDeflationSiacoins Currency
	if height <= deflationEnd {
		avgDeflationSiacoins = CalculateCoinbase(0).Add(CalculateCoinbase(height)).Div(NewCurrency64(2))
	} else {
		avgDeflationSiacoins = CalculateCoinbase(0).Add(CalculateCoinbase(deflationEnd)).Div(NewCurrency64(2))
	}
	minimumCoinbase := CalculateCoinbase(deflationEnd)
	fixedCoinbase := CalculateCoinbase(Blake256r14HardforkHeight)
	ranges := []coinbaseRange{
		{start: BlockHeight(0), end: deflationEnd, value: avgDeflationSiacoins},
		{start: deflationEnd, end: Blake256r14HardforkHeight, value: minimumCoinbase},
		{start: Blake256r14HardforkHeight, end: FixedCoinbaseEndHeight, value: fixedCoinbase},
		{start: FixedCoinbaseEndHeight, end: BlockHeight(math.MaxUint64), value: NewCurrency64(1)},
	}
	totalCoins = airdropCoins
	for _, r := range ranges {
		blocks := uint64(r.end - r.start)
		if height < r.end {
			blocks = uint64(height - r.start + 1)
			totalCoins = totalCoins.Add(r.value.Mul(NewCurrency64(blocks)))
			break
		}
		totalCoins = totalCoins.Add(r.value.Mul(NewCurrency64(blocks)))
	}
	return
}

var numGenesisSiacoins = func() Currency {
	// Sum all the values for the genesis siacoin outputs.
	numGenesisSiacoins := NewCurrency64(0)
	for _, transaction := range GenesisBlock.Transactions {
		for _, siacoinOutput := range transaction.SiacoinOutputs {
			numGenesisSiacoins = numGenesisSiacoins.Add(siacoinOutput.Value)
		}
	}
	return numGenesisSiacoins
}()

const (
	// Blake256r14InputSize is a size of input of Blake256r14 hash function.
	Blake256r14InputSize = 180

	// Blake256r14NoncePosition is a position of 8 byte nonce in Blake256r14 input, bytes, 0-based.
	Blake256r14NoncePosition = 140
)

// Blake256r14Input fills input for BLAKE256r14 hash of block header.
// Nonce is located at bytes [140, 148) of the input.
func (h BlockHeader) Blake256r14Input(input *[Blake256r14InputSize]byte) {
	// Layout of original buffer.
	// [0, 32) prev_block_id
	// [32, 40) nonce
	// [40, 48) timestamp
	// [48, 80) merkle_root
	var origBytes [80]byte
	origBuf := bytes.NewBuffer(origBytes[:0])
	encoding.NewEncoder(origBuf).Encode(h)
	src := origBuf.Bytes()
	if len(src) != 80 {
		panic(fmt.Errorf("expected otiginal input to be 80 bytes, got %d", len(src)))
	}

	// Layout of BLAKE256r14 input.
	// [0, 4)     zeros
	// [4, 36)    prev_block_id
	// [36, 68)   merkle_root
	// [68, 76)   timestamp, encoded as little endian.
	// [76, 140)  zeros
	// [140, 148) nonce
	// [148, 180) zeros
	dst := (*input)[:]
	copy(dst[4:36], src[0:32])                                                 // prev_block_id
	copy(dst[36:68], src[48:80])                                               // merkle_root
	copy(dst[Blake256r14NoncePosition:Blake256r14NoncePosition+8], src[32:40]) // nonce
	copy(dst[68:76], src[40:48])                                               // timestamp
}

// FromBlake256r14Input fills the header from a (solved) input of BLAKE256r14.
func (h *BlockHeader) FromBlake256r14Input(inputPtr *[Blake256r14InputSize]byte) error {
	input := (*inputPtr)[:]

	var encoded [80]byte
	copy(encoded[0:32], input[4:36])                                                 // prev_block_id
	copy(encoded[48:80], input[36:68])                                               // merkle_root
	copy(encoded[32:40], input[Blake256r14NoncePosition:Blake256r14NoncePosition+8]) // nonce
	copy(encoded[40:48], input[68:76])                                               // timestamp

	return encoding.Unmarshal(encoded[:], h)
}

// IDAndType returns the ID of a Block, which is calculated by hashing the header,
// and type of hash used: if true, it is blake256r14 hash, otherwise blake2b.
func (h BlockHeader) IDAndType() (id BlockID, blake256r14 bool) {
	idBlake2b := BlockID(crypto.HashObject(h))

	if h.ParentID == BlockID(crypto.Hash{}) {
		// Only genesis blocks have empty parent ID.
		// Always use regular Blake2b for genesis blocks.
		return idBlake2b, false
	}

	var blake256r14Input [Blake256r14InputSize]byte
	h.Blake256r14Input(&blake256r14Input)
	idBlake256r14 := BlockID(crypto.HashBytesBlake256r14Rev(blake256r14Input[:]))

	if bytes.Compare(idBlake2b[:], idBlake256r14[:]) <= 0 {
		return idBlake2b, false
	}

	return idBlake256r14, true
}

// ID returns the ID of a Block, which is calculated by hashing the header.
func (h BlockHeader) ID() BlockID {
	id, _ := h.IDAndType()
	return id
}

// CalculateSubsidy takes a block and a height and determines the block
// subsidy.
func (b Block) CalculateSubsidy(height BlockHeight) Currency {
	subsidy := CalculateCoinbase(height)
	for _, txn := range b.Transactions {
		for _, fee := range txn.MinerFees {
			subsidy = subsidy.Add(fee)
		}
	}
	return subsidy
}

// Header returns the header of a block.
func (b Block) Header() BlockHeader {
	return BlockHeader{
		ParentID:   b.ParentID,
		Nonce:      b.Nonce,
		Timestamp:  b.Timestamp,
		MerkleRoot: b.MerkleRoot(),
	}
}

// ID returns the ID of a Block, which is calculated by hashing the
// concatenation of the block's parent's ID, nonce, and the result of the
// b.MerkleRoot(). It is equivalent to calling block.Header().ID()
func (b Block) ID() BlockID {
	return b.Header().ID()
}

// MerkleRoot calculates the Merkle root of a Block. The leaves of the Merkle
// tree are composed of the miner outputs (one leaf per payout), and the
// transactions (one leaf per transaction).
func (b Block) MerkleRoot() crypto.Hash {
	tree := crypto.NewTree()
	var buf bytes.Buffer
	e := encoding.NewEncoder(&buf)
	for _, payout := range b.MinerPayouts {
		payout.MarshalSia(e)
		tree.Push(buf.Bytes())
		buf.Reset()
	}
	for _, txn := range b.Transactions {
		txn.MarshalSia(e)
		tree.Push(buf.Bytes())
		buf.Reset()
	}
	return tree.Root()
}

// MinerPayoutID returns the ID of the miner payout at the given index, which
// is calculated by hashing the concatenation of the BlockID and the payout
// index.
func (b Block) MinerPayoutID(i uint64) SiacoinOutputID {
	return SiacoinOutputID(crypto.HashAll(
		b.ID(),
		i,
	))
}

// CalculateMinerFees calculates the sum of a block's miner transaction fees
func (b Block) CalculateMinerFees() Currency {
	fees := NewCurrency64(0)
	for _, txn := range b.Transactions {
		for _, fee := range txn.MinerFees {
			fees = fees.Add(fee)
		}
	}
	return fees
}

// CalculateDevSubsidy takes a block and a height and determines the block
// subsidies for the dev fund.
func CalculateDevSubsidy(height BlockHeight) Currency {
	coinbase := CalculateCoinbase(height)

	devSubsidy := NewCurrency64(0)
	if DevFundEnabled && (height >= DevFundInitialBlockHeight) {
		devFundDecayPercentage := uint64(100)
		if height >= DevFundDecayEndBlockHeight {
			devFundDecayPercentage = uint64(0)
		} else if height >= DevFundDecayStartBlockHeight {
			devFundDecayStartBlockHeight := uint64(DevFundDecayStartBlockHeight)
			devFundDecayEndBlockHeight := uint64(DevFundDecayEndBlockHeight)
			devFundDecayPercentage = uint64(100) - (uint64(height)-devFundDecayStartBlockHeight)*uint64(100)/(devFundDecayEndBlockHeight-devFundDecayStartBlockHeight)
		}

		devFundPercentageRange := DevFundInitialPercentage - DevFundFinalPercentage
		devFundPercentage := DevFundFinalPercentage*uint64(100) + devFundPercentageRange*devFundDecayPercentage

		devSubsidy = coinbase.Mul(NewCurrency64(devFundPercentage)).Div(NewCurrency64(10000))
	}

	return devSubsidy
}

// CalculateSubsidies takes a block and a height and determines the block
// subsidies for miners and the dev fund.
func (b Block) CalculateSubsidies(height BlockHeight) (Currency, Currency) {
	coinbase := CalculateCoinbase(height)
	devSubsidy := CalculateDevSubsidy(height)
	minerSubsidy := coinbase.Sub(devSubsidy).Add(b.CalculateMinerFees())
	return minerSubsidy, devSubsidy
}
