package types

import (
	"math/big"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/NebulousLabs/encoding"
	"gitlab.com/scpcorp/ScPrime/crypto"
)

// TestCalculateCoinbase probes the CalculateCoinbase function. The test code
// is probably too similar to the function code to be of value.
func TestCalculateCoinbase(t *testing.T) {
	c := CalculateCoinbase(0)
	if c.Cmp(NewCurrency64(InitialCoinbase).Mul(SiacoinPrecision)) != 0 {
		t.Error("Unexpected CalculateCoinbase result")
	}

	c = CalculateCoinbase(1)
	if c.Cmp(NewCurrency64(InitialCoinbase-1).Mul(SiacoinPrecision)) != 0 {
		t.Error("Unexpected CalculateCoinbase result")
	}

	c = CalculateCoinbase(9)
	if c.Cmp(NewCurrency64(InitialCoinbase-9).Mul(SiacoinPrecision)) != 0 {
		t.Error("Unexpected CalculateCoinbase result")
	}

	c = CalculateCoinbase(125000)
	if c.Cmp(NewCurrency64(FixedCoinbase).Mul(SiacoinPrecision)) != 0 {
		t.Error(c)
		t.Error(NewCurrency64(FixedCoinbase).Mul(SiacoinPrecision))
		t.Error("Unexpected CalculateCoinbase result")
	}

	c = CalculateCoinbase(1000000000)
	if c.Cmp(NewCurrency64(1)) != 0 {
		t.Error(c)
		t.Error(NewCurrency64(1))
		t.Error("Unexpected CalculateCoinbase result")
	}
}

func TestCalculateCoinbasePriorBlakeFork(t *testing.T) {
	// Check that coinbase function does not change for heights prior to fork.
	for i := BlockHeight(0); i < Blake256r14HardforkHeight; i++ {
		require.Equal(t, preForkCoinbase(i), CalculateCoinbase(i))
	}
}

func TestCalculateNumSiacoinsPriorBlakeFork(t *testing.T) {
	// Check that total supply does not change for heights prior to fork.
	for i := BlockHeight(0); i < Blake256r14HardforkHeight; i++ {
		require.Equal(t, preForkCalculateNumSiacoins(i), CalculateNumSiacoins(i))
	}
}

// TestCalculateNumSiacoins checks that the siacoin calculator is correctly
// determining the number of siacoins in circulation. The check is performed by
// doing a naive computation, instead of by doing the optimized computation.
func TestCalculateNumSiacoins(t *testing.T) {
	c := CalculateNumSiacoins(0)
	if c.Cmp(CalculateCoinbase(0).Add(AirdropCommunityValue).Add(AirdropNebulousLabsValue).Add(AirdropPoolValue).Add(AirdropSiaPrimeValue)) != 0 {
		t.Error("unexpected circulation result for value 0, got", c)
	}

	if testing.Short() {
		t.SkipNow()
	}
	//totalCoins := NewCurrency64(0).Add(numGenesisSiacoins) // TODO: check this
	totalCoins := NewCurrency64(0).Add(AirdropCommunityValue).Add(AirdropNebulousLabsValue).Add(AirdropPoolValue).Add(AirdropSiaPrimeValue)

	for i := BlockHeight(0); i < 2500e3; i++ {
		totalCoins = totalCoins.Add(CalculateCoinbase(i))
		if totalCoins.Cmp(CalculateNumSiacoins(i)) != 0 {
			t.Fatal("coin miscalculation", i, totalCoins, CalculateNumSiacoins(i))
		}
	}
}

// TestBlockHeader checks that BlockHeader returns the correct value, and that
// the hash is consistent with the old method for obtaining the hash.
func TestBlockHeader(t *testing.T) {
	cases := []struct {
		name     string
		b        Block
		id       string
		blake2id string
	}{
		{
			name: "blake2",
			b: Block{
				ParentID:     BlockID{0, 0, 10},
				Nonce:        BlockNonce{0, 0, 2},
				Timestamp:    3,
				MinerPayouts: []SiacoinOutput{{Value: NewCurrency64(4)}},
				Transactions: []Transaction{{ArbitraryData: [][]byte{{'5'}}}},
			},
			id:       "9abb7f0d6705274662491d7831fe4bae913f88aa4762a339a956ba6adc05c112",
			blake2id: "9abb7f0d6705274662491d7831fe4bae913f88aa4762a339a956ba6adc05c112",
		},
		{
			name: "blake256r14",
			b: Block{
				ParentID:     BlockID{0, 1},
				Nonce:        BlockNonce{0, 0, 2},
				Timestamp:    3,
				MinerPayouts: []SiacoinOutput{{Value: NewCurrency64(4)}},
				Transactions: []Transaction{{ArbitraryData: [][]byte{{'5'}}}},
			},
			id:       "9237d00bc299d2fc880b23520a7b0703509964b88ef67dee0c35918b4a0c4cb0",
			blake2id: "eb8d4aa23f50fee904c8d776286a24b0045b5a16a703d6c80aa8c9bbed18f99c",
		},
	}

	for _, tc := range cases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			id1 := tc.b.ID()
			id2 := BlockID(crypto.HashBytes(encoding.Marshal(tc.b.Header())))
			id3 := BlockID(crypto.HashAll(
				tc.b.ParentID,
				tc.b.Nonce,
				tc.b.Timestamp,
				tc.b.MerkleRoot(),
			))

			require.Equal(t, tc.id, id1.String())
			require.Equal(t, id2, id3)
			require.Equal(t, tc.blake2id, id2.String())
		})
	}
}

func TestBlake256r14Input(t *testing.T) {
	h := BlockHeader{
		ParentID: BlockID{
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
			17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
		},
		Nonce:     BlockNonce{40, 41, 42, 43, 43, 45, 46, 47},
		Timestamp: Timestamp(0x3233343536373839), // 50-57.
		MerkleRoot: crypto.Hash{
			60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,
			76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,
		},
	}

	var got [Blake256r14InputSize]byte
	h.Blake256r14Input(&got)

	want := [Blake256r14InputSize]byte{
		// [0, 4)     zeros
		0, 0, 0, 0,

		// [4, 36)    prev_block_id
		1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
		17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,

		// [36, 68)   merkle_root
		60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,
		76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91,

		// [68, 76) timestamp. Encoded as little endian.
		57, 56, 55, 54, 53, 52, 51, 50,

		// [76, 140)  - (64 zeros)
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16 zeros.
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16 zeros.
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16 zeros.
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16 zeros.

		// [140, 148) nonce
		40, 41, 42, 43, 43, 45, 46, 47,

		// [148, 180) - (32 zeros)
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16 zeros.
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 16 zeros.
	}

	require.Equal(t, want, got)

	// Decode.
	var h2 BlockHeader
	require.NoError(t, h2.FromBlake256r14Input(&got))

	require.Equal(t, h, h2)
}

// TestBlockID probes the ID function of the block type.
func TestBlockID(t *testing.T) {
	// Create a bunch of different blocks and check that all of them have
	// unique ids.
	var b Block
	var ids []BlockID

	ids = append(ids, b.ID())
	b.ParentID[0] = 1
	ids = append(ids, b.ID())
	b.Nonce[0] = 45
	ids = append(ids, b.ID())
	b.Timestamp = CurrentTimestamp()
	ids = append(ids, b.ID())
	b.MinerPayouts = append(b.MinerPayouts, SiacoinOutput{Value: CalculateCoinbase(0)})
	ids = append(ids, b.ID())
	b.MinerPayouts = append(b.MinerPayouts, SiacoinOutput{Value: CalculateCoinbase(0)})
	ids = append(ids, b.ID())
	b.Transactions = append(b.Transactions, Transaction{MinerFees: []Currency{CalculateCoinbase(1)}})
	ids = append(ids, b.ID())
	b.Transactions = append(b.Transactions, Transaction{MinerFees: []Currency{CalculateCoinbase(1)}})
	ids = append(ids, b.ID())

	knownIDs := make(map[BlockID]struct{})
	for i, id := range ids {
		_, exists := knownIDs[id]
		if exists {
			t.Error("id repeat for index", i)
		}
		knownIDs[id] = struct{}{}
	}
}

// TestHeaderID probes the ID function of the BlockHeader type.
func TestHeaderID(t *testing.T) {
	// Create a bunch of different blocks and check that all of them have
	// unique ids.
	var blocks []Block
	var b Block

	blocks = append(blocks, b)
	b.ParentID[0] = 1
	blocks = append(blocks, b)
	b.Nonce[0] = 45
	blocks = append(blocks, b)
	b.Timestamp = CurrentTimestamp()
	blocks = append(blocks, b)
	b.MinerPayouts = append(b.MinerPayouts, SiacoinOutput{Value: CalculateCoinbase(0)})
	blocks = append(blocks, b)
	b.MinerPayouts = append(b.MinerPayouts, SiacoinOutput{Value: CalculateCoinbase(0)})
	blocks = append(blocks, b)
	b.Transactions = append(b.Transactions, Transaction{MinerFees: []Currency{CalculateCoinbase(1)}})
	blocks = append(blocks, b)
	b.Transactions = append(b.Transactions, Transaction{MinerFees: []Currency{CalculateCoinbase(1)}})
	blocks = append(blocks, b)

	knownIDs := make(map[BlockID]struct{})
	for i, block := range blocks {
		blockID := block.ID()
		headerID := block.Header().ID()
		if blockID != headerID {
			t.Error("headerID does not match blockID for index", i)
		}
		_, exists := knownIDs[headerID]
		if exists {
			t.Error("id repeat for index", i)
		}
		knownIDs[headerID] = struct{}{}
	}
}

// TestBlockCalculateSubsidy probes the CalculateSubsidy function of the block
// type.
func TestBlockCalculateSubsidy(t *testing.T) {
	// All tests are done at height = 0.
	coinbase := CalculateCoinbase(0)

	// Calculate the subsidy on a block with 0 fees at height 0. Result should
	// be 300,000.
	var b Block
	minerSubsidy, devSubsidy := b.CalculateSubsidies(0)
	if (minerSubsidy.Add(devSubsidy)).Cmp(coinbase) != 0 {
		t.Error("subsidy is miscalculated for an empty block")
	}

	// Calculate when there is a fee in a transcation.
	expected := coinbase.Add(NewCurrency64(123))
	txn := Transaction{
		MinerFees: []Currency{NewCurrency64(123)},
	}
	b.Transactions = append(b.Transactions, txn)
	minerSubsidy, devSubsidy = b.CalculateSubsidies(0)
	if (minerSubsidy.Add(devSubsidy)).Cmp(expected) != 0 {
		t.Error("subsidy is miscalculated for a block with a single transaction")
	}

	// Add a single no-fee transaction and check again.
	txn = Transaction{
		ArbitraryData: [][]byte{{'6'}},
	}
	b.Transactions = append(b.Transactions, txn)
	minerSubsidy, devSubsidy = b.CalculateSubsidies(0)
	if (minerSubsidy.Add(devSubsidy)).Cmp(expected) != 0 {
		t.Error("subsidy is miscalculated with empty transactions.")
	}

	// Add a transaction with multiple fees.
	expected = expected.Add(NewCurrency64(1 + 2 + 3))
	txn = Transaction{
		MinerFees: []Currency{
			NewCurrency64(1),
			NewCurrency64(2),
			NewCurrency64(3),
		},
	}
	b.Transactions = append(b.Transactions, txn)
	minerSubsidy, devSubsidy = b.CalculateSubsidies(0)
	if (minerSubsidy.Add(devSubsidy)).Cmp(expected) != 0 {
		t.Error("subsidy is miscalculated for a block with a single transaction")
	}

	// Add an empty transaction to the beginning.
	txn = Transaction{
		ArbitraryData: [][]byte{{'7'}},
	}
	b.Transactions = append([]Transaction{txn}, b.Transactions...)
	minerSubsidy, devSubsidy = b.CalculateSubsidies(0)
	if (minerSubsidy.Add(devSubsidy)).Cmp(expected) != 0 {
		t.Error("subsidy is miscalculated with empty transactions.")
	}
}

// TestCalculateDevSubsidy probes the CalculateDevSubsidy function.
func TestCalculateDevSubsidy(t *testing.T) {
	cases := []struct {
		height BlockHeight
		want   string
	}{
		{0, "0"},
		{1, "59999800000000000000000000000"},
		{2, "59999600000000000000000000000"},
		{3, "59999400000000000000000000000"},
		{10, "59998000000000000000000000000"},
		{1000, "59998000000000000000000000000"},
		{1000000, "0"},
	}
	for _, tc := range cases {
		got := CalculateDevSubsidy(tc.height)
		wantInt, ok := new(big.Int).SetString(tc.want, 10)
		if !ok {
			t.Fatalf("failed to parse number %s", tc.want)
		}
		want := NewCurrency(wantInt)
		if !got.Equals(want) {
			t.Errorf("CalculateDevSubsidy(%d) returned %s, want %s", tc.height, got, want)
		}
	}
}

// TestBlockMinerPayoutID probes the MinerPayout function of the block type.
func TestBlockMinerPayoutID(t *testing.T) {
	// Create a block with 2 miner payouts, and check that each payout has a
	// different id, and that the id is dependent on the block id.
	var ids []SiacoinOutputID
	b := Block{
		MinerPayouts: []SiacoinOutput{
			{Value: CalculateCoinbase(0)},
			{Value: CalculateCoinbase(0)},
		},
	}
	ids = append(ids, b.MinerPayoutID(1), b.MinerPayoutID(2))
	b.ParentID[0] = 1
	ids = append(ids, b.MinerPayoutID(1), b.MinerPayoutID(2))

	knownIDs := make(map[SiacoinOutputID]struct{})
	for i, id := range ids {
		_, exists := knownIDs[id]
		if exists {
			t.Error("id repeat for index", i)
		}
		knownIDs[id] = struct{}{}
	}
}
