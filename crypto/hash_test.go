package crypto

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"sort"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/NebulousLabs/fastrand"
)

type (
	// TestObject is a struct that's used for testing HashAll and HashObject. The
	// fields have to be exported so the encoder can read them.
	TestObject struct {
		A int
		B byte
		C bool
		D string
	}
)

// TestHashing uses each of the functions in hash.go and verifies that the
// results are as expected.
func TestHashing(t *testing.T) {
	// Create a test object.
	to := TestObject{
		A: 12345,
		B: 5,
		C: true,
		D: "testing",
	}

	// Call HashObject on the object.
	var emptyHash Hash
	h0 := HashObject(to)
	if h0 == emptyHash {
		t.Error("HashObject returned the zero hash!")
	}

	// Call HashAll on the test object and some other fields.
	h1 := HashAll(
		int(122),
		byte(115),
		string("test"),
		to,
	)
	if h1 == emptyHash {
		t.Error("HashAll returned the zero hash!")
	}

	// Call HashBytes on a random byte slice.
	h2 := HashBytes(fastrand.Bytes(435))
	if h2 == emptyHash {
		t.Error("HashBytes returned the zero hash!")
	}
}

// TestHashSorting takes a set of hashses and checks that they can be sorted.
func TestHashSorting(t *testing.T) {
	// Created an unsorted list of hashes.
	hashes := make([]Hash, 5)
	hashes[0][0] = 12
	hashes[1][0] = 7
	hashes[2][0] = 13
	hashes[3][0] = 14
	hashes[4][0] = 1

	// Sort the hashes.
	sort.Sort(HashSlice(hashes))
	if hashes[0][0] != 1 {
		t.Error("bad sort")
	}
	if hashes[1][0] != 7 {
		t.Error("bad sort")
	}
	if hashes[2][0] != 12 {
		t.Error("bad sort")
	}
	if hashes[3][0] != 13 {
		t.Error("bad sort")
	}
	if hashes[4][0] != 14 {
		t.Error("bad sort")
	}
}

// TestUnitHashMarshalJSON tests that Hashes are correctly marshalled to JSON.
func TestUnitHashMarshalJSON(t *testing.T) {
	h := HashObject("an object")
	jsonBytes, err := h.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(jsonBytes, []byte(`"`+h.String()+`"`)) {
		t.Errorf("hash %s encoded incorrectly: got %s\n", h, jsonBytes)
	}
}

// TestUnitHashUnmarshalJSON tests that unmarshalling invalid JSON will result
// in an error.
func TestUnitHashUnmarshalJSON(t *testing.T) {
	// Test unmarshalling invalid data.
	invalidJSONBytes := [][]byte{
		// Invalid JSON.
		nil,
		{},
		[]byte("\""),
		// JSON of wrong length.
		[]byte(""),
		[]byte(`"` + strings.Repeat("a", HashSize*2-1) + `"`),
		[]byte(`"` + strings.Repeat("a", HashSize*2+1) + `"`),
		// JSON of right length but invalid Hashes.
		[]byte(`"` + strings.Repeat("z", HashSize*2) + `"`),
		[]byte(`"` + strings.Repeat(".", HashSize*2) + `"`),
		[]byte(`"` + strings.Repeat("\n", HashSize*2) + `"`),
	}

	for _, jsonBytes := range invalidJSONBytes {
		var h Hash
		err := h.UnmarshalJSON(jsonBytes)
		if err == nil {
			t.Errorf("expected unmarshall to fail on the invalid JSON: %q\n", jsonBytes)
		}
	}

	// Test unmarshalling valid data.
	expectedH := HashObject("an object")
	jsonBytes := []byte(`"` + expectedH.String() + `"`)

	var h Hash
	err := h.UnmarshalJSON(jsonBytes)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(h[:], expectedH[:]) {
		t.Errorf("Hash %s marshalled incorrectly: got %s\n", expectedH, h)
	}
}

// TestHashMarshalling checks that the marshalling of the hash type works as
// expected.
func TestHashMarshalling(t *testing.T) {
	h := HashObject("an object")
	hBytes, err := json.Marshal(h)
	if err != nil {
		t.Fatal(err)
	}

	var uMarH Hash
	err = uMarH.UnmarshalJSON(hBytes)
	if err != nil {
		t.Fatal(err)
	}

	if h != uMarH {
		t.Error("encoded and decoded hash do not match!")
	}
}

// TestHashMarshalUnmarshalText checks Hash's MarshalText and UnmarshalText methods.
func TestHashMarshalUnmarshalText(t *testing.T) {
	h := HashObject("an object")
	hBytes, err := h.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var uMarH Hash
	err = uMarH.UnmarshalText(hBytes)
	if err != nil {
		t.Fatal(err)
	}

	if h != uMarH {
		t.Error("encoded and decoded hash do not match!")
	}
}

// TestHashLoadString checks that the LoadString method of the hash function is
// working properly.
func TestHashLoadString(t *testing.T) {
	h1 := Hash{}
	h2 := HashObject("tame")
	h1e := h1.String()
	h2e := h2.String()

	var h1d, h2d Hash
	err := h1d.LoadString(h1e)
	if err != nil {
		t.Fatal(err)
	}
	err = h2d.LoadString(h2e)
	if err != nil {
		t.Fatal(err)
	}
	if h1d != h1 {
		t.Error("decoding h1 failed")
	}
	if h2d != h2 {
		t.Error("decoding h2 failed")
	}

	// Try some bogus strings.
	h1e = h1e + "a"
	err = h1.LoadString(h1e)
	if err == nil {
		t.Fatal("expecting error when decoding hash of too large length")
	}
	h1e = h1e[:60]
	err = h1.LoadString(h1e)
	if err == nil {
		t.Fatal("expecting error when decoding hash of too small length")
	}
}

func TestConvertBytesHashes(t *testing.T) {
	var arr [][]byte
	for i := 0; i < 10; i++ {
		item := make([]byte, 32)
		item[0] = byte(i)
		arr = append(arr, item)
	}
	converted := ConvertBytesToHashes(arr)
	back := ConvertHashesToByteSlices(converted)
	require.Equal(t, arr, back)
}

func TestHashBytesBlake256r14(t *testing.T) {
	type blakeVector struct {
		out, in string
	}

	// Source: https://github.com/dchest/blake256/blob/master/blake256_test.go
	var vectors256 = []blakeVector{
		{"7576698ee9cad30173080678e5965916adbb11cb5245d386bf1ffda1cb26c9d7",
			"The quick brown fox jumps over the lazy dog"},
		{"07663e00cf96fbc136cf7b1ee099c95346ba3920893d18cc8851f22ee2e36aa6",
			"BLAKE"},
		{"716f6e863f744b9ac22c97ec7b76ea5f5908bc5b2f67c61510bfc4751384ea7a",
			""},
		{"18a393b4e62b1887a2edf79a5c5a5464daf5bbb976f4007bea16a73e4c1e198e",
			"'BLAKE wins SHA-3! Hooray!!!' (I have time machine)"},
		{"fd7282ecc105ef201bb94663fc413db1b7696414682090015f17e309b835f1c2",
			"Go"},
		{"1e75db2a709081f853c2229b65fd1558540aa5e7bd17b04b9a4b31989effa711",
			"HELP! I'm trapped in hash!"},
		{"4181475cb0c22d58ae847e368e91b4669ea2d84bcd55dbf01fe24bae6571dd08",
			`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.`, //nolint:misspell
		},
		{"af95fffc7768821b1e08866a2f9f66916762bfc9d71c4acb5fd515f31fd6785a", // test with one padding byte
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congu",
		},
	}

	for _, tc := range vectors256 {
		tc := tc
		t.Run(tc.out, func(t *testing.T) {
			got := HashBytesBlake256r14([]byte(tc.in))
			require.Equal(t, tc.out, hex.EncodeToString(got[:]))
		})
	}
}
