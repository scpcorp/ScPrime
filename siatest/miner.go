package siatest

import (
	"bytes"
	"unsafe"

	"gitlab.com/NebulousLabs/encoding"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/types"
)

// emptyBlockForWork creates an empty block without any transactions.
func emptyBlockForWork(currentBlockHeight types.BlockHeight, address types.UnlockHash, parentID types.BlockID) types.Block {
	devSubsidy := types.CalculateDevSubsidy(currentBlockHeight + 1)
	minerReward := types.CalculateCoinbase(currentBlockHeight + 1).Sub(devSubsidy)
	var b types.Block
	b.ParentID = parentID
	b.Timestamp = types.CurrentTimestamp()
	b.MinerPayouts = []types.SiacoinOutput{
		{Value: minerReward, UnlockHash: address},
		{Value: devSubsidy, UnlockHash: types.DevFundUnlockHash}}
	return b
}

// solveHeader solves the header by finding a nonce for the target
func solveHeader(target types.Target, bh types.BlockHeader, height types.BlockHeight) (types.BlockHeader, error) {
	var nonce uint64

	allowBlake256r14 := height >= types.Blake256r14HardforkHeight
	allowBlake2b := height < types.Blake256r14SoftforkHeight

	blake256 := allowBlake256r14
	if allowBlake256r14 && allowBlake2b {
		// If both hashes are allowed, use a random one, to test edge cases.
		blake256 = fastrand.Intn(2) == 0
	}

	if !blake256 {
		// Old Blake2b hash.
		header := encoding.Marshal(bh)
		for i := 0; i < 256; i++ {
			id := crypto.HashBytes(header)
			if bytes.Compare(target[:], id[:]) >= 0 {
				copy(bh.Nonce[:], header[32:40])
				if bh.ID() == types.BlockID(id) {
					// Check this to make sure it is Blake2b ID.
					// Accidentally blake256r14 hash can be smaller.
					return bh, nil
				}
			}
			*(*uint64)(unsafe.Pointer(&header[32])) = nonce
			nonce += types.ASICHardforkFactor
		}
	} else {
		// New Blake256r14 hash.
		var input [types.Blake256r14InputSize]byte
		bh.Blake256r14Input(&input)
		inputSlice := input[:]
		for i := 0; i < 256; i++ {
			id := crypto.HashBytesBlake256r14Rev(inputSlice)
			if bytes.Compare(target[:], id[:]) >= 0 {
				copy(bh.Nonce[:], input[types.Blake256r14NoncePosition:types.Blake256r14NoncePosition+8])
				if bh.ID() == types.BlockID(id) {
					// Check this to make sure it is Blake256r14 ID.
					// Accidentally blake2b hash can be smaller.
					return bh, nil
				}
			}
			*(*uint64)(unsafe.Pointer(&input[types.Blake256r14NoncePosition])) = nonce
			nonce++
		}
	}

	return bh, errors.New("couldn't solve block")
}

// MineBlock makes the underlying node mine a single block and broadcast it.
func (tn *TestNode) MineBlock() error {
	// Get the header
	target, header, err := tn.MinerHeaderGet()
	if err != nil {
		return errors.AddContext(err, "failed to get header for work")
	}
	// Get the current blockheight.
	bh, err := tn.BlockHeight()
	if err != nil {
		return errors.AddContext(err, "failed to get current blockheight")
	}
	// Solve the header
	header, err = solveHeader(target, header, bh)
	if err != nil {
		return errors.AddContext(err, "failed to solve header")
	}
	// Submit the header
	if err := tn.MinerHeaderPost(header); err != nil {
		return errors.AddContext(err, "failed to submit header")
	}
	return nil
}

// MineEmptyBlock mines an empty block without any transactions and broadcasts
// it.
func (tn *TestNode) MineEmptyBlock() error {
	// Get the current target.
	target, _, err := tn.MinerHeaderGet()
	if err != nil {
		return errors.AddContext(err, "failed to get target")
	}
	// Get the current blockheight.
	bh, err := tn.BlockHeight()
	if err != nil {
		return errors.AddContext(err, "failed to get current blockheight")
	}
	// Get the most recent block.
	cbg, err := tn.ConsensusBlocksHeightGet(bh)
	if err != nil {
		return errors.AddContext(err, "failed to get most recent block")
	}
	// Get a payout address.
	wag, err := tn.WalletAddressGet()
	if err != nil {
		return errors.AddContext(err, "failed to get new wallet address")
	}
	// Get the block.
	b := emptyBlockForWork(bh, wag.Address, cbg.ID)
	// Solve the block.
	header, err := solveHeader(target, b.Header(), bh)
	if err != nil {
		return errors.AddContext(err, "failed to solve block header")
	}
	b.Nonce = header.Nonce
	// Submit block.
	return errors.AddContext(tn.MinerBlockPost(b), "failed to submit block")
}
