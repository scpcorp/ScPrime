package miner

// testminer.go implements the TestMiner interface, whose primary purpose is
// integration testing.

import (
	"bytes"
	"encoding/binary"
	"errors"
	"unsafe"

	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

const (
	// solveAttempts is the number of times that SolveBlock will try to solve a
	// block before giving up.
	solveAttempts = 16e3
)

// solveBlock takes a block and a target and tries to solve the block for the
// target. A bool is returned indicating whether the block was successfully
// solved.
func solveBlock(b types.Block, target types.Target, height types.BlockHeight) (types.Block, bool) {
	// Assemble the header.
	merkleRoot := b.MerkleRoot()

	newBlockHeight := height + 1
	allowBlake256r14 := newBlockHeight >= types.Blake256r14HardforkHeight
	allowBlake2b := newBlockHeight < types.Blake256r14SoftforkHeight

	blake256 := allowBlake256r14
	if allowBlake256r14 && allowBlake2b {
		// If both hashes are allowed, use a random one, to test edge cases.
		blake256 = fastrand.Intn(2) == 0
	}

	if !blake256 {
		// Old Blake2b hash.
		header := make([]byte, 80)
		copy(header, b.ParentID[:])
		binary.LittleEndian.PutUint64(header[40:48], uint64(b.Timestamp))
		copy(header[48:], merkleRoot[:])
		var nonce uint64
		noncePtr := (*uint64)(unsafe.Pointer(&header[32]))
		for i := 0; i < solveAttempts; i++ {
			id := crypto.HashBytes(header)
			if bytes.Compare(target[:], id[:]) >= 0 {
				copy(b.Nonce[:], header[32:40])
				if b.ID() == types.BlockID(id) {
					// Check this to make sure it is Blake2b ID.
					// Accidentally blake256r14 hash can be smaller.
					return b, true
				}
			}
			*noncePtr = nonce
			nonce += types.ASICHardforkFactor
		}
	} else {
		// New Blake256r14 hash.
		var input [types.Blake256r14InputSize]byte
		b.Header().Blake256r14Input(&input)
		inputSlice := input[:]
		var nonce uint64
		noncePtr := (*uint64)(unsafe.Pointer(&input[types.Blake256r14NoncePosition]))
		for i := 0; i < solveAttempts; i++ {
			id := crypto.HashBytesBlake256r14Rev(inputSlice)
			if bytes.Compare(target[:], id[:]) >= 0 {
				copy(b.Nonce[:], inputSlice[types.Blake256r14NoncePosition:types.Blake256r14NoncePosition+8])
				if b.ID() == types.BlockID(id) {
					// Check this to make sure it is Blake256r14 ID.
					// Accidentally blake2b hash can be smaller.
					return b, true
				}
			}
			*noncePtr = nonce
			nonce++
		}
	}

	return b, false
}

// BlockForWork returns a block that is ready for nonce grinding, along with
// the root hash of the block.
func (m *Miner) BlockForWork() (b types.Block, t types.Target, err error) {
	// Check if the wallet is unlocked. If the wallet is unlocked, make sure
	// that the miner has a recent address.
	unlocked, err := m.wallet.Unlocked()
	if err != nil {
		return types.Block{}, types.Target{}, err
	}
	if !unlocked {
		err = modules.ErrLockedWallet
		return
	}
	m.mu.Lock()
	defer m.mu.Unlock()
	err = m.checkAddress()
	if err != nil {
		return
	}

	b = m.blockForWork()
	return b, m.persist.Target, nil
}

// AddBlock adds a block to the consensus set.
func (m *Miner) AddBlock() (types.Block, error) {
	block, err := m.FindBlock()
	if err != nil {
		return types.Block{}, err
	}
	err = m.cs.AcceptBlock(block)
	if err != nil {
		return types.Block{}, err
	}
	return block, nil
}

// FindBlock finds at most one block that extends the current blockchain.
func (m *Miner) FindBlock() (types.Block, error) {
	var bfw types.Block
	var target types.Target
	var height types.BlockHeight
	err := func() error {
		m.mu.Lock()
		defer m.mu.Unlock()

		unlocked, err := m.wallet.Unlocked()
		if err != nil {
			return err
		}
		if !unlocked {
			return modules.ErrLockedWallet
		}
		err = m.checkAddress()
		if err != nil {
			return err
		}

		// Get a block for work.
		bfw = m.blockForWork()
		target = m.persist.Target
		height = m.persist.Height
		return nil
	}()
	if err != nil {
		return types.Block{}, err
	}

	block, ok := m.SolveBlock(bfw, target, height)
	if !ok {
		return types.Block{}, errors.New("could not solve block using limited hashing power")
	}
	return block, nil
}

// SolveBlock takes a block and a target and tries to solve the block for the
// target. A bool is returned indicating whether the block was successfully
// solved.
func (m *Miner) SolveBlock(b types.Block, target types.Target, height types.BlockHeight) (types.Block, bool) {
	return solveBlock(b, target, height)
}
