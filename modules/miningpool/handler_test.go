package pool

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/scpcorp/ScPrime/crypto"
)

func TestReverseHash4Bytes(t *testing.T) {
	inputStr := "00000044fc1189b3ad71bc1f0efd006da476465859d73002971f8885911fe74b"
	expected := "44000000b38911fc1fbc71ad6d00fd0e584676a40230d75985881f974be71f91"
	var hash crypto.Hash
	require.NoError(t, hash.LoadString(inputStr))
	reverseHash4Bytes(&hash)
	require.Equal(t, expected, hash.String())
}
