package wallet

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/modules/miner"
	"gitlab.com/scpcorp/ScPrime/modules/wallet/mock_transporter_client"
	"gitlab.com/scpcorp/ScPrime/types"
	transporter "gitlab.com/scpcorp/spf-transporter"
	"gitlab.com/scpcorp/spf-transporter/common"
	"go.uber.org/mock/gomock"
)

func TestSpftransportsPremined(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	ctrl := gomock.NewController(t)
	tc := mock_transporter_client.NewMockTransporterClient(ctrl)
	tcMap := map[types.CurrencyType]TransporterClient{types.Spf: tc}

	var wt *walletTester
	t.Cleanup(func() {
		if wt != nil {
			require.NoError(t, wt.closeWt())
		}
	})

	fundAddress := types.MustParseAddress("d6a6c5a41dc935ec6aef0a9e7f83148a3fdde61062f7204dd244740cf1591bdfc10dca990dd5")

	t.Run("setup", func(t *testing.T) {
		tc.EXPECT().
			PreminedList(gomock.Any(), gomock.Any()).
			Return(&transporter.PreminedListResponse{
				Premined: map[string]common.PreminedRecord{
					fundAddress.String(): {
						Limit: types.NewCurrency64(3000),
					},
				},
			}, error(nil))

		var err error
		wt, err = createWalletTester("TestSpftransportsPremined", modules.ProdDependencies, WithTransporterClients(tcMap))
		require.NoError(t, err)

		// Load the key into the wallet.
		err = wt.wallet.LoadSiagKeys(wt.walletMasterKey, []string{"../../types/siag0of1of1.siakey"})
		require.NoError(t, err)

		oldBal, err := wt.wallet.ConfirmedBalance()
		require.NoError(t, err)
		require.Equal(t, "2000", oldBal.FundBalance.String())
		require.Equal(t, "0", oldBal.FundbBalance.String())

		// need to reset the miner as well, since it depends on the wallet
		wt.miner, err = miner.New(wt.cs, wt.tpool, wt.wallet, wt.wallet.persistDir)
		require.NoError(t, err)
	})

	t.Run("SiafundTransportAllowance empty", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfA)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Premined: map[string]types.SolanaTransportAllowance{},
		}, allowance)
	})

	t.Run("SiafundTransportAllowance premined", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3500),
					},
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfA)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Premined: map[string]types.SolanaTransportAllowance{
				fundAddress.String(): {
					MaxAllowed:   types.NewCurrency64(2000),
					PotentialMax: types.NewCurrency64(3500),
				},
			},
		}, allowance)
	})

	t.Run("SiafundTransportAllowance spf-b", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3500),
					},
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfB)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Premined: map[string]types.SolanaTransportAllowance{
				fundAddress.String(): {
					MaxAllowed:   types.ZeroCurrency,
					PotentialMax: types.NewCurrency64(3500),
				},
			},
		}, allowance)
	})

	t.Run("SiafundTransportHistory empty", func(t *testing.T) {
		history, err := wt.wallet.SiafundTransportHistory()
		require.NoError(t, err)
		require.Empty(t, history)
	})

	submitTime := types.CurrentTimestamp()
	var tx types.Transaction

	t.Run("SiafundTransportSend 10 funds", func(t *testing.T) {
		spfAmount := types.SpfAmount{
			Amount: types.NewCurrency64(10),
			Type:   types.SpfA,
		}
		solanaAddr := types.SolanaAddress{'T', 'e', 's', 't', 'S', 'o', 'l', 'a', 'n', 'a'}

		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3500),
					},
				},
			}, error(nil))

		tc.EXPECT().
			CheckSolanaAddress(gomock.Any(), &transporter.CheckSolanaAddressRequest{
				SolanaAddress: common.SolanaAddress(solanaAddr),
				Amount:        spfAmount.Amount,
			}).
			Return(&transporter.CheckSolanaAddressResponse{
				CurrentTime: time.Now(),
			}, error(nil))

		tc.EXPECT().
			SubmitScpTx(gomock.Any(), gomock.Any()).
			DoAndReturn(func(_ context.Context, req *transporter.SubmitScpTxRequest) (*transporter.SubmitScpTxResponse, error) {
				tx = req.Transaction
				return &transporter.SubmitScpTxResponse{
					WaitTimeEstimate: 0,
					SpfAmountAhead:   nil,
				}, nil
			})

		res, err := wt.wallet.SiafundTransportSend(spfAmount, types.Premined, &fundAddress, solanaAddr)
		require.NoError(t, err)
		require.Equal(t, time.Duration(0), res.WaitTime)
		require.Nil(t, res.AmountAhead)
	})

	t.Run("SiafundTransportHistory one record", func(t *testing.T) {
		history, err := wt.wallet.SiafundTransportHistory()
		require.NoError(t, err)
		require.Equal(t, []types.SolanaTransport{{
			BurnID: tx.ID(),
			SolanaTransportRecord: types.SolanaTransportRecord{
				Status:  types.SubmittedToTransporter,
				Amount:  types.NewCurrency64(10),
				Created: submitTime,
			},
		}}, history)
	})
}

func TestSpftransportsPreminedB(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	ctrl := gomock.NewController(t)
	tc := mock_transporter_client.NewMockTransporterClient(ctrl)
	tcMap := map[types.CurrencyType]TransporterClient{types.Spf: tc}

	var wt *walletTester
	t.Cleanup(func() {
		if wt != nil {
			require.NoError(t, wt.closeWt())
		}
	})

	fundAddress := types.MustParseAddress("d6a6c5a41dc935ec6aef0a9e7f83148a3fdde61062f7204dd244740cf1591bdfc10dca990dd5")

	t.Run("setup", func(t *testing.T) {
		tc.EXPECT().
			PreminedList(gomock.Any(), gomock.Any()).
			Return(&transporter.PreminedListResponse{
				Premined: map[string]common.PreminedRecord{
					fundAddress.String(): {
						Limit: types.NewCurrency64(3000),
					},
				},
			}, error(nil))

		var err error
		wt, err = createWalletTester("TestSpftransportsPreminedB", modules.ProdDependencies, WithTransporterClients(tcMap))
		require.NoError(t, err)

		// Load the key into the wallet.
		err = wt.wallet.LoadSiagKeys(wt.walletMasterKey, []string{"../../types/siag0of1of1.siakey"})
		require.NoError(t, err)

		oldBal, err := wt.wallet.ConfirmedBalance()
		require.NoError(t, err)
		require.Equal(t, "2000", oldBal.FundBalance.String())
		require.Equal(t, "0", oldBal.FundbBalance.String())

		// Turn SPF-A into SPF-B.
		outputs, err := wt.wallet.UnspentOutputs()
		require.NoError(t, err)
		var outputID types.OutputID
		ok := false
		for _, output := range outputs {
			if output.FundType == types.SpecifierSiafundOutput {
				outputID = output.ID
				ok = true
			}
		}
		require.True(t, ok)
		cst, ok := wt.cs.(interface {
			AddSiafundBOutput(id types.SiafundOutputID) error
		})
		require.True(t, ok)
		require.NoError(t, cst.AddSiafundBOutput(types.SiafundOutputID(outputID)))

		oldBal, err = wt.wallet.ConfirmedBalance()
		require.NoError(t, err)
		require.Equal(t, "0", oldBal.FundBalance.String())
		require.Equal(t, "2000", oldBal.FundbBalance.String())

		// need to reset the miner as well, since it depends on the wallet
		wt.miner, err = miner.New(wt.cs, wt.tpool, wt.wallet, wt.wallet.persistDir)
		require.NoError(t, err)
	})

	t.Run("SiafundTransportAllowance empty", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount: types.ZeroCurrency,
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfA)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Regular: types.SolanaTransportAllowance{
				MaxAllowed:   types.NewCurrency64(0),
				WaitTime:     0 * time.Second,
				PotentialMax: types.NewCurrency64(0),
			},
			Premined: map[string]types.SolanaTransportAllowance{},
		}, allowance)
	})

	t.Run("SiafundTransportAllowance premined spf-b", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount: types.ZeroCurrency,
				},
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3500),
					},
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfB)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Regular: types.SolanaTransportAllowance{
				MaxAllowed:   types.NewCurrency64(0),
				WaitTime:     0 * time.Second,
				PotentialMax: types.NewCurrency64(0),
			},
			Premined: map[string]types.SolanaTransportAllowance{
				fundAddress.String(): {
					MaxAllowed:   types.NewCurrency64(2000),
					PotentialMax: types.NewCurrency64(3500),
				},
			},
		}, allowance)
	})

	t.Run("SiafundTransportAllowance premined spf-a", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount: types.ZeroCurrency,
				},
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3500),
					},
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfA)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Regular: types.SolanaTransportAllowance{
				MaxAllowed:   types.NewCurrency64(0),
				WaitTime:     0 * time.Second,
				PotentialMax: types.NewCurrency64(0),
			},
			Premined: map[string]types.SolanaTransportAllowance{
				fundAddress.String(): {
					MaxAllowed:   types.ZeroCurrency,
					PotentialMax: types.NewCurrency64(3500),
				},
			},
		}, allowance)
	})

	t.Run("SiafundTransportHistory empty", func(t *testing.T) {
		history, err := wt.wallet.SiafundTransportHistory()
		require.NoError(t, err)
		require.Empty(t, history)
	})

	submitTime := types.CurrentTimestamp()
	var tx types.Transaction

	t.Run("SiafundTransportSend 10 funds", func(t *testing.T) {
		spfAmount := types.SpfAmount{
			Amount: types.NewCurrency64(10),
			Type:   types.SpfB,
		}
		solanaAddr := types.SolanaAddress{'T', 'e', 's', 't', 'S', 'o', 'l', 'a', 'n', 'a'}

		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount: types.ZeroCurrency,
				},
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3500),
					},
				},
			}, error(nil))

		tc.EXPECT().
			CheckSolanaAddress(gomock.Any(), &transporter.CheckSolanaAddressRequest{
				SolanaAddress: common.SolanaAddress(solanaAddr),
				Amount:        spfAmount.Amount,
			}).
			Return(&transporter.CheckSolanaAddressResponse{
				CurrentTime: time.Now(),
			}, error(nil))

		tc.EXPECT().
			SubmitScpTx(gomock.Any(), gomock.Any()).
			DoAndReturn(func(_ context.Context, req *transporter.SubmitScpTxRequest) (*transporter.SubmitScpTxResponse, error) {
				tx = req.Transaction
				return &transporter.SubmitScpTxResponse{
					WaitTimeEstimate: 0,
					SpfAmountAhead:   nil,
				}, nil
			})

		res, err := wt.wallet.SiafundTransportSend(spfAmount, types.Premined, &fundAddress, solanaAddr)
		require.NoError(t, err)
		require.Equal(t, time.Duration(0), res.WaitTime)
		require.Nil(t, res.AmountAhead)
	})

	t.Run("SiafundTransportHistory one record", func(t *testing.T) {
		history, err := wt.wallet.SiafundTransportHistory()
		require.NoError(t, err)
		require.Equal(t, []types.SolanaTransport{{
			BurnID: tx.ID(),
			SolanaTransportRecord: types.SolanaTransportRecord{
				Status:  types.SubmittedToTransporter,
				Amount:  types.NewCurrency64(10),
				Created: submitTime,
			},
		}}, history)
	})

	t.Run("confirm", func(t *testing.T) {
		for i := 0; i < 6; i++ {
			_, err := wt.miner.AddBlock()
			require.NoError(t, err)
		}
	})

	t.Run("check balance", func(t *testing.T) {
		bal, err := wt.wallet.ConfirmedBalance()
		require.NoError(t, err)
		require.Equal(t, "0", bal.FundBalance.String())
		require.Equal(t, "1990", bal.FundbBalance.String())
	})

	t.Run("SiafundTransportAllowance after submission", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{
				PreminedUnlockHashes: []types.UnlockHash{fundAddress},
			}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount: types.ZeroCurrency,
				},
				Premined: map[string]transporter.AmountWithTimeEstimate{
					fundAddress.String(): {
						Amount: types.NewCurrency64(3490),
					},
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfB)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Premined: map[string]types.SolanaTransportAllowance{
				fundAddress.String(): {
					MaxAllowed:   types.NewCurrency64(1990),
					PotentialMax: types.NewCurrency64(3490),
				},
			},
		}, allowance)
	})
}

func TestSpftransportsRegular(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	ctrl := gomock.NewController(t)
	tc := mock_transporter_client.NewMockTransporterClient(ctrl)
	tcMap := map[types.CurrencyType]TransporterClient{types.Spf: tc}

	var wt *walletTester
	t.Cleanup(func() {
		if wt != nil {
			require.NoError(t, wt.closeWt())
		}
	})

	t.Run("setup", func(t *testing.T) {
		tc.EXPECT().
			PreminedList(gomock.Any(), gomock.Any()).
			Return(&transporter.PreminedListResponse{}, error(nil))

		var err error
		wt, err = createWalletTester("TestSpftransportsRegular", modules.ProdDependencies, WithTransporterClients(tcMap))
		require.NoError(t, err)

		// Load the key into the wallet.
		err = wt.wallet.LoadSiagKeys(wt.walletMasterKey, []string{"../../types/siag0of1of1.siakey"})
		require.NoError(t, err)

		oldBal, err := wt.wallet.ConfirmedBalance()
		require.NoError(t, err)
		require.Equal(t, "2000", oldBal.FundBalance.String())
		// need to reset the miner as well, since it depends on the wallet
		wt.miner, err = miner.New(wt.cs, wt.tpool, wt.wallet, wt.wallet.persistDir)
		require.NoError(t, err)
	})

	t.Run("SiafundTransportAllowance", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount:       types.NewCurrency64(3500),
					WaitEstimate: 10 * time.Hour,
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfA)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Regular: types.SolanaTransportAllowance{
				MaxAllowed:   types.NewCurrency64(2000),
				WaitTime:     34500 * time.Second,
				PotentialMax: types.NewCurrency64(3500),
			},
			Premined: map[string]types.SolanaTransportAllowance{},
		}, allowance)
	})

	t.Run("SiafundTransportAllowance spf-b", func(t *testing.T) {
		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount:       types.NewCurrency64(3500),
					WaitEstimate: 10 * time.Hour,
				},
			}, error(nil))

		allowance, err := wt.wallet.SiafundTransportAllowance(types.SpfB)
		require.NoError(t, err)
		require.Equal(t, &types.SpfTransportAllowance{
			Regular: types.SolanaTransportAllowance{
				MaxAllowed:   types.ZeroCurrency,
				WaitTime:     32460 * time.Second,
				PotentialMax: types.NewCurrency64(3500),
			},
			Premined: map[string]types.SolanaTransportAllowance{},
		}, allowance)

		// TODO: check non-empty case for SPF-B.
	})

	t.Run("SiafundTransportHistory empty", func(t *testing.T) {
		history, err := wt.wallet.SiafundTransportHistory()
		require.NoError(t, err)
		require.Empty(t, history)
	})

	submitTime := types.CurrentTimestamp()
	var tx types.Transaction

	t.Run("SiafundTransportSend 10 funds", func(t *testing.T) {
		spfAmount := types.SpfAmount{
			Amount: types.NewCurrency64(10),
			Type:   types.SpfA,
		}
		solanaAddr := types.SolanaAddress{'T', 'e', 's', 't', 'S', 'o', 'l', 'a', 'n', 'a'}

		tc.EXPECT().
			CheckAllowance(gomock.Any(), &transporter.CheckAllowanceRequest{}).
			Return(&transporter.CheckAllowanceResponse{
				Regular: transporter.AmountWithTimeEstimate{
					Amount: types.NewCurrency64(3500),
				},
			}, error(nil))

		tc.EXPECT().
			CheckSolanaAddress(gomock.Any(), &transporter.CheckSolanaAddressRequest{
				SolanaAddress: common.SolanaAddress(solanaAddr),
				Amount:        spfAmount.Amount,
			}).
			Return(&transporter.CheckSolanaAddressResponse{
				CurrentTime: time.Now(),
			}, error(nil))

		spfAmountAhead := types.NewCurrency64(100)
		tc.EXPECT().
			SubmitScpTx(gomock.Any(), gomock.Any()).
			DoAndReturn(func(_ context.Context, req *transporter.SubmitScpTxRequest) (*transporter.SubmitScpTxResponse, error) {
				tx = req.Transaction
				return &transporter.SubmitScpTxResponse{
					WaitTimeEstimate: 10 * time.Second,
					SpfAmountAhead:   &spfAmountAhead,
				}, nil
			})

		res, err := wt.wallet.SiafundTransportSend(spfAmount, types.Regular, nil, solanaAddr)
		require.NoError(t, err)
		require.Equal(t, 10*time.Second, res.WaitTime)
		require.Equal(t, &spfAmountAhead, res.AmountAhead)
	})

	t.Run("SiafundTransportHistory one record", func(t *testing.T) {
		history, err := wt.wallet.SiafundTransportHistory()
		require.NoError(t, err)
		require.Equal(t, []types.SolanaTransport{{
			BurnID: tx.ID(),
			SolanaTransportRecord: types.SolanaTransportRecord{
				Status:  types.SubmittedToTransporter,
				Amount:  types.NewCurrency64(10),
				Created: submitTime,
			},
		}}, history)
	})
}
