package wallet

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/scpcorp/ScPrime/types"
	"gitlab.com/scpcorp/spf-transporter"
	"gitlab.com/scpcorp/spf-transporter/common"
)

// BurntAmount returns amount of coins burnt in this transaction.
func BurntAmount(ct types.CurrencyType, tx *types.Transaction) types.Currency {
	if ct == types.Scp {
		return ScpBurntAmount(tx)
	}
	return SpfBurntAmount(tx)
}

// ScpBurntAmount returns amount of SCP burnt in this transaction.
func ScpBurntAmount(tx *types.Transaction) types.Currency {
	var burntAmount types.Currency
	for _, sco := range tx.SiacoinOutputs {
		if sco.UnlockHash == types.BurnAddressUnlockHash {
			burntAmount = burntAmount.Add(sco.Value)
		}
	}
	return burntAmount
}

// SpfBurntAmount returns amount of SPF burnt in this transaction.
func SpfBurntAmount(tx *types.Transaction) types.Currency {
	var burntAmount types.Currency
	for _, sfo := range tx.SiafundOutputs {
		if sfo.UnlockHash == types.BurnAddressUnlockHash {
			burntAmount = burntAmount.Add(sfo.Value)
		}
	}
	return burntAmount
}

func extractSolanaAddress(ct types.CurrencyType, arbitraryData []byte) (common.SolanaAddress, error) {
	if ct == types.Spf {
		return common.ExtractSolanaSpfAddress(arbitraryData)
	}
	return common.ExtractSolanaScpAddress(arbitraryData)
}

// IsSolanaTransportTx returns true if given tx is a valid transport to Solana
// and detects currency type (SPF / SCP).
func IsSolanaTransportTx(tx *types.Transaction) (ct types.CurrencyType, ok bool) {
	// Ensure tx burns some coins.
	scpBurnt := ScpBurntAmount(tx)
	spfBurnt := SpfBurntAmount(tx)
	if spfBurnt.IsZero() && scpBurnt.IsZero() {
		// Not ok, transaction does not burn anything.
		return
	} else if !spfBurnt.IsZero() && !scpBurnt.IsZero() {
		// Not ok, ambiguity is not allowed.
		// Transaction must not burn both SCP and SPF.
		return
	} else if spfBurnt.IsZero() && !scpBurnt.IsZero() {
		ct = types.Scp
	} else if !spfBurnt.IsZero() && scpBurnt.IsZero() {
		ct = types.Spf
	}
	// Ensure WholeTransaction flag is set in all signatures.
	for _, sig := range tx.TransactionSignatures {
		if !sig.CoveredFields.WholeTransaction {
			// Not ok.
			return
		}
	}
	// Ensure there is valid Solana address in arbitrary data.
	for _, ad := range tx.ArbitraryData {
		if _, err := extractSolanaAddress(ct, ad); err == nil {
			ok = true
			return
		}
	}
	return
}

func spfxEmissionTime(c types.Currency) time.Duration {
	minutes := common.DivCurrencyRoundUp(c, common.SpfPerMinute).Big().Int64()
	return time.Minute * time.Duration(minutes)
}

func (w *Wallet) isPreminedUnlockHash(uh types.UnlockHash) bool {
	w.spfxPreminedAddrsMu.RLock()
	defer w.spfxPreminedAddrsMu.RUnlock()

	if _, ok := w.spfxPreminedAddrs[uh]; ok {
		return true
	}
	return false
}

func statusFromTransporter(s common.TransportStatus) (res types.SolanaTransportStatus, err error) {
	switch s {
	case common.Unconfirmed:
		res = types.SubmittedToTransporter
	case common.InTheQueue:
		res = types.InTheQueue
	case common.SolanaTxCreated:
		res = types.InTheQueue
	case common.Completed:
		res = types.Completed
	default:
		err = fmt.Errorf("status %v is not convertable", s)
	}
	return
}

func equalStatus(remote common.TransportStatus, local types.SolanaTransportStatus) bool {
	switch remote {
	case common.Unconfirmed:
		return local == types.SubmittedToTransporter
	case common.InTheQueue:
		return local == types.InTheQueue
	case common.SolanaTxCreated:
		return local == types.InTheQueue
	case common.Completed:
		return local == types.Completed
	}
	return false
}

func (w *Wallet) threadedMonitorSolanaTransports(tc TransporterClient, ct types.CurrencyType) {
	if err := w.tg.Add(); err != nil {
		return
	}
	defer w.tg.Done()

	const monitorInterval = 10 * time.Minute
	for {
		select {
		case <-time.After(monitorInterval):
		case <-w.tg.StopChan():
			return
		}
		w.mu.Lock()
		allTransports, err := dbGetAllSolanaTransports(w.dbTx, ct)
		w.mu.Unlock()
		if err != nil {
			w.log.Println("Failed to load Solana transport records from database:", err)
			continue
		}
		recordsToUpdate := make(map[types.TransactionID]types.SolanaTransportRecord)
		ctx := context.Background()
		for _, t := range allTransports {
			if t.Status == types.Completed {
				// Skip completed.
				continue
			}
			const minRecordAge = time.Minute * 20
			if time.Since(t.Created.ToStdTime()) < minRecordAge {
				// Do not touch recently created records here, they might still be
				// updated by the actual Send function.
				continue
			}
			var newStatus types.SolanaTransportStatus
			needToUpdateStatus := false
			// Check status on transporter.
			statusResp, err := tc.TransportStatus(ctx, &transporter.TransportStatusRequest{
				BurnID: t.BurnID,
			})
			if err != nil {
				w.log.Printf("Failed to get status of the queue record %s from transporter: %v", t.BurnID.String(), err)
				continue
			}
			if statusResp.Status == common.NotFound {
				// Record is unknown to transporter, check if tx is confirmed.
				if t.Status != types.BurnCreated && t.Status != types.BurnBroadcasted {
					w.log.Printf("SPF transport record %s is not found on transporter, local status %s", t.BurnID.String(), t.Status.String())
				}
				confirmed, err := w.tpool.TransactionConfirmed(t.BurnID)
				if err != nil {
					w.log.Println("Failed to check if transaction is confirmed in tpool:", err)
					continue
				}
				if confirmed {
					// Burn tx was confirmed, try submitting to transporter.
					w.mu.Lock()
					burnTx, err := dbGetSolanaTransportBurn(w.dbTx, ct, t.BurnID)
					w.mu.Unlock()
					if err != nil {
						w.log.Println("Failed to fetch burn txn set from database:", err)
						continue
					}
					if _, err := tc.SubmitScpTx(
						ctx,
						&transporter.SubmitScpTxRequest{Transaction: burnTx},
					); err != nil {
						w.log.Println("Failed to submit confirmed tx to transporter (threadedMonitorSolanaTransports):", err)
						continue
					}
					newStatus = types.SubmittedToTransporter
					needToUpdateStatus = true
				}
			} else if !equalStatus(statusResp.Status, t.Status) {
				newStatus, err = statusFromTransporter(statusResp.Status)
				if err != nil {
					w.log.Println("Failed to convert status from transporter:", err)
					continue
				}
				needToUpdateStatus = true
			}
			if needToUpdateStatus {
				newRecord := t.SolanaTransportRecord
				newRecord.Status = newStatus
				recordsToUpdate[t.BurnID] = newRecord
			}
		}
		if err := w.updateSolanaTransports(ct, recordsToUpdate); err != nil {
			w.log.Println("Failed to save updated Solana transport records:", err)
		}
	}
}

func (w *Wallet) updateSolanaTransports(ct types.CurrencyType, records map[types.TransactionID]types.SolanaTransportRecord) error {
	w.mu.Lock()
	defer w.mu.Unlock()

	for burnID, r := range records {
		if _, err := dbGetSolanaTransport(w.dbTx, ct, burnID); err == errNoKey {
			// Do not create new records here.
			continue
		}
		if err := dbPutSolanaTransport(w.dbTx, ct, types.SolanaTransport{BurnID: burnID, SolanaTransportRecord: r}); err != nil {
			return err
		}
	}
	return w.syncDB()
}

func (w *Wallet) putSolanaTransportBurn(ct types.CurrencyType, burnID types.TransactionID, tx types.Transaction) error {
	w.mu.Lock()
	defer w.mu.Unlock()

	if err := dbPutSolanaTransportBurn(w.dbTx, ct, burnID, tx); err != nil {
		return err
	}
	return w.syncDB()
}

func (w *Wallet) putSolanaTransport(ct types.CurrencyType, r types.SolanaTransport) error {
	w.mu.Lock()
	defer w.mu.Unlock()

	if err := dbPutSolanaTransport(w.dbTx, ct, r); err != nil {
		return err
	}

	return w.syncDB()
}
