package api

import (
	"context"
	"net/http"

	"github.com/starius/api2"
)

// HandlerHTTPapi api interface.
type HandlerHTTPapi interface {
	Health(ctx context.Context, req *HealthRequest) (*HealthResponse, error)
	TokenResources(ctx context.Context, req *TokenResourcesRequest) (*TokenResourcesResponse, error)
	ListSectorIDs(ctx context.Context, req *ListSectorIDsRequest) (*ListSectorIDsResponse, error)
	RemoveSectors(ctx context.Context, req *RemoveSectorsRequest) (*RemoveSectorsResponse, error)
	DownloadWithToken(context.Context, *DownloadWithTokenRequest) (*DownloadWithTokenResponse, error)
	DownloadBinaryWithToken(context.Context, *DownloadBinaryWithTokenRequest) (*DownloadBinaryWithTokenResponse, error)
	UploadWithToken(context.Context, *UploadWithTokenRequest) (*UploadWithTokenResponse, error)
	UploadBinaryWithToken(context.Context, *UploadBinaryWithTokenRequest) (*UploadBinaryWithTokenResponse, error)
	AttachSectors(context.Context, *AttachSectorsRequest) (*AttachSectorsResponse, error)
}

// GetRoutes return api routes.
func GetRoutes(ol HandlerHTTPapi) []api2.Route {
	// TODO: using JSON is a temporary solution (we will work on the custom transport)
	t := &api2.JsonTransport{
		Errors: map[string]error{
			"DownloadWithTokenError": &DownloadWithTokenError{},
			"UploadWithTokenError":   &UploadWithTokenError{},
			"AttachSectorsError":     &AttachSectorsError{},
		},
	}

	routes := []api2.Route{
		{Method: http.MethodGet, Path: "/health", Handler: api2.Method(&ol, "Health"), Transport: t},
		{Method: http.MethodGet, Path: "/resources", Handler: api2.Method(&ol, "TokenResources"), Transport: t},
		{Method: http.MethodGet, Path: "/list-sector-ids", Handler: api2.Method(&ol, "ListSectorIDs"), Transport: t},
		{Method: http.MethodPost, Path: "/remove-sectors", Handler: api2.Method(&ol, "RemoveSectors"), Transport: t},
		{Method: http.MethodPost, Path: "/download", Handler: api2.Method(&ol, "DownloadWithToken"), Transport: t},
		{Method: http.MethodPost, Path: "/binary-download", Handler: api2.Method(&ol, "DownloadBinaryWithToken"), Transport: t},
		{Method: http.MethodPost, Path: "/upload", Handler: api2.Method(&ol, "UploadWithToken"), Transport: t},
		{Method: http.MethodPost, Path: "/binary-upload", Handler: api2.Method(&ol, "UploadBinaryWithToken"), Transport: t},
		{Method: http.MethodPost, Path: "/attach", Handler: api2.Method(&ol, "AttachSectors"), Transport: t},
	}

	if ol == nil {
		// Client has one extra route used by UploadBinaryCompat.
		// TODO: remove it after switch to UploadBinaryWithToken.
		routes = append(routes, api2.Route{
			Method: http.MethodPost,
			Path:   "/upload",
			Handler: func(ctx context.Context, req *uploadBinaryCompatRequest) (*uploadBinaryCompatResponse, error) {
				panic("this placeholder must not be called")
			},
			Transport: t,
		})
	}

	return routes
}
