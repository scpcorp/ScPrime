package api

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"strings"
	"sync"

	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
)

// DownloadAndVerify calls Client.DownloadWithToken() and verifies merkle proofs.
func (c *Client) DownloadAndVerify(ctx context.Context, req *DownloadWithTokenRequest) (*DownloadWithTokenResponse, error) {
	resp, err := c.DownloadWithToken(ctx, req)
	if err != nil {
		return nil, err
	}
	if len(req.Ranges) != len(resp.Sections) {
		return nil, fmt.Errorf("number of response sections %d does not match requested %d", len(resp.Sections), len(req.Ranges))
	}
	for i, reqRange := range req.Ranges {
		sec := resp.Sections[i]
		if len(sec.Data) != int(reqRange.Length) {
			return nil, errors.New("host did not send enough sector data")
		}
		if reqRange.MerkleProof {
			proofStart := int(reqRange.Offset) / crypto.SegmentSize
			proofEnd := int(reqRange.Offset+reqRange.Length) / crypto.SegmentSize
			if !crypto.VerifyRangeProof(sec.Data, sec.MerkleProof, proofStart, proofEnd, reqRange.MerkleRoot) {
				return nil, ErrBadMerkleProof
			}
		}
	}
	return resp, nil
}

// ErrBadMerkleProof is an error returned when MerkleProof verification fails.
//
// It is returned by Client.DownloadAndVerify and by the reader returned by
// Client.DownloadBinaryAndVerify.
var ErrBadMerkleProof = errors.New("host provided incorrect sector data or Merkle proof")

// DownloadBinaryAndVerify calls Client.DownloadBinaryWithToken() and verifies merkle proofs.
//
// It returns tokenRecord and a reader producing requested data. If merkle proof
// verification was requested and proof fails, the reader will fail with
// ErrBadMerkleProof in one of reads. Note that the reader can return some data
// before it fails with this error. The caller must receive all the readers from
// the channels and read all of them to avoid goroutine leak.
func (c *Client) DownloadBinaryAndVerify(ctx context.Context, req *DownloadBinaryWithTokenRequest) (tokenRecord *TokenRecord, sectionReader io.Reader, err error) {
	// Call the API.
	resp, err2 := c.DownloadBinaryWithToken(ctx, req)
	if err2 != nil {
		return nil, nil, err2
	}

	// If error occurs when reading, read the remaining data from the reader
	// to io.Discard and close the reader. It is needed to avoid connection
	// leak of the client.
	defer func() {
		if err != nil {
			_, _ = io.Copy(io.Discard, resp.BinaryData)
			_ = resp.BinaryData.Close()
		}
	}()

	// Parse TokenRecord.
	tokenRecordJSONLen := make([]byte, 2)
	if _, err := io.ReadFull(resp.BinaryData, tokenRecordJSONLen); err != nil {
		return nil, nil, fmt.Errorf("failed to read length of TokenRecord, expected 2 bytes: %w", err)
	}
	tokenRecordJSON := make([]byte, binary.LittleEndian.Uint16(tokenRecordJSONLen))
	if _, err := io.ReadFull(resp.BinaryData, tokenRecordJSON); err != nil {
		return nil, nil, fmt.Errorf("failed to read TokenRecord, expected JSON format: %w", err)
	}
	if err := json.Unmarshal(tokenRecordJSON, &tokenRecord); err != nil {
		return nil, nil, fmt.Errorf("failed to parse TokenRecord: %w", err)
	}

	// Parse Section.
	sectionJSONLen := make([]byte, 2)
	if _, err := io.ReadFull(resp.BinaryData, sectionJSONLen); err != nil {
		return nil, nil, fmt.Errorf("failed to read length of Section: %w", err)
	}
	sectionJSON := make([]byte, binary.LittleEndian.Uint16(sectionJSONLen))
	if _, err := io.ReadFull(resp.BinaryData, sectionJSON); err != nil {
		return nil, nil, fmt.Errorf("failed to read Section: %w", err)
	}
	var sec Section
	if err := json.Unmarshal(sectionJSON, &sec); err != nil {
		return nil, nil, fmt.Errorf("failed to parse Section: %w", err)
	}

	// Create a pipe. The reader part will be returned by the function.
	// The writer part will be used in the goroutine below to copy data
	// from the HTTP response.
	r, w := io.Pipe()

	// Run the remaining part in a goroutine. It is needed because the data has
	// to be passed to MerkleProof verifier as well, which also accepts it
	// through reader interface.

	go func() {
		// Dump the data and close the connection in case an error occurs.
		// See above.
		defer func() {
			_, _ = io.Copy(io.Discard, resp.BinaryData)
			_ = resp.BinaryData.Close()
		}()

		if err := copySection(w, resp.BinaryData, req.Range, sec); err != nil {
			_ = w.CloseWithError(err)
			return
		}

		if err := w.Close(); err != nil {
			log.Printf("Failed to close writer half of pipe: %v", err)
			return
		}
	}()

	return tokenRecord, r, nil
}

// copySection copies a section of data to writer, running MerkleProof check if needed.
func copySection(w *io.PipeWriter, body io.Reader, reqRange Range, sec Section) error {
	if !reqRange.MerkleProof {
		// Simple case: no MerkleProof was requsted.
		// Just copy the data.
		n, err := io.CopyN(w, body, int64(reqRange.Length))
		if n != int64(reqRange.Length) {
			return fmt.Errorf("wrong data size copying whole section, got %d bytes, when expected %d: %w", n, reqRange.Length, io.ErrUnexpectedEOF)
		} else if err != nil {
			return err
		}

		return nil
	}

	// The case of MerkleProof verification.

	// Send a copy of data to MerkleProof verifier over a pipe.
	readerCopy, writerForTee := io.Pipe()
	teeReader := io.TeeReader(body, writerForTee)

	// Run a separate goroutine with MerkleProof verifier.
	// It reads the data from another reader (readerCopy) getting the copy
	// of the data via io.Pipe and io.TeeReader magic.
	proofCh := make(chan bool)
	go func() {
		proofStart := int(reqRange.Offset) / crypto.SegmentSize
		proofEnd := int(reqRange.Offset+reqRange.Length) / crypto.SegmentSize
		proofCh <- crypto.VerifyRangeProofFromReader(readerCopy, sec.MerkleProof, proofStart, proofEnd, reqRange.MerkleRoot)
	}()

	// Read all but last byte.
	n, err := io.CopyN(w, teeReader, int64(reqRange.Length-1))
	if n != int64(reqRange.Length-1) {
		return fmt.Errorf("wrong data size copying all but one bytes, got %d bytes, when expected %d: %w", n, reqRange.Length-1, io.ErrUnexpectedEOF)
	} else if err != nil {
		return err
	}

	// Read the last byte.
	var lastByte bytes.Buffer
	n, err = io.CopyN(&lastByte, teeReader, 1)
	if n != 1 {
		return fmt.Errorf("wrong data size reading the last byte, got %d bytes, when expected %d: %w", n, 1, io.ErrUnexpectedEOF)
	} else if err != nil {
		return err
	}

	// Wait for MerkleProof verifier.
	isCorrect := <-proofCh
	if !isCorrect {
		return ErrBadMerkleProof
	}

	// Finally send the last byte.
	n, err = io.CopyN(w, &lastByte, 1)
	if n != 1 {
		return fmt.Errorf("wrong data size sending the last byte, got %d bytes, when expected %d: %w", n, 1, io.ErrUnexpectedEOF)
	} else if err != nil {
		return err
	}

	return nil
}

type uploadBinaryCompatRequest struct {
	Authorization string        `header:"Authorization"`
	Body          io.ReadCloser `use_as_body:"true" is_stream:"true"`
}

type uploadBinaryCompatResponse struct {
	TokenRecord *TokenRecord `json:"token_record,omitempty"`
}

type readerCloser struct {
	io.Reader
	io.Closer
}

// UploadBinaryCompat emulates new reader API using old JSON API of server.
func (c *Client) UploadBinaryCompat(ctx context.Context, req *UploadBinaryWithTokenRequest) (*UploadBinaryWithTokenResponse, error) {
	base64reader, base64writer := io.Pipe()
	writer := base64.NewEncoder(base64.StdEncoding, base64writer)

	// Launch a goroutine copying data from req.Sector to writer.
	// Base64 encoder which translate these writes into corresponding writes
	// of base64 encoded data to base64writer, which will be translated by
	// io.Pipe into reads available from base64reader.

	var wg sync.WaitGroup
	wg.Add(1)
	var n int64
	go func() {
		defer wg.Done()

		var err error
		n, err = io.Copy(writer, req.Sector)
		if n != int64(modules.SectorSize) {
			base64writer.CloseWithError(fmt.Errorf("wrong data size when encoding to base64, got %d bytes, when expected %d: %w", n, modules.SectorSize, io.ErrUnexpectedEOF))
			return
		} else if err != nil {
			_ = base64writer.CloseWithError(err)
			return
		}

		_ = writer.Close()
		_ = base64writer.Close()
	}()

	resp, err := c.uploadBinaryCompat(ctx, &uploadBinaryCompatRequest{
		Authorization: req.Authorization,
		Body: &readerCloser{
			Reader: io.MultiReader(
				strings.NewReader(`{"sectors":["`),
				base64reader,
				strings.NewReader(`"]}`),
			),
			Closer: base64reader,
		},
	})

	// Close base64reader to unblock base64writer.Write call inside io.Copy
	// call above, to unblock the goroutine, in case not whole sector has been
	// read (network error or host error).
	_ = base64reader.Close()

	// Make sure the goroutine does not leak.
	wg.Wait()

	// Close the input reader.
	_ = req.Sector.Close()

	if errors.Is(err, io.ErrUnexpectedEOF) {
		if n == 0 {
			return nil, &UploadWithTokenError{
				DataLengthIsZero: true,
			}
		}
		return nil, &UploadWithTokenError{
			IncorrectSectorSize: true,
		}
	}
	if err != nil {
		if _, ok := err.(*UploadWithTokenError); !ok {
			err = &UploadWithTokenError{
				UnknownError: err.Error(),
			}
		}
		return nil, err
	}

	return &UploadBinaryWithTokenResponse{
		TokenRecord: resp.TokenRecord,
	}, nil
}
