package host

import (
	"sync/atomic"
	"testing"

	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

func TestHostFinancialMetrics_StorageRevenue(t *testing.T) {
	set, err := newTestingSet(t)
	if err != nil {
		t.Fatalf("error setting up testing set: %v", err)
	}
	defer set.closeFn()

	_, err = set.miner.AddBlock()
	if err != nil {
		t.Fatalf("error adding block: %v", err)
	}

	for i := 0; i < 5; i++ {
		soid := types.FileContractID{}
		b := fastrand.Bytes(len(soid))
		copy(soid[:], b)

		// Create a storage obligation with succeeded status
		succeededObligation := modules.StorageObligation{
			ExpirationHeight:        set.host.blockHeight - 1,
			ObligationId:            soid,
			ObligationStatus:        obligationSucceeded.String(),
			PotentialStorageRevenue: types.NewCurrency64(100),
			// Other fields can be set as needed for the test
		}

		// Register the succeeded obligation
		set.host.financialMetrics.registerStorageObligation(succeededObligation, set.host.blockHeight)
	}

	// Calculate the summary
	summary := set.host.financialMetrics.Summary()

	// Assert the storage revenue calculation for succeeded obligations
	if summary.StorageRevenue.Cmp(types.NewCurrency64(500)) != 0 {
		t.Errorf("Expected StorageRevenue to be 100, got %v", summary.StorageRevenue)
	}

	// Create a storage obligation with failed status
	soidf := types.FileContractID{}
	b := fastrand.Bytes(len(soidf))
	copy(soidf[:], b)
	// Create a missed proof outputs for the failed obligation
	mpos := make([]types.SiacoinOutput, 3)
	for i := 0; i < 3; i++ {
		mpos[i].Value = types.NewCurrency64(150)
		uh := fastrand.Bytes(len(types.UnlockHash{}))
		copy(mpos[i].UnlockHash[:], uh)
	}

	failedObligation := modules.StorageObligation{
		ObligationId:            soidf,
		ObligationStatus:        obligationFailed.String(),
		ExpirationHeight:        set.host.blockHeight - 1,
		PotentialStorageRevenue: types.NewCurrency64(150),
		MissedProofOutputs:      mpos,
	}

	// Register the failed obligation
	set.host.financialMetrics.registerStorageObligation(failedObligation, set.host.blockHeight)

	// Calculate the summary again
	summary = set.host.financialMetrics.Summary()

	// Assert the storage revenue calculation for failed obligations
	if summary.StorageRevenue.Cmp(types.NewCurrency64(500)) != 0 {
		t.Errorf("Expected StorageRevenue to be 500, got %v", summary.StorageRevenue)
	}
	// Assert Lost Revenue calculation for failed
	if summary.LostRevenue.Cmp(types.NewCurrency64(150)) != 0 {
		t.Errorf("Expected Lost Revenue to be 150, got %v", summary.LostRevenue)
	}

	// Create an active storage obligation with a non zero potential storage revenue
	// Generate new ObligationID
	soida := types.FileContractID{}
	b = fastrand.Bytes(len(soida))
	copy(soida[:], b)
	// Register the active obligation
	activeObligation := modules.StorageObligation{
		ObligationId:            soida,
		ObligationStatus:        obligationUnresolved.String(),
		OriginConfirmed:         true,
		ExpirationHeight:        set.host.blockHeight + 20,
		PotentialStorageRevenue: types.NewCurrency64(200),
	}
	set.host.financialMetrics.registerStorageObligation(activeObligation, set.host.blockHeight)

	// Calculate the summary again
	summary = set.host.financialMetrics.Summary()

	// Assert the storage revenue calculation for active obligations not being included
	if summary.StorageRevenue.Cmp(types.NewCurrency64(500)) != 0 {
		t.Errorf("Expected StorageRevenue to be 500, got %v", summary.StorageRevenue)
	}
	// Assert the active contracts count
	if summary.ContractCount != 1 {
		t.Errorf("Expected Active Storage Obligations count to be 1, got %v", summary.ContractCount)
	}
	// Assert the active contracts potential storage revenue
	if summary.PotentialStorageRevenue.Cmp(types.NewCurrency64(200)) != 0 {
		t.Errorf("Expected Active Storage Obligations potential Storage Revenue to be 200, got %v", summary.PotentialStorageRevenue)
	}
	// Assert ExpiredContractCount contract count is 6 as 5 succeeded and 1 failed have been registered
	if summary.ExpiredContractCount != 6 {
		t.Errorf("Expected Failed Storage Obligations count to be 6, got %v", summary.ExpiredContractCount)
	}
}

func TestHostFinancialMetrics_ContractCount(t *testing.T) {
	// Arrange
	fm := &hostFinancialMetrics{
		activeContractCount:      atomic.Int32{},
		unconfirmedContractCount: atomic.Int32{},
	}

	// Act
	obligations := []modules.StorageObligation{
		{ObligationId: types.FileContractID{1}, ExpirationHeight: 110, OriginConfirmed: true},
		{ObligationId: types.FileContractID{2}, ExpirationHeight: 120, OriginConfirmed: false},
		{ObligationId: types.FileContractID{3}, ExpirationHeight: 130, OriginConfirmed: true},
		{ObligationId: types.FileContractID{4}, ExpirationHeight: 140, OriginConfirmed: false},
		{ObligationId: types.FileContractID{5}, ExpirationHeight: 150, OriginConfirmed: true},
		// Add 4 expired storage obligations with expiration height less than 100
		{ObligationId: types.FileContractID{6}, ExpirationHeight: 90, OriginConfirmed: true},
		{ObligationId: types.FileContractID{7}, ExpirationHeight: 80, OriginConfirmed: true},
		{ObligationId: types.FileContractID{8}, ExpirationHeight: 70, OriginConfirmed: true},
		{ObligationId: types.FileContractID{9}, ExpirationHeight: 60, OriginConfirmed: true},
	}
	for _, so := range obligations {
		fm.registerStorageObligation(so, 100)
	}

	// Unregister one active and one unconfirmed storage obligation
	fm.unregisterStorageObligation(types.FileContractID{1})
	fm.unregisterStorageObligation(types.FileContractID{2})

	// Assert
	expectedActiveCount := uint64(2)      // 2 active obligations after unregistering one
	expectedUnconfirmedCount := uint64(1) // 1 unconfirmed obligations after unregistering one
	expectedExpiredCount := uint64(4)     // Added 4 expired obligations with expiration height less than 100
	actualActiveCount := uint64(fm.activeContractCount.Load())
	actualUnconfirmedCount := uint64(fm.unconfirmedContractCount.Load())
	actualExpiredCount := uint64(fm.expiredContractCount.Load())
	if actualActiveCount != expectedActiveCount {
		t.Errorf("Expected active contract count to be %d, but got %d", expectedActiveCount, actualActiveCount)
	}
	if actualUnconfirmedCount != expectedUnconfirmedCount {
		t.Errorf("Expected unconfirmed contract count to be %d, but got %d", expectedUnconfirmedCount, actualUnconfirmedCount)
	}
	if actualExpiredCount != expectedExpiredCount {
		t.Errorf("Expected expired contract count to be %d, but got %d", expectedExpiredCount, actualExpiredCount)
	}
}

func TestHostFinancialMetricsSummary(t *testing.T) {
	fm := &hostFinancialMetrics{}

	activeSO := modules.StorageObligation{
		ContractCost:             types.NewCurrency64(100),
		RevisionNumber:           0,
		DataSize:                 0,
		LockedCollateral:         types.NewCurrency64(50),
		ObligationId:             [32]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
		PotentialAccountFunding:  types.Currency{},
		PotentialDownloadRevenue: types.NewCurrency64(200),
		PotentialStorageRevenue:  types.NewCurrency64(150),
		PotentialUploadRevenue:   types.NewCurrency64(100),
		RiskedCollateral:         types.NewCurrency64(30),
		SectorRootsCount:         0,
		TransactionFeesAdded:     types.NewCurrency64(5),
		TransactionID:            [32]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
		ExpirationHeight:         30,
		NegotiationHeight:        0,
		ProofDeadLine:            0,
		ObligationStatus:         obligationUnresolved.String(),
		OriginConfirmed:          true,
		ProofConfirmed:           false,
		ProofConstructed:         false,
		RevisionConfirmed:        false,
		RevisionConstructed:      false,
		ValidProofOutputs:        []types.SiacoinOutput{},
		MissedProofOutputs:       []types.SiacoinOutput{},
	}

	unconfirmedSO := modules.StorageObligation{
		ContractCost:             types.NewCurrency64(120),
		RevisionNumber:           0,
		DataSize:                 0,
		LockedCollateral:         types.NewCurrency64(60),
		ObligationId:             [32]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32},
		PotentialAccountFunding:  types.Currency{},
		PotentialDownloadRevenue: types.NewCurrency64(220),
		PotentialStorageRevenue:  types.NewCurrency64(180),
		PotentialUploadRevenue:   types.NewCurrency64(120),
		RiskedCollateral:         types.NewCurrency64(40),
		SectorRootsCount:         0,
		TransactionFeesAdded:     types.NewCurrency64(6),
		TransactionID:            [32]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 32},
		ExpirationHeight:         20,
		NegotiationHeight:        0,
		ProofDeadLine:            0,
		ObligationStatus:         obligationUnresolved.String(),
		OriginConfirmed:          false,
		ProofConfirmed:           false,
		ProofConstructed:         false,
		RevisionConfirmed:        false,
		RevisionConstructed:      false,
		ValidProofOutputs:        []types.SiacoinOutput{},
		MissedProofOutputs:       []types.SiacoinOutput{},
	}

	expiredSO := modules.StorageObligation{
		ContractCost:             types.NewCurrency64(150),
		RevisionNumber:           0,
		DataSize:                 0,
		LockedCollateral:         types.NewCurrency64(75),
		ObligationId:             [32]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 33},
		PotentialAccountFunding:  types.Currency{},
		PotentialDownloadRevenue: types.NewCurrency64(275),
		PotentialStorageRevenue:  types.NewCurrency64(225),
		PotentialUploadRevenue:   types.NewCurrency64(150),
		RiskedCollateral:         types.NewCurrency64(50),
		SectorRootsCount:         0,
		TransactionFeesAdded:     types.NewCurrency64(7),
		TransactionID:            [32]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 33},
		ExpirationHeight:         types.BlockHeight(10),
		NegotiationHeight:        0,
		ProofDeadLine:            0,
		ObligationStatus:         obligationSucceeded.String(),
		OriginConfirmed:          true,
		ProofConfirmed:           false,
		ProofConstructed:         false,
		RevisionConfirmed:        false,
		RevisionConstructed:      false,
		ValidProofOutputs:        []types.SiacoinOutput{},
		MissedProofOutputs:       []types.SiacoinOutput{},
	}

	currentBlockHeight := types.BlockHeight(15)

	// create and register a failed expired contract with some storage and risked collateral
	failedExpiredSO := expiredSO
	// change the contract id
	copy(failedExpiredSO.ObligationId[:], []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34})
	// change the status to failed and add some risked collateral and storage revenue
	failedExpiredSO.RevisionNumber = 1
	failedExpiredSO.ExpirationHeight = currentBlockHeight - 2
	failedExpiredSO.ObligationStatus = obligationFailed.String()
	failedExpiredSO.PotentialStorageRevenue = types.NewCurrency64(150)
	failedExpiredSO.RiskedCollateral = types.NewCurrency64(50)
	// create missed proof outputs so there is burned value
	failedExpiredSO.MissedProofOutputs = []types.SiacoinOutput{
		{Value: types.NewCurrency64(50), UnlockHash: types.UnlockHash{}},
		{Value: types.NewCurrency64(50), UnlockHash: types.UnlockHash{}},
		{Value: types.NewCurrency64(100), UnlockHash: types.UnlockHash{}},
	}

	fm.registerStorageObligation(activeSO, currentBlockHeight)
	fm.registerStorageObligation(unconfirmedSO, currentBlockHeight)
	fm.registerStorageObligation(expiredSO, currentBlockHeight)
	fm.registerStorageObligation(failedExpiredSO, currentBlockHeight)

	summary := fm.Summary()

	if summary.ContractCount != fm.ContractCount() && fm.ContractCount() != 1 {
		t.Errorf("Expected 1 contract but got: %v", summary.ContractCount)
	}

	if summary.ExpiredContractCount != fm.ExpiredContractCount() && fm.ExpiredContractCount() != 2 {
		t.Errorf("Expected 2 expired contracts but got: %v", summary.ExpiredContractCount)
	}

	if summary.UnconfirmedContractCount != fm.UnconfirmedContractCount() && summary.UnconfirmedContractCount != 1 {
		t.Errorf("Expected 1 unconfirmed contracts but got: %v", summary.UnconfirmedContractCount)
	}

	// Test potential revenue
	potentialRevenue := summary.PotentialContractCompensation.
		Add(summary.PotentialStorageRevenue).
		Add(summary.PotentialDownloadBandwidthRevenue).
		Add(summary.PotentialUploadBandwidthRevenue)
	expectedRevenue := activeSO.ContractCost.
		Add(activeSO.PotentialStorageRevenue).
		Add(activeSO.PotentialDownloadRevenue).
		Add(activeSO.PotentialUploadRevenue)
	if !potentialRevenue.Equals(expectedRevenue) {
		t.Logf("ActiveContracts: %v, ExpiredContracts: %v, UnconfirmedContracts: %v", summary.ContractCount, summary.ExpiredContractCount, summary.UnconfirmedContractCount)
		t.Errorf("Expected Potential Contract Revenue to be %v, got: %v", expectedRevenue, potentialRevenue)
	}

	// Test summary.StorageRevenue, should be equal to the expired contract storage revenue.
	if !summary.StorageRevenue.Equals(expiredSO.PotentialStorageRevenue) {
		t.Errorf("Expected Storage Revenue to be %v, got: %v", expiredSO.PotentialStorageRevenue, summary.StorageRevenue)
	}

	expectedLostRevenue := types.NewCurrency64(50) // burned coins - risked collateral
	if !summary.LostRevenue.Equals(expectedLostRevenue) {
		t.Errorf("Expected Lost Revenue to be %v, got: %v", types.NewCurrency64(100), summary.LostRevenue)
	}
	expectedLostCollateral := types.NewCurrency64(50) //risked collateral from the failed contract
	if !summary.LostStorageCollateral.Equals(expectedLostCollateral) {
		t.Errorf("Expected Lost Collateral to be %v, got: %v", expectedLostCollateral, summary.LostStorageCollateral)
	}

	// Unregister the failed contract
	fm.unregisterStorageObligation(failedExpiredSO.ObligationId)
	summary = fm.Summary()

	if summary.ContractCount != 1 {
		t.Errorf("Expected 1 contract but got: %v", summary.ContractCount)
	}

	if summary.ExpiredContractCount != 1 {
		t.Errorf("Expected 1 expired contracts but got: %v", summary.ExpiredContractCount)
	}

	if summary.UnconfirmedContractCount != 1 {
		t.Errorf("Expected 1 unconfirmed contracts but got: %v", summary.UnconfirmedContractCount)
	}

	if !summary.LostRevenue.Equals(types.ZeroCurrency) {
		t.Errorf("Expected Lost Revenue to be zero, got: %v", summary.LostRevenue)
	}
	if !summary.LostStorageCollateral.Equals(types.ZeroCurrency) {
		t.Errorf("Expected Lost Collateral to be zero, got: %v", summary.LostStorageCollateral)
	}
}
