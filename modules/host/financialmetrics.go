package host

import (
	"sync"
	"sync/atomic"

	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
)

// hostFinancialMetricss implements modules.HostFinancialMetrics interface
// keeps separate buckets of active, expired and not yet confirmed contracts.
// Each map keeps the storage obligations with the storage obligation id as key
// and the storage obligation itself as the value
// id := key.(types.FileContractID)
// so := value.(modules.StorageObligation)
type hostFinancialMetrics struct {
	activeContracts      sync.Map
	expiredContracts     sync.Map
	unconfirmedContracts sync.Map
	// Counters
	activeContractCount      atomic.Int32
	expiredContractCount     atomic.Int32
	unconfirmedContractCount atomic.Int32
}

func (fm *hostFinancialMetrics) ContractCount() uint64 {
	return uint64(fm.activeContractCount.Load())
}

func (fm *hostFinancialMetrics) UnconfirmedContractCount() uint64 {
	return uint64(fm.unconfirmedContractCount.Load())
}

func (fm *hostFinancialMetrics) ExpiredContractCount() uint64 {
	return uint64(fm.expiredContractCount.Load())
}

func (fm *hostFinancialMetrics) ContractCompensation() types.Currency {
	cc := types.ZeroCurrency
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		cc = cc.Add(so.ContractCost)
		return true
	})
	return cc
}
func (fm *hostFinancialMetrics) PotentialContractCompensation() types.Currency {
	cc := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		cc = cc.Add(so.ContractCost)
		return true
	})
	return cc
}
func (fm *hostFinancialMetrics) LockedStorageCollateral() types.Currency {
	lsc := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		lsc = lsc.Add(so.LockedCollateral)
		return true
	})
	return lsc
}
func (fm *hostFinancialMetrics) UnconfirmedLockedCollateral() types.Currency {
	lsc := types.ZeroCurrency
	fm.unconfirmedContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		lsc = lsc.Add(so.LockedCollateral)
		return true
	})
	return lsc
}
func (fm *hostFinancialMetrics) LostRevenue() types.Currency {
	lr := types.ZeroCurrency
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		if so.ObligationStatus == obligationFailed.String() {
			mpos := so.MissedProofOutputs[:]
			if len(mpos) == 3 {
				lr = lr.Add(mpos[2].Value)
			}
		}
		return true
	})
	return lr
}
func (fm *hostFinancialMetrics) LostStorageCollateral() types.Currency {
	lc := types.ZeroCurrency
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		if so.ObligationStatus == obligationFailed.String() {
			lc = lc.Add(so.RiskedCollateral)
		}
		return true
	})
	return lc
}
func (fm *hostFinancialMetrics) PotentialStorageRevenue() types.Currency {
	psr := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		psr = psr.Add(so.PotentialStorageRevenue)
		return true
	})
	return psr
}
func (fm *hostFinancialMetrics) RiskedStorageCollateral() types.Currency {
	rc := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		rc = rc.Add(so.RiskedCollateral)
		return true
	})
	return rc
}
func (fm *hostFinancialMetrics) StorageRevenue() types.Currency {
	sr := types.ZeroCurrency
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		if so.ObligationStatus == obligationRenewed.String() || so.ObligationStatus == obligationSucceeded.String() {
			sr = sr.Add(so.PotentialStorageRevenue)
		}
		return true
	})
	return sr
}
func (fm *hostFinancialMetrics) TransactionFeeExpenses() types.Currency {
	fees := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		fees = fees.Add(so.TransactionFeesAdded)
		return true
	})
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		fees = fees.Add(so.TransactionFeesAdded)
		return true
	})
	return fees
}
func (fm *hostFinancialMetrics) DownloadBandwidthRevenue() types.Currency {
	dr := types.ZeroCurrency
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		dr = dr.Add(so.PotentialDownloadRevenue)
		return true
	})
	return dr
}
func (fm *hostFinancialMetrics) PotentialDownloadBandwidthRevenue() types.Currency {
	dr := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		dr = dr.Add(so.PotentialDownloadRevenue)
		return true
	})
	return dr
}
func (fm *hostFinancialMetrics) PotentialUploadBandwidthRevenue() types.Currency {
	ur := types.ZeroCurrency
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		ur = ur.Add(so.PotentialUploadRevenue)
		return true
	})
	return ur
}
func (fm *hostFinancialMetrics) UploadBandwidthRevenue() types.Currency {
	ur := types.ZeroCurrency
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		ur = ur.Add(so.PotentialUploadRevenue)
		return true
	})
	return ur
}

func (fm *hostFinancialMetrics) Summary() modules.HostAccountingSummary {
	summary := modules.HostAccountingSummary{
		ContractCompensation:              types.ZeroCurrency,
		LockedStorageCollateral:           types.ZeroCurrency,
		PotentialContractCompensation:     types.ZeroCurrency,
		PotentialStorageRevenue:           types.ZeroCurrency,
		PotentialDownloadBandwidthRevenue: types.ZeroCurrency,
		PotentialUploadBandwidthRevenue:   types.ZeroCurrency,
		RiskedStorageCollateral:           types.ZeroCurrency,
		TransactionFeeExpenses:            types.ZeroCurrency,
		StorageRevenue:                    types.ZeroCurrency,
		DownloadBandwidthRevenue:          types.ZeroCurrency,
		UploadBandwidthRevenue:            types.ZeroCurrency,
	}
	fm.activeContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		summary.PotentialContractCompensation = summary.PotentialContractCompensation.Add(so.ContractCost)
		summary.LockedStorageCollateral = summary.LockedStorageCollateral.Add(so.LockedCollateral)
		summary.PotentialStorageRevenue = summary.PotentialStorageRevenue.Add(so.PotentialStorageRevenue)
		summary.PotentialDownloadBandwidthRevenue = summary.PotentialDownloadBandwidthRevenue.Add(so.PotentialDownloadRevenue)
		summary.PotentialUploadBandwidthRevenue = summary.PotentialUploadBandwidthRevenue.Add(so.PotentialUploadRevenue)
		summary.RiskedStorageCollateral = summary.RiskedStorageCollateral.Add(so.RiskedCollateral)
		summary.TransactionFeeExpenses = summary.TransactionFeeExpenses.Add(so.TransactionFeesAdded)
		return true
	})
	summary.ContractCount = fm.ContractCount()

	fm.expiredContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		summary.ContractCompensation = summary.ContractCompensation.Add(so.ContractCost)
		if so.ObligationStatus == obligationSucceeded.String() || so.ObligationStatus == obligationRenewed.String() {
			summary.StorageRevenue = summary.StorageRevenue.Add(so.PotentialStorageRevenue)
			summary.DownloadBandwidthRevenue = summary.DownloadBandwidthRevenue.Add(so.PotentialDownloadRevenue)
			summary.UploadBandwidthRevenue = summary.UploadBandwidthRevenue.Add(so.PotentialUploadRevenue)
		}
		if so.ObligationStatus == obligationFailed.String() {
			summary.LostStorageCollateral = summary.LostStorageCollateral.Add(so.RiskedCollateral)
			if len(so.MissedProofOutputs) == 3 { // If it is a renewed contract there is no burned coins
				summary.LostRevenue = summary.LostRevenue.Add(so.MissedProofOutputs[2].Value)
				if so.MissedProofOutputs[2].Value.Cmp(so.RiskedCollateral) > 0 {
					summary.LostRevenue = summary.LostRevenue.Sub(so.RiskedCollateral)
				}
			}
		}
		summary.TransactionFeeExpenses = summary.TransactionFeeExpenses.Add(so.TransactionFeesAdded)
		return true
	})
	summary.ExpiredContractCount = fm.ExpiredContractCount()

	fm.unconfirmedContracts.Range(func(key, value interface{}) bool {
		so := value.(modules.StorageObligation)
		summary.UnconfirmedLockedCollateral = summary.UnconfirmedLockedCollateral.Add(so.LockedCollateral)
		return true
	})
	summary.UnconfirmedContractCount = fm.UnconfirmedContractCount()
	return summary
}

func (fm *hostFinancialMetrics) registerStorageObligation(so modules.StorageObligation, currentBlockHeight types.BlockHeight) {
	// If obligation expired move to expired bucket
	// That prevents it including in any collateral math
	if so.ExpirationHeight < currentBlockHeight {
		if _, present := fm.unconfirmedContracts.LoadAndDelete(so.ObligationId); present {
			fm.unconfirmedContractCount.Add(-1)
		}
		if _, present := fm.activeContracts.LoadAndDelete(so.ObligationId); present {
			fm.activeContractCount.Add(-1)
		}
		if _, present := fm.expiredContracts.Swap(so.ObligationId, so); !present {
			fm.expiredContractCount.Add(1)
		}
		return
	}
	// If not on blockchain yet do not include in accounting
	// just store in unconfirmed list and return
	if !so.OriginConfirmed {
		if _, present := fm.expiredContracts.LoadAndDelete(so.ObligationId); present {
			fm.expiredContractCount.Add(-1)
		}
		if _, present := fm.activeContracts.LoadAndDelete(so.ObligationId); present {
			fm.activeContractCount.Add(-1)
		}
		if _, present := fm.unconfirmedContracts.Swap(so.ObligationId, so); !present {
			fm.unconfirmedContractCount.Add(1)
		}
		return
	}
	// Storage obligation is confirmed and not expired, remove from unconfirmed list
	// and in case it is a blockchain reorg remove it from expired contracts too
	if _, present := fm.unconfirmedContracts.LoadAndDelete(so.ObligationId); present {
		fm.unconfirmedContractCount.Add(-1)
	}
	if _, present := fm.expiredContracts.LoadAndDelete(so.ObligationId); present {
		fm.expiredContractCount.Add(-1)
	}
	if _, present := fm.activeContracts.Swap(so.ObligationId, so); !present {
		fm.activeContractCount.Add(1)
	}
}

func (fm *hostFinancialMetrics) unregisterStorageObligation(soid types.FileContractID) {
	if _, present := fm.expiredContracts.LoadAndDelete(soid); present {
		fm.expiredContractCount.Add(-1)
	}
	if _, present := fm.unconfirmedContracts.LoadAndDelete(soid); present {
		fm.unconfirmedContractCount.Add(-1)
	}
	if _, present := fm.activeContracts.LoadAndDelete(soid); present {
		fm.activeContractCount.Add(-1)
	}
}

func (fm *hostFinancialMetrics) StorageObligationMetaData(soid types.FileContractID) (modules.StorageObligation, bool) {
	if value, ok := fm.unconfirmedContracts.Load(soid); ok {
		return value.(modules.StorageObligation), true
	}

	if value, ok := fm.activeContracts.Load(soid); ok {
		return value.(modules.StorageObligation), true
	}

	if value, ok := fm.expiredContracts.Load(soid); ok {
		return value.(modules.StorageObligation), true
	}

	return modules.StorageObligation{}, false
}

func (fm *hostFinancialMetrics) UnconfirmedContractIDs() []types.FileContractID {
	ids := make([]types.FileContractID, 0)
	fm.unconfirmedContracts.Range(func(key, value interface{}) bool {
		id := key.(types.FileContractID)
		ids = append(ids, id)
		return true
	})
	return ids
}

func (fm *hostFinancialMetrics) ActiveContractIDs() []types.FileContractID {
	ids := make([]types.FileContractID, 0)
	fm.activeContracts.Range(func(key, value interface{}) bool {
		id := key.(types.FileContractID)
		ids = append(ids, id)
		return true
	})
	return ids
}

func (fm *hostFinancialMetrics) ExpiredContractIDs() []types.FileContractID {
	ids := make([]types.FileContractID, 0)
	fm.expiredContracts.Range(func(key, value interface{}) bool {
		id := key.(types.FileContractID)
		ids = append(ids, id)
		return true
	})
	return ids
}
