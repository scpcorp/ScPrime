package host

import (
	"fmt"

	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/types"
	bolt "go.etcd.io/bbolt"
)

// UnconfirmedContractDataRetentionPeriod is the period any unconfirmed contract
// is taken into account when preserving sector references. The sectors referenced are included
// in the resulting reference list of host.getReferencedSectorIDs()
// TODO: decrease the retention period when contract renewal fix proves itself to be effective
var UnconfirmedContractDataRetentionPeriod = types.BlocksPerDay * 27

// MaxAllowedUnreferencedSectorsToRemove is the ceiling for sector removal when
// removing unconfirmed expired contracts
var MaxAllowedUnreferencedSectorsToRemove = 256

// removeReferencedSectorsFromContract removes referenced sectors from a given storage obligation.
// This methos is used to prevent accidental deletion of stored data sectors and should be called on contract
// before it is deleted or closed on expiry.
//
// If the contract is not confirmed or expired, it returns an error.
// If the contract has no referenced sectors, it returns nil.
// Otherwise, it updates the contract's sector roots to only include sectors that are not
// found in the supplied referencedSectorIDs and returns nil.
func (h *Host) removeReferencedSectorsFromContract(contract *storageObligation, referencedSectorIDs map[crypto.Hash]bool) error {
	// If it is empty (no referenced sectors) it can be removed from contract database without additional checks
	if len(contract.SectorRoots) == 0 {
		return nil
	}
	// Check if it has yet any chance to get confirmed (expiration height should be less than current blockheight)
	if contract.expiration() > h.blockHeight {
		return fmt.Errorf("removing referenced sectors from contract %s not allowed, it is not expired yet", contract.id())
	}
	newSectorRoots := make([]crypto.Hash, 0)
	// Iterate over referenced sectors in contractToDelete and keep the unreferenced only
	for _, sectorID := range contract.SectorRoots {
		if !referencedSectorIDs[sectorID] {
			h.log.Debugf("Marking sector %s in contract as unreferenced (allowed to delete)", sectorID)
			newSectorRoots = append(newSectorRoots, sectorID)
		}
	}
	contract.SectorRoots = newSectorRoots
	return nil
}

func (h *Host) getReferencedSectorIDs() (map[crypto.Hash]bool, error) {
	// Get a list of relevant contractIDs (Active and recent Unconfirmed)
	relevantContractIDs := h.financialMetrics.ActiveContractIDs()
	for _, cid := range h.financialMetrics.UnconfirmedContractIDs() {
		// Add to the list only if unconfirmed contract is less than UnconfirmedContractDataRetentionPeriod old
		md, ok := h.financialMetrics.StorageObligationMetaData(cid)
		if ok && h.blockHeight < md.ExpirationHeight &&
			h.blockHeight >= md.NegotiationHeight && h.blockHeight <= md.NegotiationHeight+UnconfirmedContractDataRetentionPeriod {
			relevantContractIDs = append(relevantContractIDs, cid)
		}
	}

	referencedSectorIDs := make(map[crypto.Hash]bool)
	// Exit if no relevant contracts found
	if len(relevantContractIDs) == 0 {
		return referencedSectorIDs, nil
	}

	// Build list of referenced sectors
	// Find referenced sectors in tokenstorage
	tokensectors, err := h.tokenStor.ReferencedSectorIDs()
	if err != nil {
		return referencedSectorIDs, fmt.Errorf("failed to read referenced sector IDs from tokenstorage for sector usage mapping: %w", err)
	}
	for _, sectorID := range tokensectors {
		referencedSectorIDs[sectorID] = true
	}

	// Iterate through relevant contracts
	if err = h.db.View(func(tx *bolt.Tx) error {
		for _, cid := range relevantContractIDs {
			so, err := h.getStorageObligation(tx, cid)
			if err != nil {
				return err
			}
			//Read contract sector IDs and put them ino referenced sectors map
			for _, sectorID := range so.SectorRoots {
				referencedSectorIDs[sectorID] = true
			}
		}
		return nil
	}); err != nil {
		return referencedSectorIDs, fmt.Errorf("error reading contracts from database: %w", err)
	}
	return referencedSectorIDs, nil
}

func (h *Host) deleteContractFromDB(contractID types.FileContractID) error {
	err := h.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketStorageObligations)
		h.log.Debugf("Deleting contract %v from database", contractID.String())
		return bucket.Delete([]byte(contractID[:]))
	})
	if err != nil {
		return err
	}
	h.financialMetrics.unregisterStorageObligation(contractID)
	return nil
}
