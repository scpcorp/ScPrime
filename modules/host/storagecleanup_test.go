package host

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/scpcorp/ScPrime/build"
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/modules/consensus"
	"gitlab.com/scpcorp/ScPrime/modules/gateway"
	"gitlab.com/scpcorp/ScPrime/modules/host/api"
	"gitlab.com/scpcorp/ScPrime/modules/miner"
	"gitlab.com/scpcorp/ScPrime/modules/transactionpool"
	"gitlab.com/scpcorp/ScPrime/modules/wallet"
	"gitlab.com/scpcorp/ScPrime/types"
	bolt "go.etcd.io/bbolt"
)

type testingSet struct {
	transactionpool *transactionpool.TransactionPool
	host            *Host
	hostWallet      *wallet.Wallet
	renterWallet    *wallet.Wallet
	renterSK        crypto.SecretKey
	renterPK        types.SiaPublicKey
	miner           modules.TestMiner
	minerWallet     *wallet.Wallet
	closeFn
}

func newTestingSet(t *testing.T) (*testingSet, error) {
	testdir := build.TempDir("host_test", t.Name())

	g, err := gateway.New("127.0.0.1:0", false, filepath.Join(testdir, modules.GatewayDir))
	if err != nil {
		return nil, err
	}
	cs, errChan := consensus.New(g, false, filepath.Join(testdir, modules.ConsensusDir))
	if err := <-errChan; err != nil {
		return nil, err
	}
	tp, err := transactionpool.New(cs, g, filepath.Join(testdir, modules.TransactionPoolDir))
	if err != nil {
		return nil, err
	}
	minerWallet, err := wallet.New(cs, tp, filepath.Join(testdir, modules.WalletDir))
	if err != nil {
		return nil, err
	}
	key := crypto.GenerateSiaKey(crypto.TypeDefaultWallet)
	encrypted, err := minerWallet.Encrypted()
	if err != nil {
		return nil, err
	}
	if !encrypted {
		_, err = minerWallet.Encrypt(key)
		if err != nil {
			return nil, err
		}
	}
	err = minerWallet.Unlock(key)
	if err != nil {
		return nil, err
	}
	m, err := miner.New(cs, tp, minerWallet, filepath.Join(testdir, modules.MinerDir))
	if err != nil {
		return nil, err
	}

	hostWallet, err := wallet.New(cs, tp, filepath.Join(testdir, "host_"+modules.WalletDir))
	if err != nil {
		return nil, err
	}
	hwkey := crypto.GenerateSiaKey(crypto.TypeDefaultWallet)
	encrypted, err = hostWallet.Encrypted()
	if err != nil {
		return nil, err
	}
	if !encrypted {
		_, err = hostWallet.Encrypt(hwkey)
		if err != nil {
			return nil, err
		}
	}
	err = hostWallet.Unlock(hwkey)
	if err != nil {
		return nil, err
	}

	host, err := New(cs, g, tp, hostWallet, "127.0.0.1:0", filepath.Join(testdir, modules.HostDir), nil, 5*time.Second)
	if err != nil {
		return nil, err
	}

	renterWallet, err := wallet.New(cs, tp, filepath.Join(testdir, "renter_"+modules.WalletDir))
	if err != nil {
		return nil, err
	}
	rwkey := crypto.GenerateSiaKey(crypto.TypeDefaultWallet)
	encrypted, err = renterWallet.Encrypted()
	if err != nil {
		return nil, err
	}
	if !encrypted {
		_, err = renterWallet.Encrypt(rwkey)
		if err != nil {
			return nil, err
		}
	}
	err = renterWallet.Unlock(rwkey)
	if err != nil {
		return nil, err
	}

	closeFunc := func() error {
		cerr := host.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		cerr = renterWallet.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		cerr = hostWallet.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		cerr = minerWallet.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		cerr = tp.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		cerr = cs.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		cerr = g.Close()
		if cerr != nil {
			t.Errorf("Close error: %v", cerr)
		}
		return nil
	}

	// Mine blocks to get coins
	for i := types.BlockHeight(0); i <= types.MaturityDelay; i++ {
		_, err := m.AddBlock()
		if err != nil {
			closeFunc()
			return nil, err
		}
	}

	// Send coins to host
	mb, err := minerWallet.ConfirmedBalance()
	if err != nil {
		closeFunc()
		return nil, err
	}
	huc, err := hostWallet.NextAddress()
	if err != nil {
		closeFunc()
		return nil, err
	}
	_, err = minerWallet.SendSiacoins(mb.CoinBalance.Div64(3), huc.UnlockHash())
	if err != nil {
		closeFunc()
		return nil, err
	}
	ruc, err := renterWallet.NextAddress()
	if err != nil {
		closeFunc()
		return nil, err
	}
	_, err = minerWallet.SendSiacoins(mb.CoinBalance.Div64(3), ruc.UnlockHash())
	if err != nil {
		closeFunc()
		return nil, err
	}
	_, err = m.AddBlock()
	if err != nil {
		closeFunc()
		return nil, err
	}
	// Create storage folder
	storageFolderOne := filepath.Join(testdir, modules.HostDir, "storagefolder")
	err = os.Mkdir(storageFolderOne, 0700)
	if err != nil {
		closeFunc()
		return nil, err
	}
	err = host.AddStorageFolder(storageFolderOne, modules.SectorSize*64)
	if err != nil {
		closeFunc()
		return nil, err
	}

	sk, pk := crypto.GenerateKeyPair()
	renterPK := types.SiaPublicKey{
		Algorithm: types.SignatureEd25519,
		Key:       pk[:],
	}

	return &testingSet{
		transactionpool: tp,
		host:            host,
		hostWallet:      hostWallet,
		renterWallet:    renterWallet,
		renterSK:        sk,
		renterPK:        renterPK,
		miner:           m,
		minerWallet:     minerWallet,
		closeFn:         closeFunc,
	}, nil
}

// newContract uses the host and renter wallet to create and fund a file
// contract that will form the foundation of a storage obligation.
func (set *testingSet) newContract(length uint64) (storageObligation, error) {
	builder, err := set.renterWallet.StartTransaction()
	if err != nil {
		return storageObligation{}, err
	}
	// Fund the file contract with a payout. The payout needs to be big enough
	// that the expected revenue is larger than the fee that the host may end
	// up paying.
	uc, err := set.renterWallet.NextAddress()
	if err != nil {
		builder.Drop()
		return storageObligation{}, fmt.Errorf("can not get host unlockhash: %w", err)
	}
	payout := types.SiacoinPrecision.Mul64(10e3)
	collateral := types.SiacoinPrecision.Mul64(10)
	err = builder.FundSiacoinsFixedAddress(payout, uc, uc)
	if err != nil {
		builder.Drop()
		return storageObligation{}, fmt.Errorf("unable to fund storage obligation: %w", err)
	}
	// Add the file contract that consumes the funds.
	blockHeight, err := set.renterWallet.Height()
	if err != nil {
		builder.Drop()
		return storageObligation{}, fmt.Errorf("unable to get wallet blockheight: %w", err)
	}
	_ = builder.AddFileContract(types.FileContract{
		// Because this file contract needs to be able to accept file contract
		// revisions, the expiration is put more than
		// 'revisionSubmissionBuffer' blocks into the future.
		WindowStart: blockHeight + revisionSubmissionBuffer + types.BlockHeight(length),
		WindowEnd:   blockHeight + revisionSubmissionBuffer + modules.DefaultWindowSize + types.BlockHeight(length),

		Payout: payout,
		ValidProofOutputs: []types.SiacoinOutput{
			{
				Value:      types.PostTax(blockHeight, payout).Sub(collateral),
				UnlockHash: uc.UnlockHash(),
			},
			{
				Value:      collateral,
				UnlockHash: set.host.ExternalSettings().UnlockHash,
			},
		},
		MissedProofOutputs: []types.SiacoinOutput{
			{
				Value:      types.PostTax(blockHeight, payout).Sub(collateral),
				UnlockHash: uc.UnlockHash(),
			},
			{
				Value:      collateral,
				UnlockHash: set.host.ExternalSettings().UnlockHash,
			},
			{
				UnlockHash: types.BurnAddressUnlockHash,
				Value:      types.ZeroCurrency,
			},
		},
		UnlockHash:     uc.UnlockHash(),
		RevisionNumber: 0,
	})
	// Sign the transaction.
	tSet, err := builder.Sign(true)
	if err != nil {
		return storageObligation{}, err
	}

	// Assemble and return the storage obligation.
	so := storageObligation{
		OriginTransactionSet: tSet,
		h:                    set.host,
	}
	ht := hostTester{host: set.host, wallet: set.hostWallet}
	so, err = ht.addNoOpRevision(so, set.renterPK)

	return so, err
}

func (set *testingSet) attachSectors(tokenID types.TokenID, id types.FileContractID, sectors [][]byte) error {
	contract, err := set.host.managedGetStorageObligation(id)
	if err != nil {
		return err
	}
	set.host.log.Debugf("Attaching %d sectors to %s", len(sectors), id)
	// Attach 5 sectors to contract
	sectorsToAttach := make([]api.TokenAndSector, 0, len(sectors))
	sectorIDsToAttach := make([]crypto.Hash, 0, len(sectors))

	for i := 0; i < len(sectors); i++ {
		sectorIDsToAttach = append(sectorIDsToAttach, crypto.MerkleRoot(sectors[i]))
		sectorsToAttach = append(sectorsToAttach, api.TokenAndSector{
			Authorization: tokenID.String(),
			SectorID:      sectorIDsToAttach[i],
		})
	}
	contract.SectorRoots = append(contract.SectorRoots, sectorIDsToAttach...)
	newFileMerkleRoot := cachedMerkleRoot(contract.SectorRoots)
	newFileSize := modules.SectorSize * uint64(len(contract.SectorRoots))

	// Create new revision.
	currentRevision, err := contract.recentRevision()
	if err != nil {
		return err
	}
	sectorCost := types.SiacoinPrecision.Mul64(50)
	newRevision := types.FileContractRevision{
		ParentID:          id, //contract.id(),
		UnlockConditions:  currentRevision.UnlockConditions,
		NewRevisionNumber: currentRevision.NewRevisionNumber + 1,
		NewFileSize:       newFileSize,
		NewFileMerkleRoot: newFileMerkleRoot,
		NewWindowStart:    currentRevision.NewWindowStart,
		NewWindowEnd:      currentRevision.NewWindowEnd,
		NewValidProofOutputs: []types.SiacoinOutput{
			{UnlockHash: currentRevision.NewValidProofOutputs[0].UnlockHash, Value: currentRevision.NewValidProofOutputs[0].Value.Sub(sectorCost)},
			{UnlockHash: currentRevision.NewValidProofOutputs[1].UnlockHash, Value: currentRevision.NewValidProofOutputs[1].Value.Add(sectorCost)},
		},
		NewMissedProofOutputs: []types.SiacoinOutput{
			{UnlockHash: currentRevision.NewMissedProofOutputs[0].UnlockHash, Value: currentRevision.NewMissedProofOutputs[0].Value.Sub(sectorCost)},
			{UnlockHash: currentRevision.NewMissedProofOutputs[1].UnlockHash, Value: currentRevision.NewMissedProofOutputs[1].Value.Sub(sectorCost.Div64(1000000000))},
			{UnlockHash: types.BurnAddressUnlockHash, Value: currentRevision.NewMissedProofOutputs[2].Value.Add(sectorCost).Add(sectorCost.Div64(1000000000))},
		},
		NewUnlockHash: currentRevision.NewUnlockHash,
	}
	contract.PotentialStorageRevenue = contract.PotentialStorageRevenue.Add(sectorCost)
	signedTxn := types.Transaction{
		FileContractRevisions: []types.FileContractRevision{newRevision},
		TransactionSignatures: []types.TransactionSignature{{
			ParentID:       crypto.Hash(id /*contract.id()*/),
			CoveredFields:  types.CoveredFields{FileContractRevisions: []uint64{0}},
			PublicKeyIndex: 0,
		}},
	}
	hash := signedTxn.SigHash(0, set.host.BlockHeight())
	sig := crypto.SignHash(hash, set.renterSK)
	contract.RevisionTransactionSet = append(contract.RevisionTransactionSet, signedTxn)

	attachReq := &api.AttachSectorsRequest{
		ContractID:      id, //contract.id(),
		Sectors:         sectorsToAttach,
		Revision:        newRevision,
		RenterSignature: sig[:],
		BlockHeight:     set.host.BlockHeight(),
	}
	hostApi := api.NewAPI(set.host.tokenStor, set.host.secretKey, set.host)
	_, err = hostApi.AttachSectors(context.Background(), attachReq)
	return err
}

func (set *testingSet) invalidateContract(soid types.FileContractID) (storageObligation, error) {
	var mso storageObligation
	// Invalidate the contract by corrupting OriginTransactionSet
	err := set.host.db.Update(func(tx *bolt.Tx) error {
		// Get the latest storage obligation in case it has changed
		so, err2 := set.host.getStorageObligation(tx, soid)
		if err2 != nil {
			return fmt.Errorf("error reading contract %v from database: %w", soid, err2)
		}
		otx := so.OriginTransactionSet
		lasttx := otx[len(otx)-1]
		lasttx.SiacoinInputs[0] = types.SiacoinInput{}
		otx[len(otx)-1] = lasttx

		mso = storageObligation{
			SectorRoots:              so.SectorRoots,
			ContractCost:             so.ContractCost,
			LockedCollateral:         so.LockedCollateral,
			PotentialAccountFunding:  so.PotentialAccountFunding,
			PotentialDownloadRevenue: so.PotentialDownloadRevenue,
			PotentialStorageRevenue:  so.PotentialStorageRevenue,
			PotentialUploadRevenue:   so.PotentialUploadRevenue,
			RiskedCollateral:         so.RiskedCollateral,
			TransactionFeesAdded:     so.TransactionFeesAdded,
			NegotiationHeight:        so.NegotiationHeight,
			OriginTransactionSet:     otx,
			RevisionTransactionSet:   so.RevisionTransactionSet,
			h:                        set.host,
		}
		// Store the new storage obligation to replace the old one.
		set.host.log.Debugf("Corrupting %v in host.db, setting OriginConfirmed to false", soid)
		soBytes, err := json.Marshal(mso)
		if err != nil {
			return err
		}
		err2 = tx.Bucket(bucketStorageObligations).Put(soid[:], soBytes)
		if err2 != nil {
			return fmt.Errorf("error updating contract %v in database: %w", soid, err2)
		}
		newID := mso.id()
		err2 = tx.Bucket(bucketStorageObligations).Put(newID[:], soBytes)
		if err2 != nil {
			return fmt.Errorf("error adding duplicate contract %v to database: %w", newID, err2)
		}
		return nil
	})
	if err != nil {
		return mso, fmt.Errorf("Corrupting contract failed: %w", err)
	}
	set.host.managedLockStorageObligation(mso.id())
	err = set.host.managedModifyStorageObligation(mso, nil, nil)
	set.host.managedUnlockStorageObligation(mso.id())
	return mso, err
}

func (set *testingSet) logContracts(t *testing.T) {
	if err := set.host.db.Sync(); err != nil {
		t.Fatalf("Error syncing host contract database: %v", err)
	}
	sos := set.host.StorageObligations()
	t.Logf("Total contracts: %d", len(sos))
	for xi, xo := range sos {
		sxo, err := set.host.managedGetStorageObligation(xo.ObligationId)
		if err != nil {
			if errors.Is(err, ErrNoStorageObligation) {
				continue
			}
			t.Logf("reading contract %v failed: %v", xo.ObligationId, err)
		}
		t.Logf("contract %d: %v, sectors: %d, status %s", xi, xo.ObligationId, xo.SectorRootsCount, set.host.verifyStorageObligationStatus(sxo, set.host.BlockHeight()))
	}
	ecids := set.host.financialMetrics.ExpiredContractIDs()
	acids := set.host.financialMetrics.ActiveContractIDs()
	ucids := set.host.financialMetrics.UnconfirmedContractIDs()
	t.Logf("FM: Active contracts %d: %v", len(acids), acids)
	t.Logf("FM: Unconfirmed contracts %d: %v", len(ucids), ucids)
	t.Logf("FM: Expired contracts %d: %v", len(ecids), ecids)
}

// TestRemoveReferencedSectorsFromContract checks that the host correctly identifies and removes the referenced
// sectors from a contract
func TestRemoveReferencedSectorsFromContract(t *testing.T) {
	set, err := newTestingSet(t)
	if err != nil {
		t.Fatalf("error setting up testing set: %v", err)
	}
	defer set.closeFn()

	// Generate token
	b := fastrand.Bytes(16)
	var tokenID types.TokenID
	copy(tokenID[:], b)
	err = set.host.tokenStor.AddResources(tokenID, modules.Storage, 500000)
	if err != nil {
		t.Fatal("error adding storage resource to token")
	}
	err = set.host.tokenStor.AddResources(tokenID, modules.UploadBytes, 300000)
	if err != nil {
		t.Fatal("error adding upload resource to token")
	}
	// Add a storage obligation, which should increment contract count
	so, err := set.newContract(20)
	if err != nil {
		t.Fatal(err)
	}
	set.host.managedLockStorageObligation(so.id())
	err = set.host.managedAddStorageObligation(so)
	set.host.managedUnlockStorageObligation(so.id())
	if err != nil {
		t.Fatal(err)
	}

	status := set.host.verifyStorageObligationStatus(so, set.host.BlockHeight())
	if status != obligationNotConfirmed {
		t.Errorf("Contract should have status unconfirmed but has %v", status.String())
	}

	references, err := set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	if len(references) > 0 {
		t.Fatalf("Host should have no referenced sectors yet but has %d", len(references))
	}

	// Upload 10 sectors
	hostApi := api.NewAPI(set.host.tokenStor, set.host.secretKey, set.host)

	// form upload with token request
	req := &api.UploadWithTokenRequest{
		Authorization: tokenID.String(),
	}
	// generate sectors to upload
	req.Sectors = nil
	for i := 0; i < 10; i++ {
		req.Sectors = append(req.Sectors, fastrand.Bytes(int(modules.SectorSize)))
	}

	// Send to host
	_, err = hostApi.UploadWithToken(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}

	tssids, err := set.host.tokenStor.ReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	if len(tssids) != 10 {
		t.Errorf("Expected 10 sector ids in tokenstorage but got %d", len(tssids))
	}

	// Check if host referenced sectors have 10 too
	references, err = set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	if len(references) != 10 {
		t.Errorf("Expected 10 sector references but got %d", len(references))
	}

	if set.host.financialMetrics.UnconfirmedContractCount() != 1 {
		t.Errorf("Expected to have 1 unconfirmed contract but got %d", set.host.financialMetrics.UnconfirmedContractCount())
	}
	if set.host.financialMetrics.ContractCount() > 0 {
		t.Errorf("Expected to have no confirmed contracts as no blocks were added but got %d", set.host.financialMetrics.ContractCount())
	}

	firstContractId := set.host.financialMetrics.UnconfirmedContractIDs()[0]
	so, err = set.host.managedGetStorageObligation(firstContractId)
	if err != nil {
		t.Fatalf("Error rereading so: %v", err)
	}

	status = set.host.verifyStorageObligationStatus(so, set.host.BlockHeight())
	if status != obligationNotConfirmed {
		t.Errorf("contract should have status unconfirmed but has %v", status.String())
	}

	_, err = set.miner.AddBlock()
	if err != nil {
		t.Fatal(err)
	}

	status = set.host.verifyStorageObligationStatus(so, set.host.BlockHeight())
	if status != obligationUnresolved {
		t.Errorf("contract should have status obligationUnresolved but has %v", status.String())
	}

	// Financial metrics should now have migrated the contract to regular (active) bucket
	summary := set.host.FinancialMetrics().Summary()
	if summary.ContractCount != 1 && summary.UnconfirmedContractCount != 0 {
		set.logContracts(t)
		t.Fatalf("Financial metrics should have moved contract from unconfirmed to regular bucket. Financial metrics has : Unconfirmed=%d, Unresolved=%d, Expired=%d",
			summary.UnconfirmedContractCount, summary.ContractCount, summary.ExpiredContractCount)
	}

	sectors := req.Sectors[:5]
	err = set.attachSectors(tokenID, so.id(), sectors)
	if err != nil {
		t.Fatalf("Attach sectors failed: %v", err)
	}

	// Mine a block
	_, err = set.miner.AddBlock()
	if err != nil {
		t.Fatalf("Mining block failed: %v", err)
	}

	so2, err := set.newContract(5)
	if err != nil {
		t.Fatalf("Error creating second contract: %v", err)
	}
	set.host.managedLockStorageObligation(so2.id())
	err = set.host.managedAddStorageObligation(so2)
	set.host.managedUnlockStorageObligation(so2.id())
	if err != nil {
		t.Fatal(err)
	}

	// Mine a block
	_, err = set.miner.AddBlock()
	if err != nil {
		t.Fatalf("Mining block failed: %v", err)
	}

	// Financial metrics should now have 2 active contracts
	summary = set.host.financialMetrics.Summary()
	if summary.ContractCount != 2 && summary.UnconfirmedContractCount != 0 {
		set.logContracts(t)
		t.Fatalf("Financial metrics should have moved contract from unconfirmed to regular bucket. Financial metrics has : Unconfirmed=%d, Unresolved=%d, Expired=%d",
			summary.UnconfirmedContractCount, summary.ContractCount, summary.ExpiredContractCount)
	}

	references, err = set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	if len(references) != 10 {
		t.Fatalf("Host should still have 10 sectors referenced but has %d", len(references))
	}

	// Reload so2
	so2, err = set.host.managedGetStorageObligation(so2.id())
	if err != nil {
		t.Fatalf("Error rereading so2: %v", err)
	}

	// Attach 3 sectors to second contract
	err = set.attachSectors(tokenID, so2.id(), req.Sectors[5:8])
	if err != nil {
		t.Fatalf("Attach sectors failed: %v", err)
	}

	// and again some sectors to first contract
	err = set.attachSectors(tokenID, so.id(), req.Sectors[8:])
	if err != nil {
		t.Fatalf("Attach sectors failed: %v", err)
	}

	// Now there hould still be 10 referenced sectors in total (7+3)
	references, err = set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	if len(references) != 10 {
		t.Fatalf("Host should still have 10 sectors referenced but has %d", len(references))
	}

	// No sectors remaining in token storage
	rs, err := set.host.tokenStor.ReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	if len(rs) != 0 {
		t.Fatalf("Host should not have any sectors in tokenstorage but has %d", len(rs))
	}

	// Expire second contract (will remove 3 sectors)
	for i := 0; i < 12; i++ {
		_, err := set.miner.AddBlock()
		if err != nil {
			t.Fatalf("Mining blocks failed: %v", err)
		}
	}

	// Financial metrics should now have 1 active and 1 expired contracts
	summary = set.host.financialMetrics.Summary()
	if summary.ContractCount != 1 || summary.UnconfirmedContractCount != 0 || summary.ExpiredContractCount != 1 {
		set.logContracts(t)
		t.Fatalf("Financial metrics should have moved one contract from regular to expired bucket. Financial metrics has : Unconfirmed=%d, Unresolved=%d, Expired=%d",
			summary.UnconfirmedContractCount, summary.ContractCount, summary.ExpiredContractCount)
	}

	references, err = set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	// There should be 7 sectors referenced by contracts
	if len(references) != 7 {
		t.Fatalf("Host should have 7 sectors referenced but has %d", len(references))
	}

	// Invalidate corrupts the previous record so that on load it will suddenly have a different id
	so, err = set.invalidateContract(so.id())
	if err != nil {
		t.Fatalf("Corrupting contract failed: %v", err)
	}

	status = set.host.verifyStorageObligationStatus(so, set.host.BlockHeight())
	if status != obligationNotConfirmed {
		t.Errorf("Status after corruption %s, expected %s", status, obligationNotConfirmed)
	}

	// Financial metrics should now have 1 active and 1 expired contracts
	// But the active contract was corrupted so there should be now a contract with corrupt id and a contract with corrupt transaction
	// This is the inconsistency state we should resolve correctly
	var ac, ec, uc uint64

	for _, hso := range set.host.StorageObligations() {
		hsox, err := set.host.managedGetStorageObligation(hso.ObligationId)
		if err != nil {
			t.Fatalf("Error loading contracts after corruption: %v", err)
		}
		switch status := set.host.verifyStorageObligationStatus(hsox, set.host.BlockHeight()); status {
		case obligationUnresolved:
			ac++
		case obligationNotConfirmed:
			uc++
		// all other states indicate it is closed
		case obligationRejected:
			ec++
		case obligationSucceeded:
			ec++
		case obligationFailed:
			ec++
		case obligationRenewed:
			ec++
		case obligationResolutionPending:
			ec++
		}
	}
	summary = set.host.financialMetrics.Summary()
	if summary.ContractCount == ac && summary.UnconfirmedContractCount == uc && summary.ExpiredContractCount == ec {
		set.logContracts(t)
		t.Fatalf("Financial metrics should have inconsistency but has : Unconfirmed=%d, Unresolved=%d, Expired=%d",
			summary.UnconfirmedContractCount, summary.ContractCount, summary.ExpiredContractCount)
	}

	// Make a new longer contract
	so3, err := set.newContract(250)
	if err != nil {
		t.Fatal(err)
	}
	set.host.managedLockStorageObligation(so3.id())
	err = set.host.managedAddStorageObligation(so3)
	set.host.managedUnlockStorageObligation(so3.id())
	if err != nil {
		t.Fatal(err)
	}

	// Re-upload 3 sectors again
	req2 := &api.UploadWithTokenRequest{
		Authorization: tokenID.String(),
	}
	req2.Sectors = req.Sectors[0:3]
	_, err = hostApi.UploadWithToken(context.Background(), req2)
	if err != nil {
		t.Fatal(err)
	}

	// It should fail with contract not confirmed
	err = set.attachSectors(tokenID, so3.id(), req2.Sectors)
	if err == nil {
		t.Fatal("Attach sectors should have failed")
	}

	_, err = set.miner.AddBlock()
	if err != nil {
		t.Fatalf("Mining blocks failed: %v", err)
	}

	// Now it should succeed
	err = set.attachSectors(tokenID, so3.id(), req2.Sectors)
	if err != nil {
		t.Fatalf("Attach sectors failed: %v", err)
	}

	references, err = set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	// There should be 7 sectors referenced as 3 original are removed and 3 newly
	// attached are duplicates of sectors in corrupted contracts
	if len(references) != 7 {
		t.Log("Referenced sectors")
		for k, v := range references {
			t.Logf("%v: %v", k, v)
		}
		t.Log("Token storage sectors")
		ts, err := set.host.tokenStor.ReferencedSectorIDs()
		if err != nil {
			t.Fatal(err)
		}
		for i, sid := range ts {
			t.Logf("%v: %v", i, sid)
		}
		t.Fatalf("Host should have 7 sectors referenced but has %d", len(references))
	}

	_, err = set.miner.AddBlock()
	if err != nil {
		t.Fatalf("Mining blocks failed: %v", err)
	}
	// Trigger audit in 178 blocks when the corrupted unconfirmed contract will have passed blocksInDay blocker
	atomic.StoreUint64(&set.host.scheduledAuditBlockheight, uint64(set.host.blockHeight+178))

	// Expire first contract (will remove 7 sectors) and trigger contract audit
	// So many blocks needed to hit the 144 blocks lagged cleanup
	for i := 0; i < 180; i++ {
		_, err := set.miner.AddBlock()
		if err != nil {
			t.Fatalf("Mining blocks failed: %v", err)
		}
	}

	// Now there has to be just one active contract and one expired in financial metrics
	// and financial metricss should match the state of contract database (boltdb)
	ac, ec, uc = 0, 0, 0
	for _, hso := range set.host.StorageObligations() {
		hsox, err := set.host.managedGetStorageObligation(hso.ObligationId)
		if err != nil {
			t.Fatalf("Error loading contracts after corruption: %v", err)
		}
		switch status := set.host.verifyStorageObligationStatus(hsox, set.host.BlockHeight()); status {
		case obligationUnresolved:
			ac++
		case obligationNotConfirmed:
			uc++
		// All other states indicate it is closed
		case obligationRejected:
			ec++
		case obligationSucceeded:
			ec++
		case obligationFailed:
			ec++
		case obligationRenewed:
			ec++
		case obligationResolutionPending:
			ec++
		}
	}
	summary = set.host.financialMetrics.Summary()
	if summary.ContractCount != ac || summary.UnconfirmedContractCount != uc || summary.ExpiredContractCount != ec {
		set.logContracts(t)
		t.Logf("Contract database has Unconfirmed=%d, Unresolved=%d, Expired=%d", ac, uc, ec)
		t.Fatalf("Financial metrics should not have inconsistency but has : Unconfirmed=%d, Unresolved=%d, Expired=%d",
			summary.UnconfirmedContractCount, summary.ContractCount, summary.ExpiredContractCount)
	}

	references, err = set.host.getReferencedSectorIDs()
	if err != nil {
		t.Fatal(err)
	}
	// Now there should be only 3 sectors referenced that are
	// attached to the one active contract
	if len(references) != 3 {
		t.Log("Referenced sectors")
		for k, v := range references {
			t.Logf("%v: %v", k, v)
		}
		t.Log("Token storage sectors")
		ts, err := set.host.tokenStor.ReferencedSectorIDs()
		if err != nil {
			t.Fatal(err)
		}
		for i, sid := range ts {
			t.Logf("%v: %v", i, sid)
		}
		t.Fatalf("Host should have 3 sectors referenced but has %d", len(references))
	}

	// Test host.db compaction
	// After host.db compaction file size should have reduced
	// but all the host statistics should remain the same
	fi, err := os.Stat(set.host.db.Path())
	if err != nil {
		t.Fatal(err)
	}
	fm1 := set.host.FinancialMetrics()
	summary1 := fm1.Summary()
	err = set.host.compactDatabase()
	if err != nil {
		t.Fatal(err)
	}
	fi2, err := os.Stat(set.host.db.Path())
	if err != nil {
		t.Fatal(err)
	}
	if fi.Size() <= fi2.Size() {
		t.Errorf("Expected the size to shrink but compacting results are %d -> %d", fi.Size(), fi2.Size())
	}
	fm2 := set.host.FinancialMetrics()
	summary2 := fm2.Summary()
	if &fm1 == &fm2 {
		t.Error("Financial metrics not rebuilt!")
	}
	if !reflect.DeepEqual(summary1, summary2) {
		t.Logf("Summary1: %+v", summary1)
		t.Logf("Summary2: %+v", summary2)
		t.Error("Active contract count changed")
	}
}
