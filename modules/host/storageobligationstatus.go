package host

import (
	"bytes"
	"math"
	"strconv"

	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/types"
)

// The storage obligation status definitions
const (
	obligationUnresolved        storageObligationStatus = iota // Indicates that the obligation is not expired or the proof submission deadline not reached.
	obligationRejected                                         // Indicates that the obligation never got started, no revenue gained or lost.
	obligationSucceeded                                        // Indicates that the obligation was completed, revenues were gained.
	obligationFailed                                           // Indicates that the obligation failed, revenues and collateral were lost.
	obligationRenewed                                          // Indicates that the obligation was renewed.
	obligationNotConfirmed                                     // Indicates associated transaction is not on blockchain
	obligationResolutionPending                                // Indicates storage proof deadline reached but not processed (still has referenced sectors)
)

// storageObligationStatus indicates the current status of a storage obligation
type storageObligationStatus uint64

// String converts a storageObligationStatus to a string.
func (i storageObligationStatus) String() string {
	if i == obligationUnresolved {
		return "obligationUnresolved"
	}
	if i == obligationRejected {
		return "obligationRejected"
	}
	if i == obligationSucceeded {
		return "obligationSucceeded"
	}
	if i == obligationFailed {
		return "obligationFailed"
	}
	if i == obligationRenewed {
		return "obligationRenewed"
	}
	if i == obligationNotConfirmed {
		return "obligationNotConfirmed"
	}
	if i == obligationResolutionPending {
		return "obligationResolutionPending"
	}
	return "storageObligationStatus(" + strconv.FormatInt(int64(i), 10) + ")"
}

// verifyStorageObligationStatus checks the storage obligation for presence in
// blockchain, expiration and potential renewal.
// The result should be usable to judge if it is safe to remove the data attached
// to the storage obligation (expired but not renewed) or should stay if it
// got renewed.
func (h *Host) verifyStorageObligationStatus(so storageObligation, hbh types.BlockHeight) storageObligationStatus {
	// If the status stored in storage obligation tells succeeded, rejected or failed means
	// it was closed already and there is no point in checkin it again
	if so.ObligationStatus == obligationRejected ||
		so.ObligationStatus == obligationSucceeded ||
		so.ObligationStatus == obligationFailed {
		return so.ObligationStatus
	}
	if so.ProofConfirmed {
		return obligationSucceeded
	}

	// Check if blockchain confirmed the storage obligation
	if len(so.OriginTransactionSet) < 1 {
		return obligationNotConfirmed
	}
	// Check if the contract transaction is confirmed in the blockchain if that is not known yet
	if !so.OriginConfirmed {
		txid := so.transactionID()
		confirmed, err := h.tpool.TransactionConfirmed(txid)
		h.log.Debugf("Contract transaction %v check: confirmed=%v, err=%v", txid.String(), confirmed, err)
		// The only potential error is if daemon shutting down, there is no reason to continue checks
		// return the already set value
		if err != nil {
			h.log.Errorf("Error checking transaction confirmation: %v", err)
			return so.ObligationStatus
		}
		if !confirmed {
			// No other status verification on not confirmed storage obligations
			return obligationNotConfirmed
		}
	}

	// Obligation could be already renewed (before expiration)
	// RenewAndClear sets these:
	// newRevision.NewRevisionNumber = math.MaxUint64
	// newRevision.NewFileMerkleRoot = crypto.Hash{}
	// newRevision.NewFileSize = 0
	// and removes all sector roots
	numRevisions := len(so.RevisionTransactionSet)
	if numRevisions > 0 && len(so.SectorRoots) == 0 {
		rev := so.RevisionTransactionSet[numRevisions-1].FileContractRevisions[0]
		if rev.NewFileSize == 0 && rev.NewRevisionNumber == math.MaxUint64 && bytes.Equal(rev.NewFileMerkleRoot.Bytes(), crypto.Hash{}.Bytes()) {
			return obligationRenewed
		}
	}

	soebh := so.expiration()
	if hbh < soebh {
		// Contract expiration not reached return previously stored status
		return obligationUnresolved
	}

	//Contract expiration height reached. Check if storage proof is needed
	if len(so.SectorRoots) == 0 {
		//No sectors used (empty sector roots), must be empty unused so expires as succeeded without a proof
		return obligationSucceeded
	}

	// Check if storage proof is submitted
	if so.ProofConfirmed {
		return obligationSucceeded
	}
	// Storage Obligation has sector roots, needs a proof
	// Contract can be unresolved only if not renewed and waiting for storage proof deadline
	if hbh < so.proofDeadline() {
		// Expired but proof deadline not reached.
		return obligationUnresolved
	}
	if hbh == so.proofDeadline() {
		return obligationResolutionPending
	}
	// Contract storage proof deadline reached but proof missed
	return obligationFailed
}
