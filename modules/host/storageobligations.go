package host

// storageobligations.go is responsible for managing the storage obligations
// within the host - making sure that any file contracts, transaction
// dependencies, file contract revisions, and storage proofs are making it into
// the blockchain in a reasonable time.
//
// NOTE: Currently, the code partially supports changing the storage proof
// window in file contract revisions, however the action item code will not
// handle it correctly. Until the action item code is improved (to also handle
// byzantine situations where the renter submits prior revisions), the host
// should not support changing the storage proof window, especially to further
// in the future.

// TODO: Need to queue the action item for checking on the submission status of
// the file contract revision. Also need to make sure that multiple actions are
// being taken if needed.

// TODO: Make sure that the origin tranasction set is not submitted to the
// transaction pool before addSO is called - if it is, there will be a
// duplicate transaction error, and then the storage obligation will return an
// error, which is bad. Well, or perhas we just need to have better logic
// handling.

// TODO: Need to make sure that 'revision confirmed' is actually looking only
// at the most recent revision (I think it is...)

// TODO: Make sure that not too many action items are being created.

// TODO: The ProofConstructed field of storageObligation
// is not set or used.

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"reflect"
	"sync"
	"time"

	"gitlab.com/NebulousLabs/encoding"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/scpcorp/ScPrime/build"
	"gitlab.com/scpcorp/ScPrime/crypto"
	"gitlab.com/scpcorp/ScPrime/modules"
	"gitlab.com/scpcorp/ScPrime/types"
	bolt "go.etcd.io/bbolt"
)

const (
	// largeContractSize is the threshold at which the largeContractUpdateDelay
	// kicks in whenever modifyStorageObligation is called.
	largeContractSize = 2 * 1 << 40 // 2 TiB
	// largeContractUpdateDelay is the delay applied when calling
	// modifyStorageObligation on an obligation for a contract with a size
	// greater than or equal to largeContractSize.
	largeContractUpdateDelay = 2 * time.Second

	// txnFeeSizeBuffer is a buffer added to the approximate size of a
	// transaction before estimating its fee. This makes it more likely that the
	// txn will be mined in the next block.
	txnFeeSizeBuffer = 300
)

var (
	// errInsaneFileContractOutputCounts is returned when a file contract has
	// the wrong number of outputs for either the valid or missed payouts.
	//
	//lint:ignore U1000 used in isSane() which is currently unused but we want to keep it around
	errInsaneFileContractOutputCounts = errors.New("file contract has incorrect number of outputs for the valid or missed payouts")

	// errInsaneFileContractRevisionOutputCounts is returned when a file
	// contract has the wrong number of outputs for either the valid or missed
	// payouts.
	//
	//lint:ignore U1000 used in isSane() which is currently unused but we want to keep it around
	errInsaneFileContractRevisionOutputCounts = errors.New("file contract revision has incorrect number of outputs for the valid or missed payouts")

	// errInsaneOriginSetFileContract is returned is the final transaction of
	// the origin transaction set of a storage obligation does not have a file
	// contract in the final transaction - there should be a file contract
	// associated with every storage obligation.
	//
	//lint:ignore U1000 used in isSane() which is currently unused but we want to keep it around
	errInsaneOriginSetFileContract = errors.New("origin transaction set of storage obligation should have one file contract in the final transaction")

	// errInsaneOriginSetSize is returned if the origin transaction set of a
	// storage obligation is empty - there should be a file contract associated
	// with every storage obligation.
	//
	//lint:ignore U1000 used in isSane() which is currently unused but we want to keep it around
	errInsaneOriginSetSize = errors.New("origin transaction set of storage obligation is size zero")

	// errInsaneRevisionSetRevisionCount is returned if the final transaction
	// in the revision transaction set of a storage obligation has more or less
	// than one file contract revision.
	//
	//lint:ignore U1000 used in isSane() which is currently unused but we want to keep it around
	errInsaneRevisionSetRevisionCount = errors.New("revision transaction set of storage obligation should have one file contract revision in the final transaction")

	// errInsaneStorageObligationRevision is returned if there is an attempted
	// storage obligation revision which does not have sensical inputs.
	errInsaneStorageObligationRevision = errors.New("revision to storage obligation does not make sense")

	// errInsaneStorageObligationRevisionData is returned if there is an
	// attempted storage obligation revision which does not have sensical
	// inputs.
	//
	//lint:ignore U1000 used in isSane() which is currently unused but we want to keep it around
	errInsaneStorageObligationRevisionData = errors.New("revision to storage obligation has insane data")

	// errNoBuffer is returned if there is an attempted storage obligation that
	// needs to have the storage proof submitted in less than
	// revisionSubmissionBuffer blocks.
	errNoBuffer = errors.New("file contract rejected because storage proof window is too close")

	// errObligationUnlocked is returned when a storage obligation is being
	// removed from lock, but is already unlocked.
	errObligationUnlocked = errors.New("storage obligation is unlocked, and should not be getting unlocked")

	// ErrNoStorageObligation is returned if the requested storage obligation
	// is not found in the database.
	ErrNoStorageObligation = errors.New("storage obligation not found in database")

	// errActionItemQueuedForInvalidBlockheight tells the attemptes addition to action items is for a blockheight that is already in past.
	// That means the action item will never be performed.
	errActionItemQueuedForInvalidBlockheight = errors.New("action item queued for a height less than or equal to current block height")
)

// storageObligation contains all of the metadata related to a file contract
// and the storage contained by the file contract.
type storageObligation struct {
	// Storage obligations are broken up into ordered atomic sectors that are
	// exactly 4MiB each. By saving the roots of each sector, storage proofs
	// and modifications to the data can be made inexpensively by making use of
	// the merkletree.CachedTree. Sectors can be appended, modified, or deleted
	// and the host can recompute the Merkle root of the whole file without
	// much computational or I/O expense.
	SectorRoots []crypto.Hash

	// Variables about the file contract that enforces the storage obligation.
	// The origin an revision transaction are stored as a set, where the set
	// contains potentially unconfirmed transactions.
	ContractCost             types.Currency
	LockedCollateral         types.Currency
	PotentialAccountFunding  types.Currency
	PotentialDownloadRevenue types.Currency
	PotentialStorageRevenue  types.Currency
	PotentialUploadRevenue   types.Currency
	RiskedCollateral         types.Currency
	TransactionFeesAdded     types.Currency

	// The negotiation height specifies the block height at which the file
	// contract was negotiated. If the origin transaction set is not accepted
	// onto the blockchain quickly enough, the contract is pruned from the
	// host. The origin and revision transaction set contain the contracts +
	// revisions as well as all parent transactions. The parents are necessary
	// because after a restart the transaction pool may be emptied out.
	NegotiationHeight      types.BlockHeight
	OriginTransactionSet   []types.Transaction
	RevisionTransactionSet []types.Transaction

	// Variables indicating whether the critical transactions in a storage
	// obligation have been confirmed on the blockchain.
	ObligationStatus    storageObligationStatus
	OriginConfirmed     bool
	ProofConfirmed      bool
	ProofConstructed    bool
	RevisionConfirmed   bool
	RevisionConstructed bool

	h *Host
}

// managedGetStorageObligationSnapshot fetches a storage obligation from the
// database and returns a snapshot.
func (h *Host) managedGetStorageObligationSnapshot(id types.FileContractID) (StorageObligationSnapshot, error) {
	var err error
	var so storageObligation
	h.mu.RLock()
	defer h.mu.RUnlock()

	if err = h.db.View(func(tx *bolt.Tx) error {
		so, err = h.getStorageObligation(tx, id)
		return err
	}); err != nil {
		return StorageObligationSnapshot{}, err
	}

	if len(so.OriginTransactionSet) == 0 {
		return StorageObligationSnapshot{}, errors.New("origin txnset is empty")
	}
	if len(so.RevisionTransactionSet) == 0 {
		return StorageObligationSnapshot{}, errors.New("revision txnset is empty")
	}

	revTxn := so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1]

	return StorageObligationSnapshot{
		staticContractSize:  so.fileSize(),
		staticMerkleRoot:    so.merkleRoot(),
		staticProofDeadline: so.proofDeadline(),
		staticRevisionTxn:   revTxn,
		staticSectorRoots:   so.SectorRoots,
	}, nil
}

// getStorageObligation fetches a storage obligation from the database tx.
func (h *Host) getStorageObligation(tx *bolt.Tx, soid types.FileContractID) (so storageObligation, err error) {
	soBytes := tx.Bucket(bucketStorageObligations).Get(soid[:])
	if soBytes == nil {
		return storageObligation{}, ErrNoStorageObligation
	}
	err = json.Unmarshal(soBytes, &so)
	if err != nil {
		return storageObligation{}, err
	}
	so.h = h
	return so, nil
}

// putStorageObligation places a storage obligation into the database,
// overwriting the existing storage obligation if there is one.
func putStorageObligation(tx *bolt.Tx, so storageObligation) error {
	soBytes, err := json.Marshal(so)
	if err != nil {
		return err
	}
	soid := so.id()
	return tx.Bucket(bucketStorageObligations).Put(soid[:], soBytes)
}

// StorageObligationSnapshot is a snapshot of a StorageObligation. A snapshot is
// a deep-copy and can be accessed without locking at the cost of being a frozen
// readonly representation of an SO which only exists in memory. Note that this
// snapshot only contains the properties required by the MDM to execute a
// program. This can be extended in the future to support other use cases.
type StorageObligationSnapshot struct {
	staticContractSize  uint64
	staticMerkleRoot    crypto.Hash
	staticProofDeadline types.BlockHeight
	staticRevisionTxn   types.Transaction
	staticSectorRoots   []crypto.Hash
}

// ZeroStorageObligationSnapshot returns the storage obligation snapshot of an
// empty contract. All fields are set to the defaults.
func ZeroStorageObligationSnapshot() StorageObligationSnapshot {
	return StorageObligationSnapshot{
		staticContractSize:  0,
		staticMerkleRoot:    crypto.Hash{},
		staticProofDeadline: types.BlockHeight(0),
		staticSectorRoots:   []crypto.Hash{},
		staticRevisionTxn: types.Transaction{
			FileContractRevisions: []types.FileContractRevision{
				{
					NewValidProofOutputs:  make([]types.SiacoinOutput, 2),
					NewMissedProofOutputs: make([]types.SiacoinOutput, 3),
				},
			},
		},
	}
}

// ContractSize returns the size of the underlying contract, which is static and
// is the value of the contract size at the time the snapshot was taken.
func (sos StorageObligationSnapshot) ContractSize() uint64 {
	return sos.staticContractSize
}

// ProofDeadline returns the proof deadline of the underlying contract.
func (sos StorageObligationSnapshot) ProofDeadline() types.BlockHeight {
	return sos.staticProofDeadline
}

// MerkleRoot returns the merkle root, which is static and is the value of the
// merkle root at the time the snapshot was taken.
func (sos StorageObligationSnapshot) MerkleRoot() crypto.Hash {
	return sos.staticMerkleRoot
}

// RecentRevision returns the recent revision at the time the snapshot was
// taken.
func (sos StorageObligationSnapshot) RecentRevision() types.FileContractRevision {
	return sos.staticRevisionTxn.FileContractRevisions[0]
}

// RevisionTxn returns the txn containing the filecontract revision.
func (sos StorageObligationSnapshot) RevisionTxn() types.Transaction {
	return sos.staticRevisionTxn
}

// SectorRoots returns a static list of the sector roots present at the time the
// snapshot was taken.
func (sos StorageObligationSnapshot) SectorRoots() []crypto.Hash {
	return sos.staticSectorRoots
}

// UnallocatedCollateral returns the remaining collateral within the contract
// that hasn't been allocated yet. This means it is not yet moved to the void in
// case of a missed storage proof.
func (sos StorageObligationSnapshot) UnallocatedCollateral() types.Currency {
	return sos.RecentRevision().MissedHostPayout()
}

// StorageObligation returns a storage obligation for use outside of the host
// module
func (so *storageObligation) StorageObligation() modules.StorageObligation {
	valid, missed := so.payouts()
	return modules.StorageObligation{
		ContractCost:             so.ContractCost,
		DataSize:                 so.fileSize(),
		RevisionNumber:           so.revisionNumber(),
		LockedCollateral:         so.LockedCollateral,
		ObligationId:             so.id(),
		PotentialAccountFunding:  so.PotentialAccountFunding,
		PotentialDownloadRevenue: so.PotentialDownloadRevenue,
		PotentialStorageRevenue:  so.PotentialStorageRevenue,
		PotentialUploadRevenue:   so.PotentialUploadRevenue,
		RiskedCollateral:         so.RiskedCollateral,
		SectorRootsCount:         uint64(len(so.SectorRoots)),
		TransactionFeesAdded:     so.TransactionFeesAdded,
		TransactionID:            so.transactionID(),

		ExpirationHeight:  so.expiration(),
		NegotiationHeight: so.NegotiationHeight,
		ProofDeadLine:     so.proofDeadline(),

		ObligationStatus:    so.ObligationStatus.String(),
		OriginConfirmed:     so.OriginConfirmed,
		ProofConfirmed:      so.ProofConfirmed,
		ProofConstructed:    so.ProofConstructed,
		RevisionConfirmed:   so.RevisionConfirmed,
		RevisionConstructed: so.RevisionConstructed,

		ValidProofOutputs:  valid,
		MissedProofOutputs: missed,
	}
}

// Update will take a list of sector changes and update the database to account
// for all of it.
func (so storageObligation) Update(sectorRoots []crypto.Hash, sectorsRemoved map[crypto.Hash]struct{}, sectorsGained map[crypto.Hash][]byte) error {
	so.SectorRoots = sectorRoots
	sr := make([]crypto.Hash, 0, len(sectorsRemoved))
	for sector := range sectorsRemoved {
		sr = append(sr, sector)
	}
	return so.h.managedModifyStorageObligation(so, sr, sectorsGained)
}

// expiration returns the height at which the storage obligation expires.
func (so storageObligation) expiration() types.BlockHeight {
	if len(so.RevisionTransactionSet) > 0 {
		return so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewWindowStart
	}
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].WindowStart
}

// fileSize returns the size of the data protected by the obligation.
func (so storageObligation) fileSize() uint64 {
	if len(so.RevisionTransactionSet) > 0 {
		return so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewFileSize
	}
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].FileSize
}

// id returns the id of the storage obligation, which is defined by the file
// contract id of the file contract that governs the storage contract.
func (so storageObligation) id() types.FileContractID {
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContractID(0)
}

// isSane checks that required assumptions about the storage obligation are
// correct.
//
//lint:ignore U1000 isSane() is currently unused but we want to keep it around
func (so storageObligation) isSane() error {
	// There should be an origin transaction set.
	if len(so.OriginTransactionSet) == 0 {
		build.Critical("origin transaction set is empty")
		return errInsaneOriginSetSize
	}

	// The final transaction of the origin transaction set should have one file
	// contract.
	final := len(so.OriginTransactionSet) - 1
	fcCount := len(so.OriginTransactionSet[final].FileContracts)
	if fcCount != 1 {
		build.Critical("wrong number of file contracts associated with storage obligation:", fcCount)
		return errInsaneOriginSetFileContract
	}

	// The file contract in the final transaction of the origin transaction set
	// should have two valid proof outputs and two missed proof outputs.
	lenVPOs := len(so.OriginTransactionSet[final].FileContracts[0].ValidProofOutputs)
	lenMPOs := len(so.OriginTransactionSet[final].FileContracts[0].MissedProofOutputs)
	if lenVPOs != 2 || lenMPOs != 2 {
		build.Critical("file contract has wrong number of VPOs and MPOs, expecting 2 each:", lenVPOs, lenMPOs)
		return errInsaneFileContractOutputCounts
	}

	// If there is a revision transaction set, there should be one file
	// contract revision in the final transaction.
	if len(so.RevisionTransactionSet) > 0 {
		final = len(so.OriginTransactionSet) - 1
		fcrCount := len(so.OriginTransactionSet[final].FileContractRevisions)
		if fcrCount != 1 {
			build.Critical("wrong number of file contract revisions in final transaction of revision transaction set:", fcrCount)
			return errInsaneRevisionSetRevisionCount
		}

		// The file contract revision in the final transaction of the revision
		// transaction set should have two valid proof outputs and two missed
		// proof outputs.
		lenVPOs = len(so.RevisionTransactionSet[final].FileContractRevisions[0].NewValidProofOutputs)
		lenMPOs = len(so.RevisionTransactionSet[final].FileContractRevisions[0].NewMissedProofOutputs)
		if lenVPOs != 2 || lenMPOs != 2 {
			build.Critical("file contract has wrong number of VPOs and MPOs, expecting 2 each:", lenVPOs, lenMPOs)
			return errInsaneFileContractRevisionOutputCounts
		}
	}
	return nil
}

// merkleRoot returns the file merkle root of a storage obligation.
func (so storageObligation) merkleRoot() crypto.Hash {
	if len(so.RevisionTransactionSet) > 0 {
		return so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewFileMerkleRoot
	}
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].FileMerkleRoot
}

// payouts returns the set of valid payouts and missed payouts that represent
// the latest revision for the storage obligation.
func (so storageObligation) payouts() (valid []types.SiacoinOutput, missed []types.SiacoinOutput) {
	valid = make([]types.SiacoinOutput, 2)
	missed = make([]types.SiacoinOutput, 3)
	if len(so.RevisionTransactionSet) > 0 {
		copy(valid, so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewValidProofOutputs)
		copy(missed, so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewMissedProofOutputs)
		return
	}
	copy(valid, so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].ValidProofOutputs)
	copy(missed, so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].MissedProofOutputs)
	return
}

// revisionNumber returns the last revision number of the latest revision
// for the storage obligation
func (so storageObligation) revisionNumber() uint64 {
	if len(so.RevisionTransactionSet) > 0 {
		return so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewRevisionNumber
	}
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].RevisionNumber
}

// requiresProof is a helper to determine whether the storage obligation
// requires a proof.
func (so storageObligation) requiresProof() bool {
	// No need for a proof if the obligation doesn't have a revision.
	rev, err := so.recentRevision()
	if err != nil {
		return false
	}
	// No need for a proof if the valid outputs match the invalid ones. Right
	// now this is only the case if the contract was renewed.
	if reflect.DeepEqual(rev.NewValidProofOutputs, rev.NewMissedProofOutputs) {
		return false
	}
	// Every other case requires a proof.
	return true
}

// proofDeadline returns the height by which the storage proof must be
// submitted.
func (so storageObligation) proofDeadline() types.BlockHeight {
	if len(so.RevisionTransactionSet) > 0 {
		return so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0].NewWindowEnd
	}
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].FileContracts[0].WindowEnd
}

// transactionID returns the ID of the transaction containing the file
// contract.
func (so storageObligation) transactionID() types.TransactionID {
	return so.OriginTransactionSet[len(so.OriginTransactionSet)-1].ID()
}

// value returns the value of fulfilling the storage obligation to the host.
func (so storageObligation) value() types.Currency {
	return so.ContractCost.Add(so.PotentialDownloadRevenue).Add(so.PotentialStorageRevenue).Add(so.PotentialUploadRevenue).Add(so.RiskedCollateral)
}

// recentRevision returns the most recent file contract revision in this storage
// obligation.
func (so storageObligation) recentRevision() (types.FileContractRevision, error) {
	numRevisions := len(so.RevisionTransactionSet)
	if numRevisions == 0 {
		return types.FileContractRevision{}, errors.New("Could not get recent revision, there are no revision in the txn set")
	}
	revisionTxn := so.RevisionTransactionSet[numRevisions-1]
	return revisionTxn.FileContractRevisions[0], nil
}

// managedGetStorageObligation fetches a storage obligation from the database.
func (h *Host) managedGetStorageObligation(fcid types.FileContractID) (so storageObligation, err error) {
	h.mu.RLock()
	defer h.mu.RUnlock()

	err = h.db.View(func(tx *bolt.Tx) error {
		so, err = h.getStorageObligation(tx, fcid)
		return err
	})
	return
}

// queueActionItem adds an action item to the host at the input height so that
// the host knows to perform maintenance on the associated storage obligation
// when that height is reached.
func (h *Host) queueActionItem(height types.BlockHeight, id types.FileContractID) error {
	// Sanity check - action item should be at a higher height than the current
	// block height.
	if height <= h.blockHeight {
		h.log.Println("action item queued improperly")
		return errActionItemQueuedForInvalidBlockheight
	}
	return h.db.Update(func(tx *bolt.Tx) error {
		// Translate the height into a byte slice.
		heightBytes := make([]byte, 8)
		binary.BigEndian.PutUint64(heightBytes, uint64(height))

		// Get the list of action items already at this height and extend it.
		bai := tx.Bucket(bucketActionItems)
		existingItems := bai.Get(heightBytes)
		var extendedItems = make([]byte, len(existingItems), len(existingItems)+len(id[:]))
		copy(extendedItems, existingItems)
		extendedItems = append(extendedItems, id[:]...)
		return bai.Put(heightBytes, extendedItems)
	})
}

// managedAddStorageObligation adds a storage obligation to the host. Because
// this operation can return errors, the transactions should not be submitted to
// the blockchain until after this function has indicated success. All of the
// sectors that are present in the storage obligation should already be on disk,
// which means that addStorageObligation should be exclusively called when
// creating a new, empty file contract or when renewing an existing file
// contract.
func (h *Host) managedAddStorageObligation(so storageObligation) error {
	var soid types.FileContractID
	err := func() error {
		h.mu.Lock()
		defer h.mu.Unlock()
		// Sanity check - obligation should be under lock while being added.
		soid = so.id()
		_, exists := h.lockedStorageObligations[soid]
		if !exists {
			err := errors.New("addStorageObligation called with an obligation that is not locked")
			h.log.Print(err)
			return err
		}
		// Sanity check - There needs to be enough time left on the file contract
		// for the host to safely submit the file contract revision.
		if h.blockHeight+revisionSubmissionBuffer >= so.expiration() {
			h.log.Critical("submission window was not verified before trying to submit a storage obligation")
			return errNoBuffer
		}
		// Sanity check - the resubmission timeout needs to be smaller than storage
		// proof window.
		if so.expiration()+resubmissionTimeout >= so.proofDeadline() {
			h.log.Critical("host is misconfigured - the storage proof window needs to be long enough to resubmit if needed")
			return errors.New("fill me in")
		}

		// Add the storage obligation information to the database.
		err := h.db.Update(func(tx *bolt.Tx) error {
			// Sanity check - a storage obligation using the same file contract id
			// should not already exist. This situation can happen if the
			// transaction pool ejects a file contract and then a new one is
			// created. Though the file contract will have the same terms, some
			// other conditions might cause problems. The check for duplicate file
			// contract ids should happen during the negotiation phase, and not
			// during the 'addStorageObligation' phase.

			// If the storage obligation already has sectors, it means that the
			// file contract is being renewed, and that the sector should be
			// re-added with a new expiration height. If there is an error at any
			// point, all of the sectors should be removed.
			if len(so.SectorRoots) != 0 {
				err := h.AddSectorBatch(so.SectorRoots)
				if err != nil {
					return err
				}
			}

			// Store the new obligation too.
			return putStorageObligation(tx, so)
		})
		return err
	}()
	if err != nil {
		return err
	}

	// Check that the transaction is fully valid and submit it to the
	// transaction pool.
	err = h.tpool.AcceptTransactionSet(so.OriginTransactionSet)
	if err != nil {
		// If the transaction is rejected we should remove the contract from the database to not create a
		// new stale contract
		h.log.Println("Failed to add storage obligation, transaction set was not accepted:", err)
		err2 := h.deleteContractFromDB(soid)
		if err2 != nil {
			h.log.Errorf("Failed to remove storage obligation from database: %v", err)
		}
		return err
	}

	// Once the file contract is stored in the database we need to register in FinancialMetrics
	h.financialMetrics.registerStorageObligation(so.StorageObligation(), h.blockHeight)
	// Queue the action items.
	h.mu.Lock()
	err = h.managedQueueActionItemsForNewSO(so)
	h.mu.Unlock()
	if err != nil {
		h.log.Println("Error with transaction set, redacting obligation, id", so.id())
		return composeErrors(err, h.removeStorageObligation(so, obligationRejected))
	}
	return nil
}

// managedAddRenewedStorageObligation adds a new obligation to the host and
// saves the old obligation from which it was renewed atomically.
// If the saving of the old obligation failed or there was an error during creation of new, the
// old storageObligation will be restored to its original state before the renew attempt and the
// new storageObligation will be removed from the database.
func (h *Host) managedAddRenewedStorageObligation(oldSO, newSO storageObligation) error {
	// Sanity check - obligations should be under lock while being modified.
	h.mu.RLock()
	_, exists1 := h.lockedStorageObligations[oldSO.id()]
	_, exists2 := h.lockedStorageObligations[newSO.id()]
	h.mu.RUnlock()
	if !exists1 || !exists2 {
		err := errors.New("addRenewedStorageObligation called with an obligation that is not locked")
		h.log.Print(err)
		return err
	}

	// The supplied oldSO is modified but not saved so we can revert to saved version
	// Thus we need to get a backup of the saved storageObligation in case the renewal fails
	var oldSOBefore storageObligation
	// revertOldSO reverts the storage obligation back to the previous version.
	// assumes host is locked already
	revertOldSO := func() error {
		err := h.db.Update(func(tx *bolt.Tx) error {
			return putStorageObligation(tx, oldSOBefore)
		})
		if err != nil {
			return fmt.Errorf("error reverting to previous version of storage obligation: %w", err)
		}
		// Registering the old version replaces the new in financialMetrics
		h.financialMetrics.registerStorageObligation(oldSOBefore.StorageObligation(), h.blockHeight)
		return nil
	}

	var err error
	// Update the database to contain the new storage obligation.
	h.mu.Lock()
	err = h.db.Update(func(tx *bolt.Tx) error {
		// Get the old storage obligation to have a backup to roll back to in case of an error
		oldSOBefore, err = h.getStorageObligation(tx, oldSO.id())
		if err != nil {
			return err
		}
		// Store the obligation to replace the old entry.
		err = putStorageObligation(tx, oldSO)
		if err != nil {
			return err
		}
		h.financialMetrics.registerStorageObligation(oldSO.StorageObligation(), h.blockHeight)

		// Store the new obligation too.
		err = putStorageObligation(tx, newSO)
		if err != nil {
			return err
		}
		h.financialMetrics.registerStorageObligation(newSO.StorageObligation(), h.blockHeight)
		return nil
	})
	if err != nil {
		// Forget the newSO and roll back to previous saved version of oldSO
		h.financialMetrics.unregisterStorageObligation(newSO.id())
		err2 := revertOldSO()
		if err2 != nil {
			h.log.Errorf("Failed to revert to previous version of storage obligation: %v", err2)
		}
		h.mu.Unlock()
		return fmt.Errorf("contract database error in managedAddRenewedStorageObligation: failed to update oldSO: %w", err)
	}

	// Queue the action items.
	err = h.managedQueueActionItemsForNewSO(newSO)
	if err != nil {
		// If queueing the action items failed, but broadcasting the txn worked,
		// we can only remove the newSO. The txn will still be mined.
		// TODO: Since the transaction was accepted and the contract is valid
		// but there is an error scheduling the action items we need to retry the scheduling
		// instead of removing the storageobligation and letting the coins burn
		h.log.Errorf("Removing new contract %s as Rejected: %v", newSO.id(), err)
		err2 := revertOldSO()
		if err2 != nil {
			h.log.Errorf("Failed to revert to previous version of storage obligation: %v", err2)
		}
		err3 := h.deleteContractFromDB(newSO.id())
		if err3 != nil {
			h.log.Errorf("Failed to remove new contract %s from database: %v", newSO.id(), err3)
		}
		h.mu.Unlock()
		return composeErrors(err, err2, err3)
	}
	h.mu.Unlock()

	// Check that the transaction of new contract is fully valid and submit it to the
	// transaction pool.
	err = h.tpool.AcceptTransactionSet(newSO.OriginTransactionSet)
	if err != nil {
		// If we can't submit the txn, we need to revert to the previous oldSO
		// and remove the newSO from the db.
		h.mu.Lock()
		err2 := revertOldSO()
		err3 := h.deleteContractFromDB(newSO.id())
		h.mu.Unlock()
		return fmt.Errorf("Failed to add storage obligation %v, transaction set was not accepted: %v", newSO.id(), errors.Compose(err, err2, err3))
	}
	return nil
}

// managedQueueActionItemsForNewSO queues the action items for a newly created
// storage obligation.
func (h *Host) managedQueueActionItemsForNewSO(so storageObligation) error {
	soid := so.id()

	// The file contract was already submitted to the blockchain, need to check
	// after the resubmission timeout that it was submitted successfully.
	err1 := h.queueActionItem(h.blockHeight+resubmissionTimeout, soid)
	err2 := h.queueActionItem(h.blockHeight+resubmissionTimeout*2, soid) // Paranoia
	// Queue an action item to submit the file contract revision - if there is
	// never a file contract revision, the handling of this action item will be
	// a no-op.
	err3 := h.queueActionItem(so.expiration()-revisionSubmissionBuffer-1, soid)
	err4 := h.queueActionItem(so.expiration()-revisionSubmissionBuffer-1+resubmissionTimeout, soid) // Paranoia
	// The storage proof should be submitted
	err5 := h.queueActionItem(so.expiration()+resubmissionTimeout, soid)
	err6 := h.queueActionItem(so.expiration()+resubmissionTimeout*2, soid) // Paranoia
	err := composeErrors(err1, err2, err3, err4, err5, err6)
	if err != nil {
		// It is either wrong blockheight (in the past) or there is a problem writing to the db
		// Return an error but do not delete the contract here
		return fmt.Errorf("error scheduling action items for new contract id %s: %w", soid, err)
	}
	h.log.Debugf("Scheduled actions for new contract id %s at blockheights: %v", soid, []types.BlockHeight{
		h.blockHeight + resubmissionTimeout,
		h.blockHeight + resubmissionTimeout*2,
		so.expiration() - revisionSubmissionBuffer,
		so.expiration() - revisionSubmissionBuffer + resubmissionTimeout,
		so.expiration() + resubmissionTimeout,
		so.expiration() + resubmissionTimeout*2,
	})
	return nil
}

// managedModifyStorageObligation will take an updated storage obligation along
// with a list of sector changes and update the database to account for all of
// it. The sector modifications are only used to update the sector database,
// they will not be used to modify the storage obligation (most importantly,
// this means that sectorRoots needs to be updated by the calling function).
// Virtual sectors will be removed the number of times that they are listed, to
// remove multiple instances of the same virtual sector, the virtual sector
// will need to appear in 'sectorsRemoved' multiple times. Same with
// 'sectorsGained'.
func (h *Host) managedModifyStorageObligation(so storageObligation, sectorsRemoved []crypto.Hash, sectorsGained map[crypto.Hash][]byte) error {
	// Sanity check - all of the sector data should be modules.SectorSize
	for _, data := range sectorsGained {
		if uint64(len(data)) != modules.SectorSize {
			h.log.Critical("modifying a revision with garbage sector sizes", len(data))
			return errInsaneStorageObligationRevision
		}
	}

	// TODO: remove this once the host was optimized for disk i/o
	// If the contract is too large we delay for a bit to prevent rapid updates
	// from clogging up disk i/o.
	if so.fileSize() >= largeContractSize {
		time.Sleep(largeContractUpdateDelay)
	}

	// Grab a couple of host state facts for sanity checks.
	soid := so.id()
	h.mu.Lock()
	hostHeight := h.blockHeight
	_, exists := h.lockedStorageObligations[soid]
	h.mu.Unlock()
	// Sanity check - obligation should be under lock while being modified.
	if !exists {
		err := errors.New("modifyStorageObligation called with an obligation that is not locked")
		h.log.Print(err)
		return err
	}
	// Sanity check - there needs to be enough time to submit the file contract
	// revision to the blockchain.
	if so.expiration()-revisionSubmissionBuffer <= hostHeight {
		return errNoBuffer
	}

	// Note, for safe error handling, the operation order should be: add
	// sectors, update database, remove sectors. If the adding or update fails,
	// the added sectors should be removed and the storage obligation shoud be
	// considered invalid. If the removing fails, this is okay, it's ignored
	// and left to consistency checks and user actions to fix (will reduce host
	// capacity, but will not inhibit the host's ability to submit storage
	// proofs)
	var added []crypto.Hash
	var err error
	for sectorRoot, data := range sectorsGained {
		err = h.AddSector(sectorRoot, data)
		if err != nil {
			break
		}
		added = append(added, sectorRoot)
	}
	if err != nil {
		// Because there was an error, all of the sectors that got added need
		// to be reverted.
		for _, sectorRoot := range added {
			// Error is not checked because there's nothing useful that can be
			// done about an error.
			_ = h.RemoveSector(sectorRoot)
		}
		return err
	}

	// Lock the host while we update storage obligation and financial metrics.
	h.mu.Lock()
	defer h.mu.Unlock()

	defer func() {
		//Financial metrics will sort it into corresponding bucket based on the contents
		h.financialMetrics.registerStorageObligation(so.StorageObligation(), h.blockHeight)
		// The locked storage collateral was altered, we potentially want to
		// unregister the insufficient collateral budget alert
		h.tryUnregisterInsufficientCollateralBudgetAlert()
	}()

	// Update the database to contain the new storage obligation.
	err = h.db.Update(func(tx *bolt.Tx) error {
		// Store the new storage obligation to replace the old one.
		return putStorageObligation(tx, so)
	})
	if err != nil {
		// Because there was an error, all of the sectors that got added need
		// to be reverted.
		for sectorRoot := range sectorsGained {
			// Error is not checked because there's nothing useful that can be
			// done about an error.
			_ = h.RemoveSector(sectorRoot)
		}
		return err
	}
	// Call removeSector for all of the sectors that have been removed.
	for k := range sectorsRemoved {
		// Error is not checkeed because there's nothing useful that can be
		// done about an error. Failing to remove a sector is not a terrible
		// place to be, especially if the host can run consistency checks.
		_ = h.RemoveSector(sectorsRemoved[k])
	}
	return nil
}

// removeStorageObligation will remove a storage obligation from the host,
// either due to failure or success.
func (h *Host) removeStorageObligation(so storageObligation, sos storageObligationStatus) error {
	if sos == obligationUnresolved {
		h.log.Critical("storage obligation 'unresolved' during call to removeStorageObligation, id", so.id())
	}
	obligationStatus := h.verifyStorageObligationStatus(so, h.blockHeight)
	h.log.Debugf("removeStorageObligation(%v,%s), verifystatus=%s", so.id(), sos, obligationStatus)
	defer func() {
		//Update financial metrics buckets after processing
		h.financialMetrics.registerStorageObligation(so.StorageObligation(), h.blockHeight)
		h.tryUnregisterInsufficientCollateralBudgetAlert()
	}()
	var wg sync.WaitGroup
	wg.Add(1)
	go func(roots []crypto.Hash) {
		// Error is not checked, we want to call remove on every sector even if
		// there are problems - disk health information will be updated.
		err := h.RemoveSectorBatch(roots)
		if err != nil {
			h.log.Debugf("removeStorageObligation called RemoveSectorBatch failed with error: %v", err)
		} else {
			h.log.Debugf("removeStorageObligation successful RemoveSectorBatch of %d sector roots", len(roots))
		}
		wg.Done()
	}(so.SectorRoots)

	wg.Wait()
	// Update the storage obligation to be finalized but still in-database. The
	// obligation status is updated so that the user can see how the obligation
	// ended up, and the sector roots are removed because they are large
	// objects with little purpose once storage proofs are no longer needed.
	so.ObligationStatus = sos
	so.SectorRoots = nil
	return h.db.Update(func(tx *bolt.Tx) error {
		return putStorageObligation(tx, so)
	})
}

// resetFinancialMetrics completely resets the host's financial metrics using
// the storage obligations that are currently present in the hostdb. This
// function is triggered after pruning stale obligations and is a way to ensure
// the financial metrics correctly reflect the host's financial statistics.
func (h *Host) resetFinancialMetrics() error {
	// Initialize new values for the host financial metrics.
	fm := &hostFinancialMetrics{}
	err := h.db.View(func(tx *bolt.Tx) error {
		c := tx.Bucket(bucketStorageObligations).Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var so storageObligation
			if err := json.Unmarshal(v, &so); err != nil {
				return fmt.Errorf("unable to unmarshal storage obligation: %w", err)
			}
			fm.registerStorageObligation(so.StorageObligation(), h.blockHeight)
		}
		return nil
	})
	if err != nil {
		h.log.Println(fmt.Errorf("unable to reset host financial metrics: %w", err))
		return err
	}
	h.financialMetrics = fm
	return nil
}

// threadedHandleActionItem will look at a storage obligation and determine
// which action is necessary for the storage obligation to succeed.
func (h *Host) threadedHandleActionItem(soid types.FileContractID) {
	err := h.tg.Add()
	if err != nil {
		return
	}
	defer h.tg.Done()

	// Lock the storage obligation in question.
	h.managedLockStorageObligation(soid)
	defer func() {
		h.managedUnlockStorageObligation(soid)
	}()

	// Fetch the storage obligation associated with the storage obligation id.
	var so storageObligation
	h.mu.RLock()
	blockHeight := h.blockHeight
	err = h.db.View(func(tx *bolt.Tx) error {
		so, err = h.getStorageObligation(tx, soid)
		return err
	})
	h.mu.RUnlock()
	if err != nil {
		h.log.Errorf("Could not get storage obligation %v: %v", soid.String(), err)
		return
	}

	//Whatever the state after processing at the end is update financial metrics
	defer func() {
		h.financialMetrics.registerStorageObligation(so.StorageObligation(), blockHeight)
	}()

	// Check whether the storage obligation has already been completed.
	if so.ObligationStatus != obligationUnresolved {
		// Storage obligation has already been completed, skip action item.
		return
	}

	// Check whether the file contract has been seen. If not, resubmit and
	// queue another action item. Check for death. (signature should have a
	// kill height)
	if !so.OriginConfirmed {
		h.log.Debugf("Contract %s needs to be confirmed, blockheight = %d ", soid, blockHeight)
		// Submit the transaction set again, try to get the transaction
		// confirmed.
		err := h.tpool.AcceptTransactionSet(so.OriginTransactionSet)
		if err != nil {
			h.log.Errorf("Could not get origin transaction set accepted for contract %v: %v", so.id(), err)
			// Check if the transaction is invalid with the current consensus set.
			// If so, the transaction is highly unlikely to ever be confirmed, and
			// the storage obligation should be removed. This check should come
			// after logging the errror so that the function can quit.
			//
			// TODO: If the host or tpool is behind consensus, might be difficult
			// to have certainty about the issue. If some but not all of the
			// parents are confirmed, might be some difficulty.

			//If there is a chance for the transaction to succeed, leave the storage obligation as is
			// and add to action items for check later
			//If there are less than 3 blocks until expiration remaining means no chance to make it
			if modules.IsConsensusConflict(err) && blockHeight > so.expiration()-resubmissionTimeout {
				h.log.Println("Consensus conflict on the origin transaction set, id", so.id())
				h.log.Debugf("Removing storage obligation due to inability to confirm transaction")
				h.mu.Lock()
				//Remove sectors not referenced in other contracts
				references, err := h.getReferencedSectorIDs()
				if err != nil {
					h.log.Debugf("error getting referenced sectors: %v", err)
					h.mu.Unlock()
					return
				}
				err = h.removeReferencedSectorsFromContract(&so, references)
				if err != nil {
					h.log.Debugf("error removing referenced sectors from contract: %v", err)
					h.mu.Unlock()
					return
				}
				err = h.removeStorageObligation(so, obligationRejected)
				h.mu.Unlock()
				if err != nil {
					h.log.Println("Error removing storage obligation:", err)
				}
				return
			}
		}

		// Queue another action item to check the status of the transaction.
		h.mu.Lock()
		err = h.queueActionItem(blockHeight+resubmissionTimeout, so.id())
		h.mu.Unlock()
		if err != nil {
			h.log.Println("Error queuing action item:", err)
		}
	}

	// Revision exists but is not confirmed in time
	if !so.RevisionConfirmed && len(so.RevisionTransactionSet) > 0 && blockHeight >= so.expiration()-revisionSubmissionBuffer {
		// Sanity check - there should be a file contract revision.
		rtsLen := len(so.RevisionTransactionSet)
		if rtsLen < 1 || len(so.RevisionTransactionSet[rtsLen-1].FileContractRevisions) != 1 {
			h.log.Critical("transaction revision marked as unconfirmed, yet there is no transaction revision")
			return
		}

		// Check if the last revision has failed to submit before expiration.
		if blockHeight > so.expiration() {
			// Re-check the RevisionConfirmed status
			revTXId := so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].ID()
			confirmed, err := h.tpool.TransactionConfirmed(revTXId)
			if err != nil {
				h.log.Errorf("Error checking revision transaction: %v", err)
				return
			}
			if !confirmed {
				h.log.Debugf("Full time has elapsed, but the revision transaction %v could not be submitted to consensus, contract id: %v", revTXId, so.id())
				obligationStatus := h.verifyStorageObligationStatus(so, h.BlockHeight())
				h.log.Debugf("Status: OriginConfirmed: %t, RevisionConfirmed: %t, ProofConstructed: %t, ProofConfirmed: %t, verify status: %v",
					so.OriginConfirmed, so.RevisionConfirmed, so.ProofConstructed, so.ProofConfirmed, obligationStatus)
				if obligationStatus == obligationUnresolved {
					h.log.Errorf("storage obligation verification tells 'unresolved' when call to removeStorageObligation as Rejected, id: %v", so.id())
				}
				h.mu.Lock()
				//Remove sectors not referenced in other contracts
				references, err := h.getReferencedSectorIDs()
				if err != nil {
					h.log.Debugf("error getting referenced sectors: %v", err)
					h.mu.Unlock()
					return
				}
				err = h.removeReferencedSectorsFromContract(&so, references)
				if err != nil {
					h.log.Debugf("error removing referenced sectors from contract: %v", err)
					h.mu.Unlock()
					return
				}
				h.removeStorageObligation(so, obligationRejected)
				h.mu.Unlock()
				return
			}
		}

		// Queue another action item to check the status of the transaction.
		h.mu.Lock()
		err := h.queueActionItem(blockHeight+resubmissionTimeout, so.id())
		h.mu.Unlock()
		if err != nil {
			h.log.Println("Error queuing action item:", err)
		}

		// Retry to submit the revision transaction to the blockchain
		revisionTxnIndex := len(so.RevisionTransactionSet) - 1
		revisionParents := so.RevisionTransactionSet[:revisionTxnIndex]
		revisionTxn := so.RevisionTransactionSet[revisionTxnIndex]
		builder, err := h.wallet.RegisterTransaction(revisionTxn, revisionParents)
		if err != nil {
			h.log.Println("Error registering transaction:", err)
			return
		}
		_, feeRecommendation := h.tpool.FeeEstimation()
		if so.value().Div64(2).Cmp(feeRecommendation) < 0 {
			// There's no sense submitting the revision if the fee is more than
			// half of the anticipated revenue - fee market went up
			// unexpectedly, and the money that the renter paid to cover the
			// fees is no longer enough.
			h.log.Debugf("Dropping revision transaction due to low potential earnings %v compared to transactionfee of %v", so.value(), feeRecommendation)
			builder.Drop()
			return
		}
		txnSize := uint64(len(encoding.MarshalAll(so.RevisionTransactionSet)) + txnFeeSizeBuffer)
		requiredFee := feeRecommendation.Mul64(txnSize)
		uc, err := h.wallet.UnlockConditions(h.unlockHash)
		if err != nil {
			builder.Drop()
			h.log.Printf("Can not get host unlockhash, error: %v ", err)
			return
		}
		err = builder.FundSiacoinsFixedAddress(requiredFee, uc, uc)
		if err != nil {
			h.log.Println("Error funding transaction fees", err)
			builder.Drop()
		}
		builder.AddMinerFee(requiredFee)
		feeAddedRevisionTransactionSet, err := builder.Sign(true)
		if err != nil {
			h.log.Println("Error signing transaction", err)
			builder.Drop()
		}
		h.log.Debugf("Submitting contract %s revision transaction %s to transactionpool", soid, revisionTxn.ID())
		err = h.tpool.AcceptTransactionSet(feeAddedRevisionTransactionSet)
		if err != nil {
			h.log.Println("Error submitting transaction to transaction pool", err)
			builder.Drop()
		}
		so.RevisionConstructed = true
		h.log.Debugf("Contract %s revision %d transaction %s accepted by transactionpool", soid, so.revisionNumber(), feeAddedRevisionTransactionSet[len(feeAddedRevisionTransactionSet)-1].ID())
		so.TransactionFeesAdded = so.TransactionFeesAdded.Add(requiredFee)
	}

	// Check whether a storage proof is ready to be provided, and whether it
	// has been accepted. Check for death.
	if !so.ProofConfirmed && blockHeight >= so.expiration()+resubmissionTimeout {
		h.log.Debugln("Host is attempting a storage proof for", so.id())

		// If the obligation doesn't require a proof, we can remove the
		// obligation and avoid submitting a storage proof. In that case the
		// host payout for the contract includes the contract cost and locked
		// collateral.
		if !so.requiresProof() {
			h.log.Debugf("Storage proof for contract %s not required", so.id())
			h.mu.Lock()
			err := h.removeStorageObligation(so, obligationSucceeded)
			h.mu.Unlock()
			if err != nil {
				h.log.Println("Error removing storage obligationwith no proof required:", err)
			}
			return
		}
		// If the window has closed, the host has failed and the obligation can
		// be removed.
		if so.proofDeadline() < blockHeight {
			h.log.Debugln("storage proof not confirmed by deadline, id", so.id())
			h.mu.Lock()
			err := h.removeStorageObligation(so, obligationFailed)
			h.mu.Unlock()
			if err != nil {
				h.log.Println("Error removing storage obligation after failed proof:", err)
			}
			return
		}

		// Get the index of the segment for which to build the proof.
		segmentIndex, err := h.cs.StorageProofSegment(so.id())
		if err != nil {
			h.log.Debugln("Host got an error when fetching a storage proof segment:", err)
			return
		}

		// Build StorageProof.
		sp, err := h.managedBuildStorageProof(so, segmentIndex)
		if err != nil {
			h.log.Debugln("Host encountered an error when building the storage proof", err)
			return
		}
		h.log.Debugf("Storage proof for contract %v built", so.id())

		// Create and build the transaction with the storage proof.
		builder, err := h.wallet.StartTransaction()
		if err != nil {
			h.log.Println("Failed to start transaction:", err)
			return
		}
		_, feeRecommendation := h.tpool.FeeEstimation()
		txnSize := uint64(len(encoding.Marshal(sp)) + txnFeeSizeBuffer)
		requiredFee := feeRecommendation.Mul64(txnSize)
		if so.value().Cmp(requiredFee) < 0 {
			// There's no sense submitting the storage proof if the fee is more
			// than the anticipated revenue.
			h.log.Debugln("Host not submitting storage proof due to a value that does not sufficiently exceed the fee cost")
			builder.Drop()
			return
		}
		uc, err := h.wallet.UnlockConditions(h.unlockHash)
		if err != nil {
			builder.Drop()
			h.log.Printf("Can not get host unlockhash, error: %v ", err)
			return
		}
		err = builder.FundSiacoinsFixedAddress(requiredFee, uc, uc)
		if err != nil {
			h.log.Println("Host error when funding a storage proof transaction fee:", err)
			builder.Drop()
			return
		}
		builder.AddMinerFee(requiredFee)
		builder.AddStorageProof(sp)
		storageProofSet, err := builder.Sign(true)
		if err != nil {
			h.log.Println("Host error when signing the storage proof transaction:", err)
			builder.Drop()
			return
		}
		err = h.tpool.AcceptTransactionSet(storageProofSet)
		if err != nil {
			h.log.Println("Host unable to submit storage proof transaction to transaction pool:", err)
			builder.Drop()
			return
		}
		so.TransactionFeesAdded = so.TransactionFeesAdded.Add(requiredFee)
		h.log.Debugf("Storage proof for contract %v accepted by transactionpool", so.id())
		so.ProofConstructed = true

		// Queue another action item to check whether the storage proof
		// got confirmed.
		h.mu.Lock()
		err = h.queueActionItem(so.proofDeadline(), so.id())
		h.mu.Unlock()
		if err != nil {
			h.log.Println("Error queuing action item:", err)
		}
	}

	// Save the storage obligation to account for any fee changes.
	err = h.db.Update(func(tx *bolt.Tx) error {
		soBytes, err := json.Marshal(so)
		if err != nil {
			return err
		}
		return tx.Bucket(bucketStorageObligations).Put(soid[:], soBytes)
	})
	if err != nil {
		h.log.Println("Error updating the storage obligations", err)
	}

	// Check if all items have succeeded with the required confirmations. Report
	// success, delete the obligation.
	if so.ProofConfirmed && blockHeight >= so.proofDeadline() {
		h.log.Println("file contract complete, id", so.id())
		h.mu.Lock()
		err = h.removeStorageObligation(so, obligationSucceeded)
		h.mu.Unlock()
		if err != nil {
			h.log.Println("Error removing storage obligation after successful proof:", err)
		}
	}
}

// managedBuildStorageProof builds a storage proof for a given storageObligation
// for the host to submit.
func (h *Host) managedBuildStorageProof(so storageObligation, segmentIndex uint64) (types.StorageProof, error) {
	// Handle empty contract edge case.
	if len(so.SectorRoots) == 0 {
		return types.StorageProof{
			ParentID: so.id(),
		}, nil
	}
	sectorIndex := segmentIndex / (modules.SectorSize / crypto.SegmentSize)
	// Pull the corresponding sector into memory.
	sectorRoot := so.SectorRoots[sectorIndex]
	sectorBytes, err := h.ReadSector(sectorRoot)
	if err != nil {
		return types.StorageProof{}, errors.AddContext(err, "managedBuildStorageProof: failed to read sector")
	}

	// Build the storage proof for just the sector.
	sectorSegment := segmentIndex % (modules.SectorSize / crypto.SegmentSize)
	base, cachedHashSet := crypto.MerkleProof(sectorBytes, sectorSegment)

	// Using the sector, build a cached root.
	log2SectorSize := uint64(0)
	for 1<<log2SectorSize < (modules.SectorSize / crypto.SegmentSize) {
		log2SectorSize++
	}
	ct := crypto.NewCachedTree(log2SectorSize)
	ct.SetIndex(segmentIndex)
	for _, root := range so.SectorRoots {
		ct.PushSubTree(0, root)
	}
	hashSet := ct.Prove(base, cachedHashSet)
	sp := types.StorageProof{
		ParentID: so.id(),
		HashSet:  hashSet,
	}
	copy(sp.Segment[:], base)
	return sp, nil
}

// StorageObligation returns the storage obligation matching the id or
// an error if it does not exist.
func (h *Host) StorageObligation(obligationID types.FileContractID) (modules.StorageObligation, error) {
	so, err := h.managedGetStorageObligation(obligationID)
	if err != nil {
		return modules.StorageObligation{}, errors.AddContext(err, "failed to fetch storage obligation")
	}
	return so.StorageObligation(), nil
}

// StorageObligations fetches the set of storage obligations in the host and
// returns metadata on them.
func (h *Host) StorageObligations() (sos []modules.StorageObligation) {
	h.mu.RLock()
	defer h.mu.RUnlock()

	err := h.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketStorageObligations)
		err := b.ForEach(func(idBytes, soBytes []byte) error {
			var so storageObligation
			err := json.Unmarshal(soBytes, &so)
			if err != nil {
				return build.ExtendErr("unable to unmarshal storage obligation:", err)
			}

			sos = append(sos, so.StorageObligation())
			return nil
		})
		if err != nil {
			return build.ExtendErr("ForEach failed to get next storage obligation:", err)
		}
		return nil
	})
	if err != nil {
		h.log.Println(build.ExtendErr("database failed to provide storage obligations:", err))
	}

	return sos
}

// MoveTokenSectorsToStorageObligation moves sectors from temporary storage to storage obligation object and returns host signature.
func (h *Host) MoveTokenSectorsToStorageObligation(fcID types.FileContractID, renterRevision types.FileContractRevision, sectorsWithTokens []types.SectorWithToken, renterSig []byte) ([]byte, error) {
	err := h.managedTryLockStorageObligation(fcID, obligationLockTimeout)
	if err != nil {
		return nil, err
	}
	defer h.managedUnlockStorageObligation(fcID)
	var so storageObligation
	h.mu.RLock()
	tokenStor := h.tokenStor
	_, maxFee := h.tpool.FeeEstimation()
	settings := h.externalSettings(maxFee)
	blockHeight := h.blockHeight
	secretKey := h.secretKey
	err = h.db.View(func(tx *bolt.Tx) error {
		var err error
		so, err = h.getStorageObligation(tx, fcID)
		return err
	})
	h.mu.RUnlock()
	if err != nil {
		return nil, extendErr("could not get storage obligation "+fcID.String()+": ", err)
	}
	contractstatus := h.verifyStorageObligationStatus(so, blockHeight)
	if contractstatus != obligationUnresolved {
		return nil, fmt.Errorf("can not attach sectors to contract %s, wrong status: %s", fcID, contractstatus)
	}
	if tokenStor == nil {
		return nil, errors.New("host module not loaded")
	}
	currentRevision := so.RevisionTransactionSet[len(so.RevisionTransactionSet)-1].FileContractRevisions[0]

	newRoots := make([]crypto.Hash, 0, len(sectorsWithTokens))
	for _, s := range sectorsWithTokens {
		newRoots = append(newRoots, s.SectorID)
	}

	// update finances.
	bytesAdded := modules.SectorSize * uint64(len(newRoots))
	blocksRemaining := so.proofDeadline() - blockHeight
	blockBytesCurrency := types.NewCurrency64(uint64(blocksRemaining)).Mul64(bytesAdded)
	var storageRevenue, newCollateral, bandwidthRevenue types.Currency
	storageRevenue = settings.StoragePrice.Mul(blockBytesCurrency)
	newCollateral = newCollateral.Add(settings.Collateral.Mul(blockBytesCurrency))
	bandwidthRevenue = bandwidthRevenue.Add(settings.UploadBandwidthPrice.Mul64(bytesAdded))

	newRevision := currentRevision
	newRevision.NewRevisionNumber = renterRevision.NewRevisionNumber
	newRevision.NewFileSize += modules.SectorSize * uint64(len(newRoots))
	newRevision.NewFileMerkleRoot = renterRevision.NewFileMerkleRoot

	newRevision.NewValidProofOutputs = make([]types.SiacoinOutput, len(currentRevision.NewValidProofOutputs))
	for i := range newRevision.NewValidProofOutputs {
		newRevision.NewValidProofOutputs[i] = types.SiacoinOutput{
			Value:      renterRevision.NewValidProofOutputs[i].Value,
			UnlockHash: currentRevision.NewValidProofOutputs[i].UnlockHash,
		}
	}
	newRevision.NewMissedProofOutputs = make([]types.SiacoinOutput, len(currentRevision.NewMissedProofOutputs))
	for i := range newRevision.NewMissedProofOutputs {
		newRevision.NewMissedProofOutputs[i] = types.SiacoinOutput{
			Value:      renterRevision.NewMissedProofOutputs[i].Value,
			UnlockHash: currentRevision.NewMissedProofOutputs[i].UnlockHash,
		}
	}

	// Verify the new revision.
	newTotalRoots := append(so.SectorRoots, newRoots...)
	newRevenue := settings.BaseRPCPrice.Add(storageRevenue).Add(bandwidthRevenue)
	so.SectorRoots, newTotalRoots = newTotalRoots, so.SectorRoots // verifyRevision assumes new roots
	err = verifyRevision(so, newRevision, blockHeight, newRevenue, newCollateral)
	so.SectorRoots, newTotalRoots = newTotalRoots, so.SectorRoots
	if err != nil {
		return nil, err
	}

	// Sanity check: compare new revision formed by host and new revision from renter.
	bufNewRev := new(bytes.Buffer)
	if err = newRevision.MarshalSia(bufNewRev); err != nil {
		return nil, err
	}
	bufReqRev := new(bytes.Buffer)
	if err = renterRevision.MarshalSia(bufReqRev); err != nil {
		return nil, err
	}
	if !bytes.Equal(bufNewRev.Bytes(), bufReqRev.Bytes()) {
		return nil, errors.New("incorrect revision")
	}

	renterSignature := types.TransactionSignature{
		ParentID:       crypto.Hash(newRevision.ParentID),
		CoveredFields:  types.CoveredFields{FileContractRevisions: []uint64{0}},
		PublicKeyIndex: 0,
		Signature:      renterSig,
	}
	txn, err := createRevisionSignature(newRevision, renterSignature, secretKey, blockHeight)
	if err != nil {
		return nil, err
	}

	// Now that all checks have passed, we can remove sector ids from temporary storage.
	// We can't do it after `managedModifyStorageObligation`(), because there is no
	// common lock for temporary storage and host's database, which means
	// that someone (checkExpiration goroutine or RemoveSectors method) may
	// remove sectors after we attach them to a storage obligation here,
	// which would result in losing contract's sectors. We can't do it before this place,
	// because renter's signature and revision is checked above.
	// Therefore, the only place to call tokenStorage.AttachSectors is right here.
	tokensSectors := make(map[types.TokenID][]crypto.Hash)
	for _, s := range sectorsWithTokens {
		tokenSectors := tokensSectors[s.Token]
		tokenSectors = append(tokenSectors, s.SectorID)
		tokensSectors[s.Token] = tokenSectors
	}
	if err := tokenStor.AttachSectors(tokensSectors, time.Now()); err != nil {
		return nil, fmt.Errorf("tokenStor.AttachSectors: %w", err)
	}

	so.SectorRoots = newTotalRoots
	so.PotentialStorageRevenue = so.PotentialStorageRevenue.Add(storageRevenue)
	so.RiskedCollateral = so.RiskedCollateral.Add(newCollateral)
	so.PotentialUploadRevenue = so.PotentialUploadRevenue.Add(bandwidthRevenue)
	so.RevisionTransactionSet = []types.Transaction{txn}
	err = h.managedModifyStorageObligation(so, nil, nil)
	if err != nil {
		return nil, err
	}
	return txn.TransactionSignatures[1].Signature, nil
}

// AuditStorageObligations locks the host, fetches the set of storage
// obligations in the host and checks if any old storage obligations are not
// marked correctly and removed if expired along with stale contract removal
func (h *Host) AuditStorageObligations() {
	//Host needs to be locked for audit
	h.mu.Lock()
	defer h.mu.Unlock()
	err := h.auditStorageObligations()
	if err != nil {
		h.log.Println("ERROR: AuditStorageObligations() failed:", err)
	}
	//Recalculate the financial metrics of the host.
	err = h.resetFinancialMetrics()
	if err != nil {
		h.log.Println(errors.AddContext(err, "Autit: unable to reset host financial metrics"))
	}
}

// auditStorageObligations fetches the set of storage obligations in the host and
// checks if any old storage obligations are not marked correctly and removed if expired.
func (h *Host) auditStorageObligations() error {
	var storageObligations sync.Map
	var badDBEntries sync.Map
	//Read storage obligations from DB
	err := h.db.View(func(tx *bolt.Tx) error {
		cursor := tx.Bucket(bucketStorageObligations).Cursor()
		for k, v := cursor.First(); k != nil; k, v = cursor.Next() {
			//collect unreadable entries in contract database
			var fcid types.FileContractID
			copy(fcid[:], k)
			var so storageObligation
			dataerr := json.Unmarshal(v, &so)
			if dataerr != nil {
				h.log.Printf("Marking corrupt storageobligation key: %v with error: %v", fcid, dataerr)
				badDBEntries.Store(fcid, v)
				continue
			}

			//collect corrupt entries returning contract id different from the key stored
			fcidReturned := so.id()
			h.log.Debugf("Comparing key %v to stored id: %v", fcid, fcidReturned)
			if fcid != fcidReturned {
				h.log.Printf("Marking corrupt storageobligation key: %v returns id: %v", fcid, fcidReturned)
				badDBEntries.Store(fcid, v)
				continue
			}
			storageObligations.Store(so.id(), so)
		}
		return nil
	})
	if err != nil {
		h.log.Errorf("Auditing contracts, reading contracts from database failed: %v", err)
		return err
	}

	// Delete corrupt entries from DB if there are any
	badDBEntries.Range(func(key, value interface{}) bool {
		fcid := key.(types.FileContractID)
		err = h.deleteContractFromDB(fcid)
		if err != nil {
			h.log.Errorf("Deleting corrupt storage obligation entry from database failed: %v", err)
			return false
		}
		return true
	})

	//Collect referenced sector IDs
	references, err := h.getReferencedSectorIDs()
	if err != nil {
		return fmt.Errorf("error collecting referenced sector IDs: %w", err)
	}
	bh := h.blockHeight

	// Catch the case iteration aborts with an error
	var auditActionError error
	storageObligations.Range(func(key, value interface{}) bool {
		defer storageObligations.Delete(key)
		id := key.(types.FileContractID)
		so := value.(storageObligation)
		h.log.Debugf("auditing storage obligation %v", id)
		status := h.verifyStorageObligationStatus(so, bh)
		h.log.Debugf("Status of %v initially determined to be %s", so.id(), status)

		//Quick skip over resolved contracts
		if status == obligationFailed ||
			status == obligationSucceeded ||
			status == obligationRenewed {
			return true
		}

		// Re-check the contract confirmation status
		confirmed, err := h.tpool.TransactionConfirmed(so.transactionID())
		if err != nil {
			//Error indicates transactionpool is closing, return
			auditActionError = fmt.Errorf("auditStorageObligations: check transaction confirmed failed with error %w", err)
			return false
		}

		//Quick check and easy fix of confirmation status of non expired contracts
		if confirmed && so.expiration() < bh {
			if so.OriginConfirmed == confirmed {
				// all perfect, contract confirmed and not expired skip further checking
				return true
			}
			// Fix status inconsistency
			// Contract is actually confirmed on blockchain, mark as OriginConfirmed if it is not marked yet.
			h.log.Printf("Audit: marking unconfirmed contract %v as confirmed", id)
			// Update the database to contain the corrected storage obligation flag set.
			err = h.db.Update(func(tx *bolt.Tx) error {
				// Get the latest storage obligation in case it has changed
				mso, err2 := h.getStorageObligation(tx, id)
				if err2 != nil {
					return fmt.Errorf("error reading contract %v from database: %w", id, err2)
				}
				//Change the OriginConfirmed status
				mso.OriginConfirmed = true
				// Store the new storage obligation to replace the old one.
				h.log.Debugf("Updating %v in host.db, setting OriginConfirmed to true", id)
				err2 = putStorageObligation(tx, mso)
				if err2 != nil {
					return fmt.Errorf("error updating contract %v in database: %w", id, err2)
				}
				h.financialMetrics.registerStorageObligation(mso.StorageObligation(), bh)
				return nil
			})
			if err != nil {
				auditActionError = fmt.Errorf("auditStorageObligations: database failed to update storage obligation with error : %w", err)
				return false
			}
			// contract not expired, skip potential removal
			return true
		}

		//From here the contract is either not confirmed or reached expiration so is potential candidate for removal

		// First get rid of expired unconfirmed (stale) contracts
		// An obligation is considered stale if it has not been confirmed
		// within RespendTimeout blocks after negotiation.
		// But for safety use contract expiration() instead of (so.NegotiationHeight + wallet.RespendTimeout)
		// as deadline for confirmation to give up on a contract
		if !confirmed && bh > so.expiration() {
			h.log.Printf("Audit: Found expired unconfirmed (stale) contract ID %v", id)
			if len(so.SectorRoots) > 0 {
				//Remove referenced sectors from the stale contract (clean up the list of referenced sectors by removing referenced from it)
				err = h.removeReferencedSectorsFromContract(&so, references)
				if err != nil {
					auditActionError = fmt.Errorf("error removing referenced sectors before deleting: %w", err)
					return false
				}
				// Temporary blocker to not remove big unconfirmed contract referenced sectors yet
				// TODO: Either remove this completely or set the MaxAllowedUnreferencedSectorsToRemove
				// value in storagecleanup.go to a high number. This is bad as it does leave garbage in the storage folders and
				// and the sector metadata (sectors will have a wrong refcount)
				if len(so.SectorRoots) > MaxAllowedUnreferencedSectorsToRemove {
					h.log.Printf("Removing contract %v record from database as expired but unconfirmed", id)
					err = h.deleteContractFromDB(id)
					if err != nil {
						auditActionError = fmt.Errorf("auditStorageObligations: database failed to delete storage obligation with error: %w", err)
						return false
					}
					h.log.Printf("Not removing actual sectors, %d exceeds the maximum number of sectors allowed for removal", len(so.SectorRoots))
					return true
				}
				h.log.Printf("Audit: Removing %d data sectors as not referenced in any active contract", len(so.SectorRoots))
				err = h.RemoveSectorBatch(so.SectorRoots)
				if err != nil {
					auditActionError = fmt.Errorf("auditStorageObligations: removing stale contract data sectors error :%w", err)
					return false
				}
			}
			// It is safe to remove the contract from database here as if it had excess data it would have not reached this place
			h.log.Printf("Removing unconfirmed expired contract %s entry from contract database", id)
			err = h.deleteContractFromDB(id)
			if err != nil {
				auditActionError = fmt.Errorf("auditStorageObligations: database failed to delete storage obligations with error: %w", err)
				return false
			}
			return true
		}

		//Clear/close expired contracts where the storage proof deadline is reached but status not updated (stuck contracts)
		soh := so.proofDeadline() + types.BlocksPerDay
		h.log.Debugf("AUDIT DEBUG: h.blockHeight: %d; storageObligation's proof deadline + BlocksPerDay: %d", bh, soh)
		if (status == obligationUnresolved || status == obligationResolutionPending) && (bh > soh) {
			//TODO: Check the correct state of contract!
			//If it was renewed without storage proof, then it should get
			//marked as success if still as unresolved for correct accounting.
			if len(so.SectorRoots) == 0 { //empty or renewed by renewAndClear
				h.log.Debugf("Audit: Empty storage obligation %v, should be removed as Succeeded", id)
				err = h.removeStorageObligation(so, obligationSucceeded)
				if err != nil {
					auditActionError = fmt.Errorf("error closing empty contract: %w", err)
					return false
				}
				return true
			}
			//Force close contract
			refcount := len(so.SectorRoots)
			h.log.Printf("Audit: Detected expired contract %v that should be closed but has %d sectors referenced", id, refcount)
			err = h.removeReferencedSectorsFromContract(&so, references)
			if err != nil {
				auditActionError = fmt.Errorf("error removing referenced sectors before deleting: %w", err)
				return false
			}
			sectorsToDelete := len(so.SectorRoots)
			h.log.Debugf("Audit: Preserving %d sectors as referenced in active contracts before closing", refcount-sectorsToDelete)
			if len(so.SectorRoots) == 0 {
				h.log.Printf("Audit: Closing contract %s as it has no data not referenced elsewhere", id)
				err = h.removeStorageObligation(so, obligationSucceeded)
			} else {
				h.log.Printf("Audit: Closing contract %s with %d sectors not referenced elsewhere as failed", so.id(), sectorsToDelete)
				err = h.removeStorageObligation(so, obligationFailed)
			}
			if err != nil {
				h.log.Errorf("Audit: Error closing storage obligation %v: %v", id, err.Error())
				return false
			}
			return true
		}
		return true
	})
	return auditActionError
}
